\section{Related Work}\label{sec:rw}


\subsection{Attack Graphs}
In the context of network security, attack graphs identify all known attack paths the attacker could follow to exploit known vulnerabilities to compromise a specific network.
\emph{State-based attack graphs} \citep{sheyner2002} consist of network states, which scales exponentially with the increasing number of logical statements. 
Therefore, a more efficient \emph{logic-based attack graphs} representation was introduced, consisting of \emph{fact nodes}, that can be either true or false, and the \emph{action nodes}, that can modify the facts.
%The state of all fact nodes define the state of the network, which is why this attack graph does not grow exponentially with the number of facts.
There are number of tools that generate such attack graphs, such as \emph{topological analysis of network attack vulnerability} (TVA) from \citep{jajodia2005topological,noel2010measuring}, \emph{a network security planning architecture} (NETSPA) from \citep{lippmann2002netspa} and \emph{multi-host, multistage, vulnerability analysis} (MulVAL) from \citep{ou2005mulval}.


%\subsubsection{Network Security Assessment}
Attack graphs are used to check the existence of sequences of actions that could lead to compromising the network \citep{ritchey2000using}, a minimal set of vulnerabilities that must be patched to guarantee that the intruder cannot achieve their goal \citep{barik2016attack,sheyner2002,jha2002two}, or perform network risk assessment \citep{cheng2014metrics,nandi2016interdicting}.
In papers \citep{buldas2012upper,ou2012quantitative,wang08dbsec} and \citep{hu2017quantitative} the authors analyze attack graphs with probabilistic outcomes of the actions and introduce various quantitative metrics for networks risk assessment.
These probabilities can be estimated from historical data, red team exercises or Common Vulnerability Scoring System (CVSS) presented in \citep{schiffman2004common} that compute the overall score of the vulnerability based on vulnerability attributes.
We use probabilistic actions in attack graphs as well.

\subsubsection{Attack Planning}
%deterministic actions
%Adding costs of actions in the attack graphs directed research to plan cheapest attack plans
The attack graphs with deterministic actions are also used in combination with the classical planning, where researchers find a sequence of actions that lead to a specific goal \citep{boddy2005course} or plans that meet certain constraints \citep{polatidis2017cyber}.
These plans can be used to predict the attacker's behavior or for penetration testing to automate generating of the attack scenarios to ease the task of searching the likely exploitable attack paths \citep{obes2013attack}.
In \citep{sarraute2012pomdps} the authors introduced a POMDP model that includes the attacker's uncertainty about the network configuration, while actions are still deterministic.


%probabilistic action to find attack path
Planning with probabilistic actions is more complex since plans are not \emph{linear} anymore but \emph{contingent}, specifying the attacker course of actions in the case the previous attempt of action failed.
Algorithms that find optimal contingency plans do not scale well \citep{durkota2014} and heuristic algorithm is presented in \citep{sarraute2012pomdps}.
In \citep{bi2016k,jia2010attack,borbor2017securing} the researchers focus on finding the most probable attack paths to identify areas to improve the security, or to replan if previous action should fail \citep{krautsevich2012towards}.
%probabilistic action to find contingency plan

\subsubsection{Attack Graph Games}
The main motivation to use game-theory in combination with the attack graphs is to model the dynamics of the attacker's complex reaction to the defender's defense measures.
Static defense measures, such as presented in \citep{barik2016attack,sheyner2002,jha2002two} can result in more expensive security measures than are necessary to demotivate the attacker from attacking a network. %, or for some vulnerabilities, there may not exist vulnerability patches at all.
Modeling the attacker's possible reactions to the defense strategies can lead to more efficient and robust defense strategies.

In \citep{bistarelli2006strategic} the authors published the first attack tree game, where the attacker chooses a path from the root to a leaf (goal) according to the attack tree, and the defender chooses a defense measure.
A game-theoretic model similar to ours is presented in \citep{letchford2013optimal}, where the attacker chooses an attack based on the attack graph that maximizes his expected utility and the defender decides which vulnerabilities to fix in the attack graph to maximize his own expected utility.
In contrast to this work, we do not assume deterministic actions outcomes; additionally, we compute strategy for multiple networks at once, which allows exploiting the attacker's uncertainty about the attacked networks.

\subsection{Honeypots}

%Usualy securing a computer network meant deploying intrusion detection systems (IDS) to detect and block malicious actors.
%Attackers have significant advantage in such asymmetric games, where network administrators only block the malicious actors.

Adversary deception and misinformation can be used to detect and mitigate an attack.
In \citep{almeshekah2016cyber} the authors present an overview of deception techniques in the cybersecurity domain.
Decoy-based deception mechanism, e.g. \emph{honeypot}, is a network component that serves to deceive the attacker into believing that it is a valuable target \citep{almeshekah2016cyber}.
In \citep{spitzner2003honeypotspap} the researchers use honeypots to detect and analyze attackers' behavior and the tools they use.
For example, the Honeynet Project in \citep{spitzner2003honeynet} provides software and literature describing the knowledge acquired about the attackers.
Companies use honeypots as intrusion detection systems (IDS), e.g., a hardware product called \emph{Canary} produced by company Thinkst is a simple honeypot that can emulate various operating systems.
 
%\subsubsection{Deception Games}
A game-theoretical framework can explicitly capture the information that players reveal and obtain; therefore, it can be used to compute deception strategies.
%It is often used to compute strategies for the systems when to reply truthfully or untruthfully.
One of the basic deception game was introduced in \citep{carroll2011game}, where the authors use signaling game to analytically compute a strategy, which prescribes when a production host should pretend to be a honeypot and when a honeypot should pretend to be a production host.
In \citep{hayatle2012game} the authors introduce the game, where a botmaster (attacker) tries to discriminate the honeypot services from the production services by executing testing and attacking actions.
On the other hand, the honeypots decide what to reply to the attacker to be as believable real service as possible, and whether to execute or not the attacker's potentially dangerous command.
In \citep{albanese2016deceiving,jajodia2017probabilistic} the authors investigate the responses the defender should reply to the attacker to maximize the defender's utility in more detail.

In this work, we investigate where in the network to deploy the honeypots and what services they should provide rather than what answer they should respond to the attackers.
In \citep{garg2007deception,pibil2012game} the authors model game-theoretically the situation without attack graphs, where the attacker has a direct visibility of the hosts and decides which hosts to probe and then to attack.
Our work is distinguished from the above in several aspects.
We consider the attacker's contingency plans based on the attack graphs generated by MulVAL, which fully characterize the attacker's behavior and allows the defender to strategize against all possible scenarios.
Parts of our work were published in \citep{durkota2014,durkota2015optimal}, where the attackers are assumed to have perfect information about a network; in \citep{durkota2015approximate} we introduce heuristic algorithms and compared them to a upper bound on the defender's utility in Stackelberg equilibrium; in \citep{durkota2016case} we compared game-theoretic strategies to human strategies and analyzed when the game-theory is superior to humans and otherwise.

%In this paper we introduce optimal and approximation algorithms to compute the solution concept.
%We reduce the size of the game by approximation the set of rationalizable actions for the attacker at the cost of losing almost neglectable relative utility of the players.
%This allows us measuring the qualities of the heuristic algorithms more accurately and draw more conclusive results about the them as well as to answer different networks security related questions.
