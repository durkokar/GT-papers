\section{Experiments}\label{sec:experiments}

We experimentally evaluate the game-theoretic model and compare the proposed heuristic algorithms to the approximation algorithm (MILP) described in Section~\ref{sec:alg-so}.
Namely, we examine: (i) Correlated Stackelberg Equilibrium (CSE) as described in Section~\ref{sec:alg-slp}, (ii) Perfect information (PI) solved using backward induction described in Section~\ref{sec:alg-pi}, and (iii) the zero-sum games solved with single-oracle algorithm described in Section~\ref{sec:alg-zs} (ZS1 through ZS4).

The structure of the experimental section is as follows: in Section~\ref{sec:setting} we describe the networks and attack graphs for our games,
in Section~\ref{sec:scale} we analyze the scalability of the approximation and heuristic algorithms,
in Section~\ref{sec:util} we compare the qualities of the strategies found by the heuristic algorithms to the strategies computed by approximation algorithm,
in Section~\ref{sec:sensitivity} we investigate the sensitivity of our model on the attack graph parameters,
in Section~\ref{sec:nohp} we answer several research questions such as the impact of incorrectly assuming the imperfect information about the attacker, etc.

\subsection{Experimental Setting}\label{sec:setting}

We use five different computer network topologies, depicted in Figure~\ref{fig:nets} to generate the attack graphs for the game.
Labels $(remote, local, client)$ at the hosts denotes the number of remote, local and client vulnerabilities of that host type in the attack graph.
The action costs in attack graphs we sample from a normal distribution $NormDist(50, 50)$.
We assume that exploits are not expensive compared to the rewards, as they are available online (e.g., in Metasploit\footnote{www.metasploit.com}), but the attacker still pays for spending some resources (e.g. time).
The action success probabilities we draw from normal distribution $NormDist(0.7, 0.2)$ with interval bounds $[0,1]$.
%We set these probabilities higher for analytical purposes to avoid situations where attacker's expected reward is lower than the expected cost and does not carry out an attack at all.

For a specific set of host types $T$, we generate the set of networks $\mathcal{N}$ for the Nature to choose from by extending the network that consist of a host of a black host type in Figure~\ref{fig:nets}.
We extend that network by adding up to $N$ hosts from $T$ (the black and possibly gray host types), which results in $|\mathcal{N}|=\sum_{i=0}^N|T|^{i}$ networks for the Nature to choose from.
Similarly, the defender chooses from $|Y|=\sum_{i=0}^k|T|^{i}$ different defenses for each network.
Increasing the parameter $T$ increase the complexity of the network attack graphs and the set of the Nature's and the defender's actions.

The \emph{business} network is inspired by \citep{homer2013aggregating}, and resembles a smaller company computer network.
For $T=3,4,\dots,$ we increase the network complexity by adding host types VPN, Firewall, PC$_2$, PC$_3$, etc., which results in attack graphs with 7, 13, 16, 22 attack actions, in this order.

\emph{Chain} network consists of PC types in a grid-shaped topology with two rows and a variable number of columns.
We increase the network's complexity by adding a column of PC's, where all hosts in the last column have a direct visibility to the database.
For $T=3,4,\dots,$ the attack graphs consist of 7, 15, 23, 31 attack actions, in this order.

\emph{LocalPlus} is a simple network topology, where only host types of PC.
For $T=4,5,\dots,$ the attack graphs consist of 8, 11, 14, 17 attack actions, in this order.

\emph{TV} network is inspired by a network used during cyber security exercises in Swedish Defence Research Agency in 2012 \citep{sommestad2015empirical}, where the networks were deliberately left vulnerable to the attacks.
For $T=5,6,\dots,$ the attack graphs consist of 26, 29, 32 attack actions, in this order. 

\emph{Unstructured} (unstr) is a synthetic network consisting of host types that are directly visible to the attacker from the internet.
%The attacker can attack any host type at any time, therefore, order attack actions in the attack policies in any order.
These attack graphs provide the largest number of possible attack policies w.r.t. the number of host types, in comparison to the attack graphs of the other network.
For $T=1,2,\dots,$ the attack graphs consist of 3, 6, 9, 12 attack actions, in this order.
In all networks, the values of the host types are set as follows: $r_{Database}=r_{studio}=5000, r_{Server}=2500, r_{PC}=r_{VPN}=500$, and honeypot cost factor is $\gamma_c=0.02$.
%All experiments were run on a 2-core 2.6GHz processor with 30GB memory and 2 hour of runtime limitation.



\begin{figure}[t!]
  \subfloat[][Business]{%
    \resizebox{5cm}{!}{
      \usebox{\business}\label{fig:netBusiness}
    }
  }
  \hfill
  \subfloat[][Chain]{%
    \resizebox{5cm}{!}{
      \usebox{\chain}\label{fig:netChain}
    }   
  }
  \\
  \subfloat[][LocalPlus]{%
    \resizebox{4cm}{!}{
      \usebox{\localPlus}\label{fig:netLocalPlus}
    }   
  }
  \subfloat[][TV]{%
    \resizebox{5cm}{!}{
      \usebox{\tv}\label{fig:netTV}
    }   
  }
  \subfloat[][Unstrucured]{%
    \resizebox{3cm}{!}{
      \usebox{\unstr}\label{fig:netUnstr}
    }   
  }
  \caption{Networks used in the experiments}
  \label{fig:nets}
\end{figure}


%Two of them are depicted in Figure~\ref{fig:net}, \emph{small business} (Figure~\ref{fig:netBusiness}) and \emph{chain} network (Figure~\ref{fig:netChain}). % and a \emph{unstructured} network.
%Connections between the host types in the network topology correspond to pivoting actions for the attacker in the attack graph (from the compromised host the attacker can further attack the connected host types).
%We vary the number of vulnerabilities of each host type, which is reflected in the attack graph as an attack action per vulnerability.
%We generate the actions' success probabilities $p_a$ using the MulVAL that uses Common Vulnerability Scoring System.
%Action costs $c_a$ are drawn randomly in the range from 1 to 100, and host type values $r_t$ and the cost for honeypot $c^h_t$ of host type $t$ are listed in Table~\ref{tab:b552}.
%We assume that the more valuable a host type is the more expensive it is to add a HP of that type.
%We derive honeypot costs linearly from the host values with a factor of 0.02.
%The basis network $b$ for the business and chain network consists of the black host types in Figure~\ref{fig:net}.
%We scale each network by adding the remaining depicted host types and then by additional workstations.
%We also scale the total number of hosts $n$ in the network and the number of honeypots $k$.
%Each parameter increases combinatorially the size of the game.  

%\begin{table}[t!]
%  \centering
%  \caption{Host type values and costs for deploying them as honeypots.}
%  \label{tab:b552}
%  \begin{tabular}{ | l | c | c|c|c| r | }
%    \hline
%    Host type $t$ & database & firewall & PC$_n$ & server \\ \hline
%    Value of host type $r_t$ & 5000 & 500 & 500 & 2500 \\ \hline
%    Cost for deploying HP of host type $c^h_t$& 100 & 10 & 20 & 50 \\ \hline
%  \end{tabular}
%\end{table}


%\subsection{Analytical Approach for CURB for Unstructured Network}\label{sec:anal}
%The incremental pruning algorithm described in Sec.~\ref{sec:CURBasPOMDP} generates a very large number of attack policies in the CURB set for the unstructured network.
%In order to be able to compute the upper bound for solution quality for larger game and in order to understand the complexities hidden in CURB computation, we analyze this structure of the curb for this simplest network structure formally.
%In Figure~\ref{fig:curbPrev} we show an example of the attacker's utilities for the policies in a CURB set generated for an information set with two networks.
%Let $h_t$ be the probability that action $t$ interacts with a honeypot.
%On the x-axis is probability distribution space between two networks, one with $h_t=0$ and other with $h_t=1$.
%The y-axis is the attacker's utility for each attack policy in the CURB.
%The algorithm generates the attack policies known as \emph{zero optimal area policies} (ZAOPs)~\citep{littman1994witness}, denoted with dashed lines in the figure.
%A policy is ZAOP if and only if it is an optimal policy at a single point in the probability space (dashed policies in Figure~\ref{fig:curbPrev}).
%The property of ZAOP is that there is always another policy in the CURB set with strictly larger undominated area.
%%Therefore, in the witness algorithm the ZAOPs can be removed, although, they are hard to detect.
%It raises two questions: (i) can we remove ZAOPs from the CURB set and (ii) how to detect them.
%Recall that in SSE the attacker breaks ties in favour of the defender. % plays his best response and in case he is indifferent between two actions he plays the one that yields higher utility for the defender.
%Therefore, we can discard ZAOP as long as we keep the best option for the defender. 
%
%Further analysis showed that ZAOPs occur when $h_t=1-\frac{c_t}{p_tr_t}$ (at probability 0.69 and 0.99 in Figure~\ref{fig:curbPrev}).
%It is because the expected reward of action $t$ at that point is $p_t(1-h_t)r_t-c_t = p_tr_t\frac{c_t}{p_tr_t} - c_t = 0$, which means that the attacker is indifferent whether to perform action $t$ or not. 
%The algorithm at probability $h_t=1-\frac{c_t}{p_tr_t}$ generates the set of attack policies with all possible combinations where the attacker can perform action $t$ in the attack policy.
%Let $P(t)$ be the probability that the attacker performs action $t$ in an attack policy.
%The defender's utility for action $t$ is $-r_tp_t(1-h_t)P(t) = -c_tP(t)$.
%Because the attacker breaks ties in favour of the defender, at $h_t=1-\frac{c_t}{p_tr_t}$ the attacker will choose not to perform action $t$ and we can keep only the policy that does not contain action $t$.
%
%Furthermore, we categorize each action $t$ based on $h_t$ to one of three classes: to class A if $h_t=0$, to class B if $0<h_t<1-\frac{c_t}{p_tr_t}$ and to class C if $1-\frac{c_t}{p_tr_t}<h_t$.
%In an optimal attack policy: (i) all actions from A are performed first and any of their orderings yield the same expected utility for the attacker, (ii) all actions from B are performed and their order is in increasing order of $\frac{p_t(1-h_t)r_t-c_t}{h_t}$ ratios, and (iii) none of the actions from C are performed, as they yield negative expected reward for the attacker.
%We partition all probability distributions into regions $h_t = 1-\frac{c_t}{p_tr_t}$ and in each region we assign actions to the classes.
%We find the set of optimal attack policies for each region.
%The attack policies in one region differ from each other in ordering of the actions in B.
%
%In Figure~\ref{fig:beliefClassed} we show an example of the probability distribution space of three networks in an information set.
%The probabilities that actions $1,2$ and $3$ interact with a honeypot represent a point $(h_1,h_2,h_3)$. 
%We partition the probability space and assign each action to a class.
%In all experiments we use this approach to generate the CURB set without ZAOPs in games for unstructured networks.




%Therefore, we can eliminate all ZAOPs that are generated as combinations of places where to perform action $t$.

%We use more analytical approach to generate the curb for the information set by partitioning the probability space it into regions with borders $h_i = 1-\frac{c_i}{p_ir_i}$ and within each region we compute set of optimal attack policies for that region.
%In Figure~\ref{fig:beliefClassed} we present an example of a belief space made of three states in the information set, and its partition into four regions.
%In Figure~\ref{fig:curbPrev} and Figure~\ref{fig:curbAfter} we compare the CURB sets generated with POMDPs and the analytical approach.
%That crucial part is the upper envelope of the CURBs which is identical for both approaches.
%In all experiments we use analytical approach for the unstructured network which increases its scalability.


%While running SLP approximation on unstructured network we ran into a issue that POMDPs approach, described in Sec.~\ref{sec:slp}, can generate exponential number of undominated attack policies in the CURB for the attacker's information sets.
%However, the exponential explosion of the undominated attack policies is in a single belief state in a continues belief space of honeypot deployments.
%Specifically, assume that action $i$ interacts with a honeypot with probability $h_i = 1-\frac{c_i}{p_ir_i}$.
%The expected reward of such action is $p_i(1-h_i)r_i-c_i = p_ir_i\frac{c_i}{p_ir_i} - c_i = 0$, which means that either performing this action nor not at the bottom of the attack policy yields the same utility for the attacker.
%For this belief the POMDP search returns set of attack policies which differ one from another with combination of leafs where action $i$ is performed.
%Therefore, we use rather analytical approach to generate the CURB for the information set by partitioning it into regions with borders $h_i = 1-\frac{c_i}{p_ir_i}$ and within each region we compute set of optimal attack policies for that region.
%In Figure~\ref{fig:beliefClassed} we present an example of a belief space made of three states in the information set, and its partition into four regions.
%In Figure~\ref{fig:CURBPrev} and Figure~\ref{fig:curbAfter} we compare the CURB sets generated with POMDPs and the analytical approach.
%That crucial part is the upper envelope of the CURBs which is identical for both approaches.
%In all experiments we use analytical approach for the unstructured network which increases its scalability.

%\begin{figure}
%    \centering
%    \subfloat[]{\includegraphics[width=0.5\textwidth]{fig/experiments/data2prev-dashed}}
%    \subfloat[]{\includegraphics[width=0.5\textwidth]{fig/experiments/beliefSpaceClassed}}
%    \caption{(a) Attack policies from a CURB set for an information set for the unstructured network. (b) Probability space partitioning by action belonging into the categories. }
%    \label{fig:comparison}
%\end{figure}


\subsection{Scalability}\label{sec:scale}
\subsubsection{Setting}
We analyze the size of the problems that each algorithm can solve.
For each network, we ran 3 instances with different random seeds to draw the action success probabilities and costs in the attack graphs.
We increase the problem complexity by increasing $T$, $N$ and $k$.
For each experiment we set 2 hour time limit.
%Each problem setting we ran with different seeds which generate mentioned values.
%For algorithms that did not finish within the two hour limitation we show empty bar in Figure~\ref{fig:busTime}.
%If approximation algorithm did not finish within two hours, we leave an empty space in the bar charts.

\subsubsection{Discussion}
In Figure~\ref{fig:runtime-comparison} we show the average, minimal and maximal runtime (as error bars) for each algorithm and network type that finished within the given time limit.
The number on each bar denotes the number of finished instances (if less than 3).  
The results indicate that finding approximation solutions with the MILP does not scale well, finding a solution in only 49\% of the problems within 2 hour time limit.
CSE was able to solve larger instances than the MILP, and 88\% of the problems in total.
Both algorithms first have to compute CURB for each information set in the game before engaging into computation of the solution concepts.

All zero sum approaches have similar scalability, so we present only ZS1 here.
The zero sum and PI approaches do not require constructing CURBs in advance, so they were able to solve 97\% and 99\% of the problems, respectively.
We were able to reached their limits in game with TV network topology ($T=8, n=3, k=3$) which consists of $|\mathcal{N}|=|Y|=585$ actions for the Nature and the attacker and 3552 information sets for the attacker.
In all instances PI was faster then zero sum approach.
%In the unstructured network the problem specific CURB computation took only about 4\% of total runtime.


\begin{figure}[h!]
    \centering
	\subfloat[][Business network]{%
		\includegraphics[width=0.5\textwidth]{fig/r/business/runtime_sec_log.pdf}
		}
	\subfloat[][Chain network]{%
		\includegraphics[width=0.5\textwidth]{fig/r/chain/runtime_sec_log.pdf}
		}

	\subfloat[][LocalPlus network]{%
		\includegraphics[width=0.5\textwidth]{fig/r/localPlus/runtime_sec_log.pdf}
		}
	\subfloat[][TV network]{%
		\includegraphics[width=0.5\textwidth]{fig/r/tv/runtime_sec_log.pdf}
		}

	\subfloat[][Unstructured network]{%
		\includegraphics[width=0.5\textwidth]{fig/r/unstr/runtime_sec_log.pdf}
		}
	\caption{Average, minimal and maximal (the error bars) runtimes in seconds for each algorithm, different network topology, number of host types $T$, parameter to generate networks for the nature $N$ and maximal number of honeypots available to the defender $k$}
    \label{fig:runtime-comparison}
\end{figure}

\subsection{Solution Quality}\label{sec:util}
\subsubsection{Setting}
We analyze the quality of the strategies computed by each algorithm in comparison to the strategy from the MILP approximation.
We use the concept of \emph{relative regret} to capture the relative difference in the defender's utilities for using one strategy instead of another.
The relative regret of strategy $\sigma_d$ w.r.t. the strategy $\sigma_d^*$ is $\rho(\sigma_d,\sigma_d^*) = 100\times\frac{u_d(\sigma_d^*,BR_a(\sigma_d^*))-u_d(\sigma_d,BR_a(\sigma_d))}{|u_d(\sigma_d^*,BR_a(\sigma_d^*))|}$ [in \%].
%In our games, the defender's expected utility is always negative or zero, therefore the relative regret $\rho(\sigma_d,\sigma_d^*)$ is always positive. 
The higher the regret $\rho(\sigma_d,\sigma_d^*)$ the worse strategy $\sigma_d$ is compared to strategy $\sigma_d^*$ for the defender. 
In the case the defender's strategy yields zero utility we do not add this instance in the total average due to division with zero.
This can happen, when the attacker's actions have low success probabilities and high action costs, so the attacker does not attack even if no honeypots are deployed.
This happened only for unstructured network topology in 9 instances.

We additionally tested game-theoretic strategies against baseline strategies.
\emph{Uniform} (Uni) strategy, which deploys all honeypots with equal probability.
This strategy produces high uncertainty for the attacker in the information sets, however, it does not consider that the attacker focuses on attacking the valuable host types.
The second baseline approach, \emph{most-rewarded} (Rew), deploys honeypots of the most valuable host type.
While it may protect the most valuable host type, the attacker can confidently attack the remaining host types.
The last baseline is \emph{weighted-reward} (Wei), which is a mixture of the previous two baseline methods.
In each network it deploys honeypot of type $t$ with the probability proportional to its value (for type $t$ the probability is $\frac{r^t}{\sum_{t\in T}r^t}$).
%This strategy works well if only few honeypots are available. However, with more honeypots, the defender may prefer duplicating less valuable hosts, if attacks to the more valuable hop from the less valuable hosts.

\subsubsection{Discussion}

%T, F
% business: 2, 28
% chain     0, 23
% tv        0, 16
% local+    7, 34
% unstr    10, 54
%          19, 155

In Figure~\ref{fig:utility-comparison} we compare the defender's utility regrets for the strategies from heuristic algorithms in comparison to the strategy from MILP approximation (notice logarithmic y axis).
We compute regrets only from the solutions where all algorithms found strategies within the 2 hour limit (including MILP).
CSE found strategies with the lowest overall regrets and in 88\% of the problems it even found the strategy with zero regret.

Among the zero sum approaches, ZS4 performs the best except the unstructured network.
In ZS4 approach the defender's utility is augmented to prefer outcomes with expensive attack policies for the attacker.
Therefore, the ZS4 approach works well for networks where long attack policies are produced (e.g., tv or chain networks) but worse where attack policies are cheap (e.g. unstructured network).
Perfect information strategy had relative regret of 2\% in all except the unstructured network.
This can be useful for computing the defense strategies if more accurate algorithms are intractable for a problem.
The unstructured network topology has higher overall regrets compared to the other networks, because there is no host type with very high value.
Since in the unstructured networks the players yield utilities closer to zero, thus a smaller change in the utility results in higher regret.

Although the ZS4 performed best among the zero-sum heuristics, each of the zero sum heuristics have certain drawbacks. 
In the ZS1 and ZS2 the defender ignores his costs for deploying the honeypots; these strategies often deploy the honeypot of database type, which is the most expensive action.
For example, the ZS2 deploys database in 74\% of the problems, while the CSE only in 43\% of the problems.
Strategies computed by the ZS3 and ZS4 are difficult to analyze.
They often miss the precise probability distribution of the states in the information set that makes the attacker indifferent between two or more attack policies, in which case the attacker would choose a policy in favor of the defender.
However, there is no general error that the strategies computed with the zero-sum heuristics do.

The baseline strategies, that do not consider the attacker's possible reactions to the defense strategies, resulted on average relative regret about 56\%.
Even the worst game theoretical algorithm (PI) performed better than any baseline solutions.
This shows that game-theoretic approach are essential to develop good defense strategy.

Since we use approximated CURB in the MILP and CSE algorithms, let us present the players' total utility errors due to CURB approximation.
For $\epsilon=10^{-5}$, the maximal relative errors for the defender and the attacker's across all experiments were only $5.4\times 10^{-3}$\% and $1.6\times 10^{-2}$\% (for the TV network), respectively, which can be neglected.
In the games with TV network topology it dramatically decreased the number of policies in $\kappa(I)$ (e.g., for some information set from 500 to 4).


%The defender often allocates honeypots so the attacker interacts with them towards the end of the attack policy, in which case the attacker pays for unsuccessful attack.

\begin{figure}[h!] \centering
	\subfloat[][Business network (37 instances).]{\includegraphics[width=0.5\textwidth]{fig/r/business/utility.pdf}}
	\subfloat[][Chain network (32 instances).]{\includegraphics[width=0.5\textwidth]{fig/r/chain/utility.pdf}}

	\subfloat[][LocalPlus network (60 instances).]{\includegraphics[width=0.5\textwidth]{fig/r/localPlus/utility.pdf}}
	\subfloat[][TV network (16 instances).]{\includegraphics[width=0.5\textwidth]{fig/r/tv/utility.pdf}}

	\subfloat[][Unstructured network (74 instances).]{\includegraphics[width=0.5\textwidth]{fig/r/unstr/utility.pdf}}
	\caption{The defender's average relative regret and standard error (error bars) for the strategies computed by the heuristic algorithms and the baseline approaches for each network}
    \label{fig:utility-comparison}
\end{figure}

\subsection{Sensitivity Analysis}\label{sec:sensitivity}
\subsubsection{Setting}

The defender's optimal strategy highly depends on the attack graph structure, the action costs, success probabilities and rewards.
In real-world scenarios the defender can only estimate these values.
Since the ZS4 approach has a good trade-off between its scalability and the solution quality, we analyze the sensitivity of the defender's strategies computed with the ZS4 to perturbations in action costs, success probabilities and values of host types in attack graphs.

We generate the defender's estimate of the attack graph by perturbing the original attack graph actions as follows: (i) action success probability are chosen uniformly from the interval $[p_a-\delta_p, p_a+\delta_p]$ restricted to $[0.05,0.95]$ to prevent it becoming impossible or infallible, (ii) action costs are chosen uniformly from interval $[c_a(1-\delta_c), c_a(1+\delta_c)]$, and (iii) rewards for host $t$ uniformly from the interval $[ r_t(1-\delta_r), r_t(1+\delta_r)]$, where $p_a,c_a$ and $r_t$ are the original values and $\delta_p,\delta_c$ and $\delta_r$ is the amount of perturbation.
The action probabilities are perturbed absolutely (by $\pm \delta_p$), but the costs and rewards are perturbed relative to their original value (by $\pm \delta_c c_a$ and $\pm \delta_r r_f$).
The intuition behind this is that the higher the cost or reward values the larger the errors the defender could have made while estimating them, which cannot be assumed for the probabilities.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.5\textwidth]{fig/experiments/businessPerp-cropTimes100.pdf}
	\caption{The defender's regret for perturbed action success probabilities, action costs, and host type values}
	\label{fig:perturb}
\end{figure}


\subsubsection{Discussion}
We compute (i) the defender's strategy $\sigma_d$ on the game with the original attack graphs and (ii) the defender's strategy $\sigma_d^p$ on the game with the perturbed attack graph.
Figure~\ref{fig:perturb} presents the dependence of the relative regret $\rho(\sigma_d,\sigma_d^p)$  on the perturbations of each parameter individually ($\delta_p,\delta_c,\delta_r$) and altogether ($\delta_a$). 
The results suggest that the regret depends significantly on the action success probabilities and the least on the action costs.
For example, the error of 20\% ($\delta_a=0.2$) in the action probabilities results in a strategy with 25\% lower expected utility for the defender than the strategy computed based on the true values.
The same imprecision in action costs or host type rewards result in only 5\% lower utility.

%We also examined the effect of perturbing each component individually, however, due to the space limit, we did not include figures (which are similar). 

\subsection{Essential Network Security and Honeypot Related Analysis}\label{sec:nohp}
%\kdnote{Do you have better title for this section??}
The presented game-theoretic framework can be used to answer interesting questions.
For instance, what is the optimal number of the honeypots to deploy into the network, what is the defender's regret for not using the honeypots, or for not exploiting the attacker's imperfect information in the game.
In this section, we answer some representative questions using our game-theoretic framework.

{\bf Honeypot Contribution to Network Security}
It is important to quantitatively measure \emph{how honeypots contribute to securing the network}, which is often required by the organizations before deciding whether or not to deploy the honeypots.
We use the relative utility regret measure to compare two defender's strategies: one which does not use any honeypot and the second which deploys up to $k$ honeypots according to the MILP strategy.
In Table~\ref{tab:nohp-regret} we summarize the results and show that the defender has a significant regret for not using honeypots.
The regrets differ across the network, since some of them are initially harder to exploit, e.g., the chain network has database located far from the internet and the attacker needs to compromise many intermediate host types.

Notice that for all networks (except the unstructured) the regret for not deploying two honeypots ($k=2$) is larger than twice the regret for not using one honeypot ($k=1$).
Stated differently, the defender is able coordinates the two honeypots to produce even higher utility benefit than the sum of the benefits of the individual honeypots.
For some networks the same holds for three honeypots, but the benefit of each additional honeypot is expected to decrease, as we will see later in this section.
%While the contribution of the third honeypot depends on the network, the contribution of each additional honeypot is expected to decrease.

\begin{table}[t]
  \centering
  \caption{The defender's relative utility regret [in \%] for not using up to $k$ in the network.}
  \label{tab:nohp-regret}
  \begin{tabular}{ | r | c | c | c |c|c | }
    \hline
	  & \multicolumn{5}{c|}{ Network } \\\hline
      $k$  & business & chain & localPlus & TV & unstructured \\ \hline
    1& 52.9  &  87.6  & 49.4  & 90.5      & 682.64  \\ \hline
    2& 117.8  &  170.8  & 120.4  & 217.7  & 1083.15  \\ \hline
    3& 184.54  & 233.3  & 185.8  & 364.2  & 1183.94 \\ \hline
  \end{tabular}
\end{table}

%In Figure~\ref{fig:scalezs4} we present the defender's utility components separately, for the HP costs and expected loss for being compromised.
%Values at each point shows how many honeypots in average were deployed by each defender using that strategy.

%\subsubsection{Defender's Cooperation}\label{sec:defCooperation}
{\bf Regret for PI}
In \citep{durkota2015optimal} it was shown that in games where the attacker has perfect information (PI) about the network, the defender either duplicates the most valuable host type or deploys honeypots of the host types in the "choke-points" of the network, which is a set of hosts that the attacker must always pass in order to compromise a target or a valuable host.
In games where the attacker has imperfect information about the networks, the defender's decision-making is more complicated, as he must weigh whether to adopt one of the previously mentioned PI strategies or to exploit the attacker imperfect information in the information sets.
In Table~\ref{tab:is-regret} we present the defender's regret (MILP vs PI) for playing perfect information strategy instead of the MILP strategy.
The average relative regret is between 2.5\% and 3\%, which suggests that the attacker's uncertainty can be exploited to the defender's benefit, but not by much.

{\bf Attacker's Uncertainty in Information Sets}
The defender's regret for using the PI strategy instead of the MILP strategy does not reveal \emph{how} the MILP strategy exploits the attacker's imperfect information in the game.
Here we aim to understand the degree of the uncertainty that the attacker faces in the information sets due to the defense strategies.
We use \emph{Entropy} to measure the unpredictability of each state in the information set for the attacker. 
In Table~\ref{tab:is-regret} we show the weighted average entropy across all of the attacker's information sets computed for MILP and PI strategies.
The higher the entropy is, the less predicable states are for the attacker in the information sets.
Although the PI heuristic strategy does not explicitly exploit the attacker's imperfect information, in some cases it may play to the same information set by chance and create uncertainty for the attacker, as can be seen in the results.
Thus, the entropy for the PI can be considered as a baseline value.
In comparison, we see that the MILP strategy actively exploits the attacker's imperfect information, which can be observed by having twice as much entropy as the PI strategy.

{\bf Incorrectly Assuming Imperfect Information}
%Our model assumes that the attacker has the imperfect information in the game.
If the imperfect information assumption for the attacker is not met, the computed strategies that try to exploit it (MILP) may results in a dramatic loss for the defender.
First, we define the defender's relative utility loss for playing strategy $\sigma_d$ and the attacker switching from $\sigma_a$ to $\sigma_a'$ as $100\times\frac{u_d(\sigma_d,\sigma_a)-u_d(\sigma_d,\sigma_a')}{u_d(\sigma_d,\sigma_a)}$ [in \%].
In Table~\ref{tab:is-regret} (MILP vs. MILP-PI) we show the defender’s relative utility loss if the attacker plays perfect information best response instead of the imperfect information best response.
The results show that this loss can be often three times as large as the defender's relative regret for playing PI strategy instead of MILP (in MILP vs. PI).
The attacker can actively exploit the defender's strategy which incorrectly assumed the attacker's imperfect information.
Based on these results, the strategy that exploits the attacker's imperfect information (MILP) should be used only in the case, that the attacker's imperfect information assumption is likely to be true.
Otherwise, it is better to play securely perfect information (PI) strategy, which does not attempt to exploit the attacker’s imperfect information and itself is a less exploitable strategy.

{\small
\begin{table}[t]
  \centering
	\caption{For each network topology we show the weighted average Entropy for the attacker in the information sets due to MILP and PI strategies. 
	We show the defender's relative regret for playing PI instead of MILP (MILP vs PI) and the defender's relative loss if the imperfect information assumption does not hold for the attacker (MILP vs MILP-PI).}
  \label{tab:is-regret}
	\setlength\tabcolsep{2.0pt}
  \begin{tabular}{ | r | c | c | c |c|c| }
	  \hline
	  & unstructured & business & chain & local & TV \\ \hline
	  Entropy for MILP & 0.45 $\pm$ 0.1 & 0.48 $\pm$ 0.1 & 0.44 $\pm$ 0.1 & 0.51 $\pm$ 0.1 & 0.32 $\pm$ 0.1 \\ 
	  Entropy for PI & 0.02 $\pm$ 0.1 & 0.26 $\pm$ 0.1 & 0.28 $\pm$ 0.1 &   0.28 $\pm$ 0.1 &  0.27 $\pm$ 0.1 \\ \hline
	  MILP vs PI & $100.69 \pm 18.53 $  &  $2.95 \pm 0.28$  & $2.87 \pm 0.40$ &  $2.94 \pm 0.23$ &  $1.01 \pm 0.36$ \\ 
	  MILP vs MILP-PI & 515.21 $\pm$ 73.6 & 9.07 $\pm$ 0.5 &  9.46 $\pm$ 1.2 &  9.08 $\pm$ 0.4 & 2.84 $\pm$ 1.1  \\ \hline
  \end{tabular}
\end{table}
}


%\subsubsection{Optimal Number of Honeypots}\label{sec:scaleZS4}
\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\textwidth]{fig/scalezs4.pdf}
	\caption{The defender's cost for having his network compromised $R_d(\sigma_d,\sigma_a)$ (solid line) and cost for deploying honeypots $C_d(\sigma_d)$ (dashed line) for different number of honeypots $k$ in business network with $N=1, T=3$.
	For each $k$ we depict the expected number of honeypots (values) used in each network for different honeypot cost factors.}
	\label{fig:scalezs4}
\end{figure}

{\bf Optimal Number of Honeypots}
Another natural question that arises is how many honeypots the defender should deploy, taking into account the costs, to protect the networks maximally.
To find the optimal number of honeypots, we increase the maximum number of allowed honeypots $k$ and for each find ZS4 strategy.
We choose ZS4 for its scalability and low relative utility regret for the defender.
%Here, we allow the defender to deploy at most $k$ honeypots in each network after the chance plays.
In Figure~\ref{fig:scalezs4}, we show the average number of honeypots that the defender deploys in each of the Nature's network.
We scale the number of available honeypots for the defender  ($k=1,\dots,15$) with two different honeypot deployment cost factors.
For the honeypot cost factor $\gamma_c=0.1$ the defender uses 4.6 honeypots on average. 
Any additional honeypot costs him more than it contributes to securing the network.
While with cheaper honeypots ($\gamma_c=0.02$) the defender deploys 11.5 honeypots on average in each network after the Nature move.
Converging to a specific number of honeypots does not necessarily imply that with $k+1$ honeypots the defender's strategy would not change, but it is unlikely.

{\bf Defender's Utility Cost Components}
Analyzing the defender's different cost components can be valuable for strategic planning of an organization.
In Figure~\ref{fig:scalezs4} we present the defender's cost for having compromised the network (solid line) and the expected cost for the deployed honeypots (dashed line) for the different honeypot cost factors.
For example, with $\gamma_c=0.02$ and $k=12$ we can see that the network is almost secure and most expenses are due honeypot costs. 
Therefore, to increase the defender's expected utility, an additional security measure may not be as desirable as reducing the honeypot costs.

{\bf Choice of Honeypot Types}
Another interesting observation in Figure~\ref{fig:scalezs4} can be seen for $\gamma_c=0.1$ and for $k=2$, where the overall honeypot deployment cost strategy $C_d(\sigma_d)$ is larger than for the case of $\gamma_c=0.1$ and $k=3$.
With the third honeypot ($k=3$) the probability of deploying the cheaper server honeypot has increased from 0.51 to 0.73, while the probability of deploying the twice more expensive database honeypot has decreased from 0.37 to 0.02.
With three honeypots the honeypot deployment cost $C_d(\sigma_d)$ is cheaper \emph{and} results in a more secure strategy (higher $R_d(\sigma_d,\sigma_a)$) at the same time.  
%The reason is, that in some networks the defender strategically distributes the honeypots into the choking-points of the network
Also note, that the honeypot types used in strategy for $k=2$, namely the database honeypot, is used much less in the strategy for $k=3$.
Therefore, deciding which honeypots to deploy should be done strategically, as for different $k$ the deployed honeypot types may be very different.



% for two honeypots:
% DB, PC, SRV
% 0.37, 0.12, 0.51

% for three
% 0.02, 0.25, 0.73
