\subsection{Computing Optimal Attack Policy for Single Network using MDP}\label{sec:MDP}
This algorithm is used primarily in perfect information approximation, but its pruning techniques are used in algorithms for computing the attacker's best response strategy and CURB of information sets.
We represent the problem of computing the optimal attack policy for an attack graph $AG$ as a finite horizon Markov Decision Process (MDP)~\citep{bellman1956dynamic}.
The MDP for attack graph $AG$ is defined as a tuple $\langle A_{MDP}, S_{MDP}, P, \rho \rangle$, where:
\begin{itemize}
  \item $A_{MDP}=A\cup\{\text{T}\}$ is the set of actions equivalent to the action in attack graph $AG$ with additional terminal action $T$.
  \item $S_{MDP}$ is the set of states $s = (\alpha_s,\phi_s,\tau_s)$, where: $\alpha_s\subseteq A_{MDP}$ is the set of actions that the rational attacker can still perform, $\phi_s \subseteq F$ is the set of achieved facts, and $\tau_s\subseteq T$ is the set of host types that the attacker has interacted with so far. 
    The initial MDP state is $s_0=(A_{MDP},\emptyset,\emptyset)$.
\item $P^a_{ss'}\in[0,1]$ is the probability that performing action $a$ in state $s$ leads to state $s'$.
  Performing action $a$ may lead to three possible outcomes: 
  (i) $a$ interacts with a honeypot with probability $h=1-\prod_{t:\tau_a\setminus\tau_s}(\frac{n^t}{y^t+n^t})$
  and his attack immediately ends; 
  (ii) action $a$ does not interact with a honeypot and is successful with probability $p_a(1-h)$
  or (iii) action $a$ does not interact with a honeypot and neither is successful with probability $(1-p_a)(1-h)$.
  The sets defining the newly reached state are updated based on these outcomes.
\item $\rho^a_{ss'}$ is the attacker's reward for performing action $a$ in state $s$ leading to $s'$. 
  It is based on the set of facts that became true, the action cost $c_a$ and whether $a$ interacts with a honeypot.
  \end{itemize}

%  \begin{algorithm}
%  \caption{Euclid’s algorithm}
%  \label{euclid}
%     \begin{algorithmic}[1] % The number tells where the line numbering should start
%       \Procedure{Euclid}{$a,b$} \Comment{The g.c.d. of a and b}
%    \State $r\gets a \bmod b$
%      \While{$r\not=0$} \Comment{We have the answer if r is 0}
%     \State $a \gets b$
%      \State $b \gets r$
%     \State $r \gets a \bmod b$
%  \EndWhile\label{euclidendwhile}
%      \State \textbf{return} $b$\Comment{The gcd is b}
%     \EndProcedure
%     \end{algorithmic}
%  \end{algorithm}

  We compute the optimal policy in the MDP using backward induction based on the depth-first search with several pruning techniques to speed up the search.
  In every state $s\in S_{MDP}$ the algorithm computes the expected utility for every executable action from $a\in\alpha_s$, assuming that then the optimal policy is followed.
  The algorithm constructs the optimal policy by prescribing to state $s$ the action with the highest expected utility.
  To speed up the algorithm, we use various techniques which avoid computing the expected utility of the unpromising actions.
  First, we describe the techniques in high level, and in the remainder of this section in more details.

  {\bf Dynamic programming} significantly enhances the performance of the algorithm.
  Since the same states in the MDP can often be reached via more than one sequence of actions, we cache the expected rewards and corresponding policies of the visited states and reuse it when possible.
  In Figure~\ref{fig:realstrategy} can be seen the repetition of the subpolicies, which we computed only once.
  The second technique uses {\bf sibling-class theorem} in every state $s\in S_{MDP}$ to prune out the actions that are guaranteed to be suboptimal.
  Intuitively it states, that if in states $s$ the attacker can perform two actions and both have the same effect, he first executes the action with the highest success probability to cost ratio (the succeed-fast-and-cheap strategy).
  Similarly, if the attacker must perform two actions to reach fact $f$, he first executes the action with the highest fail probability to cost ration (the fail-fast-and-cheap strategy).
  Third, we apply {\bf branch and bound} techniques to further eliminate not promising actions in state $s$.
  For that, we compute \emph{lower} and \emph{upper bounds} of the expected utility of each action in $s$.
  If the upper bound of action $a$ is lower than the lower bound of action $b$, we prune out $a$.
  %In the rest of this section, we describe the sibling-class theorem and computation of the lower and upper bound of a state, which is used in branch and bound techniques.


\subsubsection{Sibling-Class Pruning}

The complexity of the optimal attack planning originates in the need to explore all possible orderings of actions.
The sibling-class theorem states that in some situations, the optimal order for executing actions can be determined directly without any search.
This theorem was proven in \citep{greiner2006finding} in the context of ``probabilistic AND-OR tree resolution" (PAOTR).
The AND part of the theorem is proven in the context of simplified attack graphs in \citep{buldas2012upper}.
In both of these works, the actions can be present only in the leafs of the AND-OR graph, without any preconditions.
The inner nodes represent only the conjunctions and disjunctions and do not have any costs or probabilities of success.
Moreover, the theorem in \citep{greiner2006finding} is proven only for AND-OR trees in which no node can have more than one parent.
There is no notion of reward, and the AND-OR tree is evaluated until the root of the tree is resolved.

In the attack graphs generated by existing network security tools (e.g., MulVAL \citep{ou2006scalable}), attack actions are present in the inner nodes of the attack graphs, which can also be cyclic graphs.
Furthermore, we assume that the attacker can obtain intermediate rewards for activating subsets of fact nodes and not only for reaching the root of the attack graph.
Here we generalize the sibling-class theorem to handle these additional cases.
We provide only a proof sketch here based on the original proof.
We find that the original proof holds with minor modifications.
To describe the theorem, we first need to define several additional concepts.

%\kdnote{Add intuition of sibling class theorem}

\begin{definition}
 \emph{OR-sibling class} is a set of executable actions each of which have the same set of effect facts $f \subseteq F$ in the attack graph and do not have any other effect besides $f$.\\
 \emph{AND-sibling class} is a set of executable actions $X\subseteq A$ in the attack graph, such that there is a ``grandchild'' action $g\in A$ that can be executed if an only if all of the actions in $X$ are successful ($\pre(g) \subseteq \bigcup_{a\in X}\eff(a) \;\;\&\;\; \forall b\in X\; pre(g) \setminus \bigcup_{a\in X\setminus\{b\}}\eff(a)\neq \emptyset$) %\kdnote{pre(g) - some may already by satisfied}% 
 and none of the actions has an effect that would help enable other actions, besides $g$:
 \[\forall a_1,a_2\in X; g_1,g_2\in A \;\; \eff(a_1) \subseteq \pre(g_1) \& \eff(a_2) \subseteq \pre(g_2) \Rightarrow g_1=g_2.\]
\end{definition}

\begin{definition}
 We define the \emph{R-ratio} of actions in sibling classes as
 \begin{align}
  R(a) &= \frac{p_{a}}{c_a} \textrm{ if action $a$ is in OR-sibling class; and} \\
  R(a) &= \frac{{1-p_a}}{c_a} \textrm{ if action $a$ is in AND-sibling class.}
 \end{align}
\end{definition}

The sibling theorem states that actions in the optimal policy for an attack graph that belong to the same sibling class are always executed in decreasing order of their R-ratios.

\begin{theorem}[Sibling class theorem]
 Let $AG$ be an attack graph and let $\xi^*$ be the optimal policy for the attack graph. 
 Then for any actions $x,y$ in the same sibling class, such that $R(y)>R(x)$, $x$ is never performed before $y$ in $\xi^*$. 
\end{theorem}
The full proof is presented in Appendix of the paper.
%\kdnote{present intuition of the proof}

Therefore, when we consider which action to perform at a decision point we can consider only those with the highest R-ratios in each sibling-class and those that have multiple parents or grandparents.
After some actions in the attack graph are executed the inner nodes in the attack graph can become leaf nodes and the sibling theorem is then applied to those nodes as well.
Unfortunately, the attack graphs with honeypots violate the monotonicity property of actions (assumed in the proof of Sibling Class Theorem).
These actions cannot be pruned or preferred to other actions, therefore, we apply sibling-class theorem only to actions that do not interact with honeypots.

\subsubsection{Branch and Bound Technique}
The branch and bound needs to compute the lower and upper bounds on the expected utilities of the states.
The {\bf lower bound} $LB$ in state $s$ we set to the exact expected utility of some action $a\in\alpha_s$ performed in $s$.
If there is no action for which the exact expected utility has been computed then lower bound is set to zero, which corresponds to the termination action $a_T$.

The {\bf upper bound} for state $s$ we compute by performing a depth-first search on the attack graph relaxed in two ways.
We set action costs to zero, and the probabilities of touching honeypot to zero.
One can easily see that this can only increase the attacker's expected utility in the modified attack graph.
%Moreover, in this setting the optimal attack policy executes \emph{all} actions available to the attacker in any possible order, since all attack policies result in the \emph{same} expected probability of compromising each host type in the network.
In the relaxed attack graph for each fact node $f$ with reward $r_f>0$, we compute the upper bound on the probability that fact $f$ becomes true.
The probability is determine by calculating the probability that at least one of the actions in $\{a\in A | f\in\eff(a)\}$ will have all its preconditions $\pre(a)$ satisfied \emph{and} action $a$ will be performed successfully.
This leads to a recursive computation of the probabilities that the action's preconditions in $\pre(a)$ becomes true, etc.
The algorithm terminates either by reaching the facts in $\phi_s$ that are already true, or reaching the facts that cannot be satisfied ($\forall a\in\alpha_s: f\not\in\eff(a)$).
To avoid complications caused by cycles, during the traversal we restrict each fact node to be precondition of at most one action in the attack graph.
Removing preconditions for the actions can only increase the resulting probability because fewer facts are required to fulfill the preconditions of that action.
Let $P_{true}[f|s]$ be the computed upper bound on the probability that fact $f$ will become true in state $s$.
The final upper bound on the expected utility is $\sum_{f\in \{f\in F|r_f>0\}} P_{true|s}[f]r_t$.
We use standard branch and bound technique to prune out provably suboptimal actions.

When we compute the expected utility of $a$, which was not pruned out, we first compute the expected utility of the success branch of the action.
This allows us to compute tighter lower bound for actions failing branch.
Namely, we bound it by $LB=\frac{M+c_a-p_aU_a(\xi_{a^+}+IR(\xi_{a^+}))}{1-p_a}$, where $M$ is the highest expected utility for an action in $s$ for which the exact expected utility has been computed.

% Vaclavske nam. 12, 17;30 - Prucha zvonek.

Our algorithm for computing the optimal attack policies showed to be significantly faster than the competing domain-independent (PO)MDP solvers, as was shown in \citep{durkota2014}.
We used this algorithm for computing the perfect information heuristic, but its pruning techniques we reuse in the other algorithms for computing attacker's attack policies.

%We also use the cache to compute all optimal attack policies within a game, as their depth-first search trees may reach the same MDP states as well.


%Another useful properties of the optimal attack policies that we use are as follows:
%\begin{proposition}\label{prop:1}
%In the optimal policy $\xi^*$ for every (sub)policy $\xi^*_n$ rooted at a node $n$ labeled with action $a=action(n)$ the following is true: $U{(\xi^*_n)} \ge U{(\xi^*_{n^-})}$.
%\end{proposition}
%
%\begin{proposition}\label{prop:2}
%  Let $\xi^*_n$ be the subpolicy of an optimal policy rooted at a node $n$ with action $a=action(n)$, then: $U(\xi^*_{n^-}) \leq U(\xi^*_{n^+})+IR(\xi_n)-c_a/p_a$.
%\end{proposition}
%Find the proofs in the Appendix.

%The use of the sibling theorem is demonstrated by the following example.
%Assume we compute first action that the attacker should perform in attack graph in Figure~\ref{fig:attackgraph}.
%Note, that actions \texttt{Exploit Firewall} and \texttt{Send MW Email} belong to the same sibling class, while action \texttt{Create Dictionary} belong in the separate sibling class.
%Sibling class theorem states, that if optimal attack strategy begins with the action from the first sibling class, than it must start with action \texttt{Send MW Email}, since $R(\texttt{Send MW Email})=\frac{0.23}{2}=0.115 > 0.054=\frac{0.27}{5}=R(\texttt{Exploit Firewall}$.
%Therefore, we have to explore only possibilities of choosing action \texttt{Create Dictionary} or \texttt{Send MW Email}.

%\noindent {\bf Sibling-Class Theorem with Honeypots}

%Consider the counterexample depicted in Figure~\ref{fig:counterexample} with two actions $a$ and $b$.
%Action $a$ succeeds with probability $p_a=1$, costs $c_a=10$, thus its $R(a)=1/10=0.1$, and interacts with host of type 1.
%Action $b$ succeeds with probability $p_b=0.5$, costs $c_b=1$, thus its $R(b)=0.5/1=0.5$, and interacts with host of type 2.
%Both types $t={1,2}$ consist of 2 hosts, one real $y_t=1$ and one honeypot $x_t=1$, therefore, have probability $h=0.5$ to interact with a honeypot for each action.
%Since both actions have the same effect, they belong to one OR-sibling class $O^{OR}$.
%Despite the fact that $R(b) > R(a)$, in which case the sibling-class theorem states that in the optimal to begin with action $b$, because of the presence of honeypots, it is optimal to begin with action $a$ with expected reward 40.
%Swapping the action ordering would result in expected reward only 34.
%The reason is that if first action $b$ interacts with a honeypot, the attacker will not get a change to perform action $a$ anymore.
%Thus, he betters off by starting with action $a$, despite the fact that its R-ratio is lower than the action $b$'s.
%Therefore, we use the sibling-class theorem only for actions that do not interact with a honeypot.
%Note, that action does not interact with a honeypot either if there are no honeypots of the types $\tau_a$ to action interacts with ($\forall t\in\tau_a:x_t=0$), or previous actions interacted with real hosts of types $\tau_a$.
%In appendix we provide this example in details.














\subsection{Optimal Attack Policy for Multiple Network in Information Set}\label{sec:POMDP}
The previous algorithm assumes that the MDP states can be perfectly observed.
We extend the  existing algorithm to find the optimal attack policy for a set of networks with a \emph{known} prior probability distribution over them.
This algorithm is essential to compute the attacker's best response strategy $BR_a(\sigma_d)$ against the defender's strategy $\sigma_d$.

We translate the problem of finding the optimal attack policy in the information set $I$ into a POMDP problem.
Instead of computing the backward induction algorithm on a single MDP, we compute it concurrently in all MDPs, one per network in the information set.
The same action in different MDPs may have different transition probabilities, so we use Bayes rule to update the probability distribution among the MDPs based on the action probabilities.
%Let $J$ be the number of MDPs and let $\beta_j(o)\in\mathbb{R^+}$ be the probability that the attacker is in state $o$ and in MDP $j=1,\dots,J$. % in information set $I$. %, so that $\sum_{z_i\in I}b(z_i)=1$.
%Performing action $a$ leads to a new state $o'$ with probability $P_j(o,o',a)$. %in information set $I$, leads to a new information set $I'$.
%The updated probability of being in $j$-th MDP given state $o'$ is $\beta_j(o') = \frac{P_j(o,o',a) \beta_j(o)}{\sum_{j'=1}^{J}P_{j'}(o,o',a)\beta_{j'}(o)}$. 
%\emph{For example}, let state $o$ be the first MDP state in each information set $I_1$ in Figure~\ref{fig:game} with probabilities $\beta_1(o)=0.5$ and $\beta_2(o)=0.5$.
%The probability of successfully performing action $3$ is 0.2 in the first MDP and 0.6 in the second.
%After performing successfully action $3$, which leads to state $o'$ (the information set $I_2$ in Figure~\ref{fig:game}) we update the probabilities as $\beta_1(o')=\frac{0.2b_1(o)}{0.2\beta_1(o) + 0.4\beta_2(o)}=\frac{1}{3}$ and $b_2(o')=\frac{0.4\beta_2(o)}{0.2b_1(o)+0.4\beta_2(o)}=\frac{2}{3}$.

This algorithm returns single attack policy $\xi$ with the highest expected utility given the probability distribution over the states in $I$.
During the computation, we use similar pruning techniques to those described in Section~\ref{sec:MDP}.
The only modifications are those to ensure that the attacker chooses best response in favor of the defender by maximizing defender's reward $R_d(\xi,z)$ in case of a tie for the attacker.
%This algorithm is at least NP-hard since finding optimal attack policy for one MDP is already NP-hard.
%We use this algorithm as a subroutine in the SO algorithm for ZS and GS games.


%\begin{definition}
%  A attack policy $\pi_a\in\Pi_a$ is \emph{rationalizable} if and only if $\exists \sigma_d\in\Sigma_d: \pi_i\in BR_a(\sigma_d)$.
%\end{definition}

%\begin{definition}
%  $CURB(I)\subseteq\Xi(I)$ is a set of rationalizable (undominated) pure attack policies in information set $I$.
%  Attack policies in $\Xi(I)\setminus CURB(I)$ are called \emph{dominated}.
%\end{definition}


\subsection{Computing CURB}
In this section, we describe the algorithms that we use to compute a set of attack policies in CURB $\kappa(I)$ for each information set $I\in\topinfo$ in our game.
We use a variant of the \emph{incremental pruning} algorithm~\citep{cassandra1997incremental}, a backward induction algorithm, which in every attacker's decision state propagates the CURB set of attack policies.
%Let $A(I)$ be a set of atomic actions available to the attacker in information set $I$.
In every information set $I\in\allinfo$, we first generate set of policies $S(I)$, from which we consequently eliminate all dominated policies, and propagate them upwards in POMDP search tree.
The Algorithm~\ref{alg:curb} generates CURB $\kappa(I)$ for specific $I$.

\begin{algorithm}
	\caption{Computes CURB given the information set $I$.}\label{alg:curb}
\begin{algorithmic}[1]
	\Function{$\kappa$}{$I$}
 \State $S(I) \leftarrow \emptyset$
 \ForAll{$a\in A(I)$}
  \State $I_{a,succ}\leftarrow \textrm{perform}(I,a,succ)$
  \State $I_{a,fail}\leftarrow \textrm{perform}(I,a,fail)$
 \ForAll{$\xi_1\in \kappa(I_{a,succ})$}
 \ForAll{$\xi_2\in \kappa(I_{a,fail})$}
  \State $\xi'\leftarrow \textrm{policy}(a,\xi_1,\xi_2)$
  \State $S(I) \leftarrow S(I) \cup \{\xi'\}$
 \EndFor
 \EndFor
 \EndFor
  \State $S(I) \leftarrow S(I) \cup \{\textrm{policy}(a_T, \emptyset, \emptyset)\}$
  \State $\kappa(I) \leftarrow \textrm{ExactCURB}(S(I))$
	%\State $\kappa^*(I) \leftarrow \textrm{ApproximatedCURB}(\kappa(I))$
	\State \Return{$\kappa(I)$}
 \EndFunction
\end{algorithmic}
\end{algorithm}

Function perform$(I,a,succ)$ (resp. perform$(I,a,fail)$) returns the information set reached by the attacker after action $a$ is successfully (resp. unsuccessfully) performed in $I$.
Function policy$(a,\xi_1,\xi_2)$ returns a new policy which starts with action $a$ and follows with $\xi_1$ (resp. $\xi_2$) if $a$ succeeds (resp. fails).
For each action $a\in A(I)$ the algorithm computes CURB for the case that the action succeeds ($\kappa(I_{a,succ})$) and the case it fails ($\kappa(I_{a,fail})$).
Then it creates set of policies $S(I)$ as combination of policies which start with action $a$ and continue with $\xi\in\kappa(I_{a,succ})$ if $a$ succeeds and with $\xi'\in\kappa(I_{a,fail})$ if $a$ fails.
Function ExactCURB eliminates policies from $S(I)$ that are not in the $\kappa(I)$ described in next section.
%Function ApproximatedCURB further eliminates policies, such that their removal will change the player utilities no more than $\epsilon$.
%Both functions are explained in the rest of this section.

\subsubsection{Exact CURB}
Let $U_a(\xi,z)$ be the attacker's utility for choosing policy $\xi\in \Xi(I)$ in a state $z\in I$.
If there exists probability distribution over $z\in I$, such that $\xi^*$ yields maximal expected utility to the attacker among possible policies $\Xi(I)$, then $\xi$ is rationalizable and $\xi\in\kappa(I)$.
Otherwise, we can eliminate policy ($\xi\not\in\kappa(I)$).
We find such probability distribution over $z\in I$ using linear feasibility program (LFP) from \citep{benisch2010algorithms}:

\begin{subequations}\label{lp:1}
  \begin{align}
   %\textrm{max: (no objective)} \\
   \textrm{s.t.}: & (\forall \xi\in \Xi(I)\setminus \{\xi^*\}): \sum_{z\in I}U_a(\xi^*,z)p_z \geq \sum_{z\in I}U_a(\xi,z)p_z \\
   & (\forall z\in I): 0 \leq p_z \leq 1 \\
   & \sum_{z\in I} p_z = 1 
  \end{align}
\end{subequations}

Variables are $p_z$ for each $z\in I$.
LFP finds probabilities $p_z$ over $I$, such that policy $\xi^*$ is a best response.
If LFP does not have a feasible solution, there is no probability distribution, where policy $\xi^*$ would be a best response.
Therefore, $\xi^*\not\in \kappa(I)$, and it can be eliminated; otherwise $\xi^*\in \kappa(I)$. 
In Algorithm~\ref{alg:exact} we show how to compute the exact CURB.
\begin{algorithm}
	\caption{Computes exact CURB given set of policies $S(I)$.}\label{alg:exact}
\begin{algorithmic}[1]
 \Function{ExactCURB}{$S(I)$}
 \ForAll{$\xi\in S(I)$}
  \State $\kappa(I)\leftarrow\emptyset$
  \State  $LP\leftarrow $SolveLP-$\ref{lp:1}(\xi,\Xi(I))$
  \If{ $LP$ has feasible solution}
   \State $\kappa(I)\leftarrow\kappa(I)\cup\{\xi\}$
  \EndIf
 \EndFor
 \EndFunction
\end{algorithmic}
\end{algorithm}
For each action it tests if there exists probability distribution where that action is rationalizable.
SolveLP-\ref{lp:1}$(\xi,S(I))$ builds and solves Linear Feasibility Program~\ref{lp:1}.
Algorithm~\ref{alg:exact} already eliminates the vast majority of unnecessary attack policies, however, in some informations sets it still results in a large set of policies, where many policies are best responses in a very little probability region or even in a single point.

%\kdnote{Consider adding captions for algorithms.}
\subsubsection{Approximated CURB}
In order to reduce the strategy space we allow the attacker and the defender to make a small $\epsilon$ errors.
Intuitively, if removing strategy $\xi^*$ will not cause a utility difference more than $\epsilon$ for either player, then we remove it from $\kappa(I)$.
This is similar to weaker-than-weak elimination of dominated strategies studied for Nash equilibrium in \citep{cheng2007iterated}.
%Algorithm~\ref{alg:approx} ApproximatedCURB$(\kappa(I))$ is as follows:

\begin{algorithm}
	\caption{It computes approximated CURB given the set of policies $S(I)$ and an error parameter $\epsilon$.}\label{alg:approx}
\begin{algorithmic}[1]
 \Function{ApproximatedCURB}{$S(I), \epsilon$}
  \ForAll{$\xi^*\in S(I)$}
   \State $\epsilon_a \leftarrow$ AttackerError$(\xi^*,S(I))$
   \If{$\epsilon_a\leq\epsilon$}
    \ForAll{$\xi'\in S(I)\setminus\{\xi^*\}$}
     \State $\epsilon_d\leftarrow$ DefenderError$(\xi,\xi',S(I))$
     \State $\epsilon_d^{max}=\max\{\epsilon_d^{max},\epsilon_d\}$
    \EndFor
    \If{$\epsilon_d^{max}\leq\epsilon$}
     \State $\kappa^*(I)\leftarrow \kappa^*(I)\setminus\{\xi^*\}$
    \EndIf
   \EndIf
  \EndFor
	\State \Return{$(\kappa^*(I),\epsilon_a,\epsilon_d^{max})$}
 \EndFunction
\end{algorithmic}
\end{algorithm}

The Algorithm~\ref{alg:approx} computes the attacker's upper bound on utility loss $\epsilon_a$ that the removal of policy $\xi^*$ will incur for the attacker (line~3).
If $\epsilon_a$ is less than or equal to $\epsilon$, $\xi^*$ is a candidate for removal from $\kappa(I)$.
The algorithm considers all alternative policies $\xi'$ (line 5) that the attacker will play \emph{instead} of $\xi^*$.
For each $\xi'$ it computes the upper bound on the defender's maximal utility difference $\epsilon_d$ (line 6) and finds its maximum $\epsilon_d^{max}$ (line 7).
If $\epsilon_d^{max}$ is less than or equal to $\epsilon$, the algorithm removes policy $\xi^*$ from $\kappa(I)$.

The algorithm returns set of policies $\kappa^*(I)$ with the upper bound on attacker's and defender's utility errors $\epsilon_a$ and $\epsilon_d^{max}$, respectively.
These errors are used to calculate the defender's and attacker's errors $\epsilon_a(\xi,I)$ and $\epsilon_d(\xi,I)$, respectively, for following policy $\xi$ in I, which are used in linear programs~\ref{lp:2} and \ref{lp:3}.
We compute $\epsilon_a(\xi,I)=p_a\epsilon_a + (1-p_a)\epsilon_a'$, where $\epsilon_a$ (reps. $\epsilon_a'$) is the error in the information set reached after action $a$ succeeds (resp. fails) in $I$.
We similarly compute errors $\epsilon_d(\xi,I)$ using $\epsilon_d^{max}$ (resp. $\epsilon_d^{max}$).
%\kdnote{Explain how the errors in $I$ are reflected to $\epsilon_{d,a}(\xi,I)$?}
In Algorithm~\ref{alg:approx} function AttackerError$(\xi^*,\kappa(I))$ computes the attacker's maximal error by building and solving Linear Program~\ref{lp:2}.
The attacker's upper bound on the utility error $\epsilon_a$ is computed as follows: 
\begin{subequations}\label{lp:2}
 \begin{align}
  \textrm{max: }\epsilon_a \\
  \textrm{s.t.}: & (\forall \xi\in \kappa(I)\setminus \{\xi^*\}): \nonumber \\
  &\hspace{1cm}\sum_{z\in I}U_a(\xi^*,z)p_z + \epsilon_a(\xi^*,I) - \epsilon_a \geq \sum_{z\in I}U_a(\xi,z)p_z - \epsilon_a(\xi, I) \label{lp2:dom} \\
  & (\forall z\in I): 0 \leq p_z \leq 1 \\
  & \sum_{z\in I} p_z = 1
 \end{align} 
\end{subequations}
The linear program finds the probability distribution $p_z$ over $I$, where $\xi^*$ is undominated (constraint~\ref{lp2:dom}).
It maximizes the difference $\epsilon_a$ that the attacker receives for choosing $\xi^*$ and all other policy in $\kappa(I)\setminus\{\xi^*\}$.
$\epsilon_a$ includes policy errors $\epsilon_a(\xi^*,I)$ and $\epsilon_a(\xi,I)$ ($\epsilon_a(\xi,I)$ is constant for each policy $\xi$).
In Algorithm~\ref{alg:approx} function DefenderError$(\xi^*,\xi',\kappa(I))$ computes the upper bound on the defender's error in utility by constructing and solving linear program~\ref{lp:3}:
\begin{subequations}\label{lp:3}
  \begin{align}
   \textrm{max: }\epsilon_d \\
   \textrm{s.t.}: & (\forall \xi\in \kappa(I)\setminus \{\xi^*\}): \nonumber \\
   &\hspace{1cm}\sum_{z\in I}U_a(\xi^*,z)p_z + \epsilon_a(\xi^*,I) \geq \sum_{z\in I}U_a(\xi,z)p_z  - \epsilon_a(\xi,I) \label{lp:3astar} \\
   & (\forall \xi\in \kappa(I)\setminus \{\xi^*,\xi'\}): \nonumber \\
   &\hspace{1cm}\sum_{z\in I}U_a(\xi',z)p_z + \epsilon_a(\xi',I) \geq \sum_{z\in I}U_a(\xi,z)p_z - \epsilon_a(\xi,I) \label{lp:3alta}  \\
   & | \sum_{z\in I}R_d(\xi^*,z)p_z - \sum_{z\in I}R_d(\xi',z)p_z | + \epsilon_d(\xi',I) + \epsilon_d(\xi^*,I) \geq \epsilon \label{lp:3diff} \\
   %& \epsilon_d(\xi^*,I) + \epsilon_d(\xi',I) + \epsilon = \epsilon_d^{\xi'} \label{lp:3sum} \\
    & (\forall z\in I): 0 \leq p_z \leq 1 \\
    & \sum_{g\in G} p_s = 1 
  \end{align}
\end{subequations}
where $R_d(\xi,z)$ denotes the defender's reward if the attacker performs policy $\xi$ in state $z$.
It computes the defender's upper bound on the maximal utility difference between $\xi^*$ and $\xi'$, where $\xi'$ becomes a new best response policy.
Constraint~(\ref{lp:3astar}) restricts LP to the region, where $\xi^*$ could dominate, taking into account the possible policy errors ($\epsilon_d(\xi',I)$ and $\epsilon_d(\xi^*,I)$).
Constraint~(\ref{lp:3alta}) further restricts LP to the region, where $\xi'$ is a new rationalizable policy (yields higher utility than any alternative policy $\xi\in\kappa(I)\{\xi^*\}$).
Constraint~(\ref{lp:3diff}) ensures that $\epsilon_d$ finds the maximal upper bound on the defender's reward difference between policy $\xi^*$ and $\xi'$, including the policy errors.

The full algorithm for computing the approximated CURB is the same as algorithm for computing the exact CURB $\kappa(I)$ in Algorithm~\ref{alg:curb}, except that on line 14 we call ApproximatedCURB (Algorithm~\ref{alg:curb}) with parameter $\epsilon$ instead the ExactCURB (Algorithm~\ref{alg:approx}).
Function ApproximatedCURB sequentially eliminates the policies from $S(I)$, therefore, the players' errors can sum to more than $\epsilon$.
We can control the total error by discontinuing the further policy eliminations from $S(I)$, when the error reaches a desired upper bound.
Experimentally, the accumulated errors are shown to be neglectable.

