%!TEX root = main.tex

\section{Experiments}
We experimentally evaluate and compare our proposed approximation models and the corresponding algorithms.
Namely we examine: (i) Perfect information (PI) approximation solved using backward induction described in Sec.~\ref{sec:pi}, (ii) the zero sum approximation games solved with SO algorithm, which we refer to as to SOZS1 through SOZS4 where the number corresponds to the variant of the ZS approximation, (iii) SO algorithm applied on GS game, referred as to SOGS, and (iv) Single Linear Program (SLP) as described in Sec.~\ref{sec:slp}.
We also compute the defender's upper bound utility, SSEUB, and use it as reference point to evaluate the strategies found by the other approximations. 

The structure of the experimental section is as follows: in Sec.~\ref{sec:setting} we describe networks we use to generate the action graph game, in Sec.~\ref{sec:anal} we discuss an issue of combinatorially large CURB sets for one of the networks, in Sec.~\ref{sec:scale} we analyze the scalability of the approximated models, in Sec.~\ref{sec:util} we analyze the quality of the strategies found by the approximated models and in Sec.~\ref{sec:qualityZS} we analyze how the strategies calculated by the approximated models of ZS games depend on how close the games are to being zero-sum.
In Sec.~\ref{sec:sensitivity} we investigate the defender's regret for imprecisely modeling the attack graph, and conclude with a case-study analysis in Sec~\ref{sec:case}.

\subsection{Networks and Attack Graphs}\label{sec:setting}
We use three different computer network topologies.
Two of them are depicted in Fig.~\ref{fig:net}, \emph{small business} (Fig.~\ref{fig:netBusiness}) and \emph{chain} network (Fig.~\ref{fig:netChain}). % and a \emph{unstructured} network.
Connections between the host types in the network topology correspond to pivoting actions for the attacker in the attack graph (from the compromised host the attacker can further attack the connected host types).
We vary the number of vulnerabilities of each host type, which is reflected in the attack graph as an attack action per vulnerability.
We generate the actions' success probabilities $p_a$ using the MulVAL that uses Common Vulnerability Scoring System.
Action costs $c_a$ are drawn randomly in the range from 1 to 100, and host type values $r_t$ and the cost for honeypot $c^h_t$ of host type $t$ are listed in Table~\ref{tab:b552}.
We assume that the more valuable a host type is the more expensive it is to add a HP of that type.
We derive honeypot costs linearly from the host values with a factor of 0.02.
The basis network $b$ for the business and chain network consists of the black host types in Fig.~\ref{fig:net}.
We scale each network by adding the remaining depicted host types and then by additional workstations.
We also scale the total number of hosts $n$ in the network and the number of honeypots $k$.
Each parameter increases combinatorially the size of the game.  

%{\footnotesize
	\begin{table}[t!]
		\centering
		\caption{Host type values and costs for deploying them as honeypots.}
		\label{tab:b552}
		\begin{tabular}{ | l | c | c|c|c| r | }
			\hline
			Host type $t$ & database & firewall & WS$_n$ & server \\ \hline
			Value of host type $r_t$ & 5000 & 500 & 1000 & 2500 \\ \hline
			Cost for deploying HP of host type $c^h_t$& 100 & 10 & 20 & 50 \\ \hline
		\end{tabular}
	\end{table}
%}


The third network topology is the \emph{unstructured} network, where each host type has a direct connection to the internet and there is no connection among the host types.
The attack graph consists of one attack action $t$ per host type $T$, which attacker can perform at any time.
For the unstructured network we create diverse attack graphs by drawing: host types values uniformly from $r_t\in[500,1500]$, action success probabilities uniformly from $p_t\in[0.05,0.95]$ and action costs uniformly from $c_t\in[1,r_tp_t]$.
We restrict the action costs from above with $r_tp_t$ to avoid the situations where an action is not worth performing for the attacker, in which case the attack graph can be reduced to a problem with $|T|-1$ types.
The basis network $b$ consists of two randomly chosen host types.

All experiments were run on a 2-core 2.6GHz processor with 32GB memory limitation and 2 hours of runtime.

\subsection{Analytical Approach for CURB for Unstructured Network}\label{sec:anal}
The incremental pruning algorithm described in Sec.~\ref{sec:CURBasPOMDP} generates a very large number of attack policies in the CURB set for the unstructured network.
In order to be able to compute the upper bound for solution quality for larger game and in order to understand the complexities hidden in CURB computation, we analyze this structure of the curb for this simplest network structure formally.
In Fig.~\ref{fig:curbPrev} we show an example of the attacker's utilities for the policies in a CURB set generated for an information set with two networks.
Let $h_t$ be the probability that action $t$ interacts with a honeypot.
On the x-axis is probability distribution space between two networks, one with $h_t=0$ and other with $h_t=1$.
The y-axis is the attacker's utility for each attack policy in the CURB.
The algorithm generates the attack policies known as \emph{zero optimal area policies} (ZAOPs)~\cite{littman1994witness}, denoted with dashed lines in the figure.
A policy is ZAOP if and only if it is an optimal policy at a single point in the probability space (dashed policies in Fig.~\ref{fig:curbPrev}).
The property of ZAOP is that there is always another policy in the CURB set with strictly larger undominated area.
%Therefore, in the witness algorithm the ZAOPs can be removed, although, they are hard to detect.
It raises two questions: (i) can we remove ZAOPs from the CURB set and (ii) how to detect them.
Recall that in SSE the attacker breaks ties in favour of the defender. % plays his best response and in case he is indifferent between two actions he plays the one that yields higher utility for the defender.
Therefore, we can discard ZAOP as long as we keep the best option for the defender. 

Further analysis showed that ZAOPs occur when $h_t=1-\frac{c_t}{p_tr_t}$ (at probability 0.69 and 0.99 in Fig.~\ref{fig:curbPrev}).
It is because the expected reward of action $t$ at that point is $p_t(1-h_t)r_t-c_t = p_tr_t\frac{c_t}{p_tr_t} - c_t = 0$, which means that the attacker is indifferent whether to perform action $t$ or not. 
The algorithm at probability $h_t=1-\frac{c_t}{p_tr_t}$ generates the set of attack policies with all possible combinations where the attacker can perform action $t$ in the attack policy.
Let $P(t)$ be the probability that the attacker performs action $t$ in an attack policy.
The defender's utility for action $t$ is $-r_tp_t(1-h_t)P(t) = -c_tP(t)$.
Because the attacker breaks ties in favour of the defender, at $h_t=1-\frac{c_t}{p_tr_t}$ the attacker will choose not to perform action $t$ and we can keep only the policy that does not contain action $t$.

Furthermore, we categorize each action $t$ based on $h_t$ to one of three classes: to class A if $h_t=0$, to class B if $0<h_t<1-\frac{c_t}{p_tr_t}$ and to class C if $1-\frac{c_t}{p_tr_t}<h_t$.
In an optimal attack policy: (i) all actions from A are performed first and any of their orderings yield the same expected utility for the attacker, (ii) all actions from B are performed and their order is in increasing order of $\frac{p_t(1-h_t)r_t-c_t}{h_t}$ ratios, and (iii) none of the actions from C are performed, as they yield negative expected reward for the attacker.
We partition all probability distributions into regions $h_t = 1-\frac{c_t}{p_tr_t}$ and in each region we assign actions to the classes.
We find the set of optimal attack policies for each region.
The attack policies in one region differ from each other in ordering of the actions in B.

In Fig.~\ref{fig:beliefClassed} we show an example of the probability distribution space of three networks in an information set.
The probabilities that actions $1,2$ and $3$ interact with a honeypot represent a point $(h_1,h_2,h_3)$. 
We partition the probability space and assign each action to a class.
In all experiments we use this approach to generate the CURB set without ZAOPs in games for unstructured networks.

%Therefore, we can eliminate all ZAOPs that are generated as combinations of places where to perform action $t$.

%We use more analytical approach to generate the curb for the information set by partitioning the probability space it into regions with borders $h_i = 1-\frac{c_i}{p_ir_i}$ and within each region we compute set of optimal attack policies for that region.
%In Fig.~\ref{fig:beliefClassed} we present an example of a belief space made of three states in the information set, and its partition into four regions.
%In Fig.~\ref{fig:curbPrev} and Fig.~\ref{fig:curbAfter} we compare the CURB sets generated with POMDPs and the analytical approach.
%That crucial part is the upper envelope of the CURBs which is identical for both approaches.
%In all experiments we use analytical approach for the unstructured network which increases its scalability.


%While running SLP approximation on unstructured network we ran into a issue that POMDPs approach, described in Sec.~\ref{sec:slp}, can generate exponential number of undominated attack policies in the CURB for the attacker's information sets.
%However, the exponential explosion of the undominated attack policies is in a single belief state in a continues belief space of honeypot deployments.
%Specifically, assume that action $i$ interacts with a honeypot with probability $h_i = 1-\frac{c_i}{p_ir_i}$.
%The expected reward of such action is $p_i(1-h_i)r_i-c_i = p_ir_i\frac{c_i}{p_ir_i} - c_i = 0$, which means that either performing this action nor not at the bottom of the attack policy yields the same utility for the attacker.
%For this belief the POMDP search returns set of attack policies which differ one from another with combination of leafs where action $i$ is performed.
%Therefore, we use rather analytical approach to generate the CURB for the information set by partitioning it into regions with borders $h_i = 1-\frac{c_i}{p_ir_i}$ and within each region we compute set of optimal attack policies for that region.
%In Fig.~\ref{fig:beliefClassed} we present an example of a belief space made of three states in the information set, and its partition into four regions.
%In Fig.~\ref{fig:CURBPrev} and Fig.~\ref{fig:curbAfter} we compare the CURB sets generated with POMDPs and the analytical approach.
%That crucial part is the upper envelope of the CURBs which is identical for both approaches.
%In all experiments we use analytical approach for the unstructured network which increases its scalability.

\begin{figure}
	\centering
	\begin{subfigure}{.49\textwidth}
		\includegraphics[width=1\textwidth]{figForCurb/data2prev-dashed}
		\caption{}
		\label{fig:curbPrev}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\includegraphics[width=1\textwidth]{figForCurb/beliefSpaceClassed}
		\caption{}
		\label{fig:beliefClassed}
	\end{subfigure}
	%\begin{subfigure}{.3\textwidth}
	%	\includegraphics[width=1\textwidth]{figForCurb/data2after}
	%	\caption{}
	%	\label{fig:curbAfter}
	%\end{subfigure}
	\caption{(a) Attack policies from a CURB set for an information set for the unstructured network. (b) Probability space partitioning by action belonging into the categories. }
	\label{fig:comparison}
\end{figure}


\subsection{Scalability}\label{sec:scale}
In this section we compare the runtimes of the algorithms.
We present the mean runtimes (x-axis in logarithmic scale) for each algorithm on business (Fig.~\ref{fig:busTime}), chain (Fig.~\ref{fig:chainNsynTime} top) and unstructured (Fig.~\ref{fig:chainNsynTime} bottom) with of 5 runs (the runtimes were almost the same for each run).
We increased the number of host types $T$, number of hosts $n$ and number of honeypots $k$.
The missing data indicate that the algorithm did not finish within a 2 hour lime limit.
%For algorithms that did not finish within the two hour limitation we show empty bar in Fig.~\ref{fig:busTime}.
%If approximation algorithm did not finish within two hours, we leave an empty space in the bar charts.
From ZS approximations we show only SOZS4 since the others (SOZS1, SOZS2 and SOZS3) had almost identical runtimes.

From the results we see that least scalable are SOGS and SLP approach.
SOGS is very slow due to the computation time of the MILP.
Surprisingly, in some cases the algorithm solved more complex game (in Fig.~\ref{fig:chainNsynTime} $T=7,n=7,k=3$) and not the simpler game (in Fig.~\ref{fig:chainNsynTime} $T=7,n=7,k=1$).
The reason is that the more complex game requires 3 iterations to converge, while the simpler games required over 5 iterations, after which the restricted game became too large to solve the MILP.
The SLP algorithm was the second worst.
The bottle-neck is in the incremental pruning algorithm subroutine, which took on average 91\% of the total runtime for the business network and 80\% for the chain network.
In the unstructured network the problem specific CURB computation took only about 4\% of total runtime.
The algorithms for ZS1-ZS4 and PI approximation showed the best scalability.
Further scaling was prohibited due to memory restrictions.


\begin{figure}[t!]
	\centering
	\begin{subfigure}{.50\textwidth}
		\includegraphics[width=1\textwidth]{fig/rplots/compareTimeBusinessT-crop.pdf}
		\caption{}
		\label{fig:busTime}
	\end{subfigure}
	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=1\textwidth]{fig/rplots/compareTimeChainNSynDiag.pdf}
		\caption{}
		\label{fig:chainNsynTime}
	\end{subfigure}
	~
	\begin{subfigure}{.32\textwidth}
		%\includegraphics[width=1\textwidth]{fig/rplots/compareUtilityBusiness-crop.pdf}
		\includegraphics[width=1\textwidth]{fig/rplots/compareRegBusinessMean-crop.pdf}
		\caption{}
		\label{fig:busUtil}
	\end{subfigure}
	\begin{subfigure}{.32\textwidth}
		%\includegraphics[width=1\textwidth]{fig/rplots/compareUtilityChain-crop.pdf}
		\includegraphics[width=1\textwidth]{fig/rplots/compareRegChainMean-crop.pdf}
		\caption{}
		\label{fig:chainUtil}
	\end{subfigure}
	\begin{subfigure}{.32\textwidth}
		%\includegraphics[width=1\textwidth]{fig/rplots/compareUtilitySyn-crop.pdf}
		\includegraphics[width=1\textwidth]{fig/rplots/compareRegSynMean-crop.pdf}
		\caption{}
		\label{fig:synUtil}
	\end{subfigure}
	\caption{Comparison of approximations scalability for (a) business, and (b top) chain and (b bottom) unstructured network. In (c),(d) and (e) we compare the defender's upper bound of relative regret of strategies computed with approximation algorithms business, chain and unstructured network, respectively.}
	\label{fig:comparison}
\end{figure}

\subsection{Solution Quality}\label{sec:util}
In this section we analyze the quality of the strategy that each approximation algorithm found.
We use the concept of \emph{relative regret} to capture the relative difference in the defender's utilities for using one strategy instead of another.
Formally, the relative regret of strategy $\sigma_d$ w.r.t. the optimal strategy $\sigma_d^*$ is $\rho(\sigma_d,\sigma_d^*) = \frac{u_d(\sigma_d^*,BR_a(\sigma_d^*))-u_d(\sigma_d,BR_a(\sigma_d))}{u_d(\sigma_d^*,BR_a(\sigma_d^*))}$.
The higher the regret $\rho(\sigma_d,\sigma_d^*)$ the worse strategy $\sigma_d$ is compared to strategy $\sigma_d^*$ for the defender. 
We calculate the defender's \emph{upper bound for relative regret $\bar{\rho}$} by comparing the utilities of the computed strategies to SSEUB.
Notice that the results are overestimation of the worst-case relative regrets for the defender.
In Fig.~\ref{fig:comparison} we show the means and standard errors $\bar{\rho}$ of 200 runs for the business network (Fig.~\ref{fig:busUtil}), chain network (Fig.~\ref{fig:chainUtil}) and unstructured network (Fig.~\ref{fig:synUtil}), with $T=5,n=5$ and $k=2$.
In each instance we altered the number of vulnerabilities of the host types and host type values.
The action costs $c_i$ we draw randomly from $[1,100]$ and action success probabilities $p_i$ from $[0,1]$.

The SLP algorithm computed the best strategies with lowest $\bar{\rho}$. %, however, this algorithm is not scalable.
The SOGS is second best in all networks except unstructured.
Unfortunately, these algorithms are least scalable.
The strategies computed with SOZS algorithm are within reasonable quality.
In the business network SOZS4 performed the best among the ZS approximations and in the chain network the computed strategies were almost as good as the best strategies computed with the SLP algorithm.
However, in the unstructured network it performed worse.
In ZS4 approximations the defender's utility is augmented to prefer outcomes with expensive attack policies for the attacker.
Therefore, the ZS4 approximation works well for networks where long attack policies are produced.
In chain networks the database is the furthest from the internet and in the unstructured network is the closest.
%The defender often allocates honeypots so the attacker interacts with them towards the end of the attack policy, in which case the attacker pays for unsuccessful attack.
PI algorithm computed the worst strategies in all networks.
Because of the good tradeoff between scalability and quality of the produces strategies, 
we decided to further analyze the strategies computed with SOZS4 algorithm.

%In table~\ref{tab:reg} we present mean upper bounds of the relative regrets for the business network.
%The results show that for the business network SOGS approximation finds lowest relative regret and SLP approach has finds second best.
%However, both of these approaches are not very scalable.
%While defender's relative regret with strategies found by SOZS4 approach are close to the former two approaches and it is much more scalable.

%\kdnote{update the table below and text above!}
%{\footnotesize
%	\begin{table}
%		\centering
%		\begin{tabular}{ | l | c |c|c|c|c | r | }
%			\hline
%			SLP & SOGS & SOZS1 & SOZS2 & SOZS3 & SOZS4 & PI \\ \hline
%			TODO:0.104 ?& 0.083 ?& 2.861 ?& 1.951 ?& 1.448 ?& 0.131 ? & 7.767 ?\\
%			\hline
%		\end{tabular}
%		\caption{Comparison of relative regrets of the strategies from approximation games.}
%	\label{tab:reg}
%	\end{table}
%}


\subsection{Quality of ZS Approximations}\label{sec:qualityZS}

\begin{figure}[t!]
	\begin{subfigure}[b]{.33\textwidth}
		\centering
		\includegraphics[width=1\textwidth]{fig/rplots/zerosumnessBucketbusiness-crop.pdf}
		\caption{}
		\label{fig:zsQualityBusiness}
	\end{subfigure}
	\begin{subfigure}[b]{.33\textwidth}
		\includegraphics[width=1\textwidth]{fig/rplots/zerosumnessBucketchain-crop.pdf}
		\caption{}
		\label{fig:zsQualityChain}
	\end{subfigure}
	\begin{subfigure}[b]{.33\textwidth}
		\includegraphics[width=1\textwidth]{fig/rplots/zerosumnessBucketsyn-crop.pdf}
		\caption{}
		\label{fig:zsQualitySyn}
	\end{subfigure}
	%\begin{subfigure}[b]{.5\textwidth}
	%	\includegraphics[width=1\textwidth]{fig/rplots/businessPerp-crop.pdf}
	%	\caption{}
	%	\label{fig:perturb}
	%\end{subfigure}
	\caption{We plot the defender's relative regret dependence on game distance from the zero sum game (computed as mean of $u_a+u_d$) for (a) business, (b) chain and (c) unstructured networks. 
%(d) The defender's utility regret for perturbed action success probabilities, action costs, and host type values.
	}
	\label{fig:zsQuality}
%	\vspace{-1cm}
\end{figure}

The zero sum approximations rely on a zero-sum assumption not actually satisfied in the game. It is natural to expect that the more this assumption is violated in the solved game, the lower the solution quality will be. In order to better understand this tradeoff, we analyze the dependence of the quality of the strategy computed with SOZS4 algorithm  on the amount of \emph{zero-sumness} of the original game.
We define a game's \emph{zero-sumness} as $\bar{u}=\frac{1}{|L|}\sum_{l\in L}(|u_d(l)+u_a(l)|)$, where $L$ is the set of all terminal states of the game.

In Fig.~\ref{fig:zsQuality} we show the upper bound for relative regret $\bar{\rho}$ on the y-axis for the strategies computed by SOZS4 and amount of game zero-sumness $\bar{u}$ on the x-axis for 300 randomly generated game instances for the business network (Fig.~\ref{fig:zsQualityBusiness}), chain network (Fig.~\ref{fig:zsQualityChain}) and unstructured network (Fig.~\ref{fig:zsQualitySyn}) with $T=5,n=5$ and $k=2$.
In each instance we randomly chose the attacker's action costs $c_a\in[1,500]$ and honeypot costs $c^h_t\in[0,0.1r_t]$, while host type values $r_t$ were fixed.
We also show the means and the standard errors of the instances partitioned by step sizes of 50 for $\bar{u}$.

We show that the games with low zero-sumness can be approximated as zero-sum games and the computed strategies have low relative regret for the defender.
For example, in a general sum game with $\bar{u}=250$ the defender computes a strategy at most 6\% worse than the optimal strategy.

%However, for the games with low zero sumness an alternative scalable approach should be used.

\subsection{Sensitivity Analysis}\label{sec:sensitivity}

The defender's optimal strategy depends on the attack graph structure, the action costs, success probabilities and rewards.
In real-world scenarios the defender can only estimate these values.
We analyze the sensitivity of the defender's strategies computed with SOZS4 to perturbations in action costs, success probabilities and values of host types in attack graphs.
%In the this experiment we analyze how sensitive is the solution quality computed with SOZS4 approximation to imprecision in models of the attack graphs. 

We generate the defender's estimate of the attack graph by perturbing the original attack graph actions as follows: (i) action success probability are chosen uniformly from the interval $[p_a-\delta_p, p_a+\delta_p]$ restricted to $[0.05,0.95]$ to prevent it becoming impossible or infallible, (ii) action costs are chosen uniformly from interval $[c_a(1-\delta_c), c_a(1+\delta_c)]$, and (iii) rewards for host $t$ from uniformly from the interval $[ r_t(1-\delta_r), r_t(1+\delta_r)]$, where $p_a,c_a$ and $r_t$ are the original values and $\delta_p,\delta_c$ and $\delta_r$ is the amount of perturbation.
The action probabilities are perturbed absolutely (by $\pm \delta_p$), but the costs and rewards are perturbed relative to their original value (by $\pm \delta_c c_a$ and $\pm \delta_r r_f$).
The intuition behind this is that the higher the cost or reward values the larger the errors the defender could have made while estimating them, which cannot be assumed for the probabilities.

\begin{wrapfigure}{r}{0.5\textwidth}
        \vspace{-3mm}
	\includegraphics[width=0.5\textwidth]{fig/rplots/businessPerp-crop.pdf}
        \caption{The defender's utility regret for perturbed action success probabilities, action costs, and host type values.}\label{fig:perturb}
\end{wrapfigure}

We compute (i) the defender's strategy $\sigma_d$ on the game with the original attack graphs and (ii) the defender's strategy $\sigma_d^p$ on the game with the perturbed attack graph.
Fig.~\ref{fig:perturb} presents the dependence of the relative regret $\rho(\sigma_d,\sigma_d^p)$  on the perturbations of each parameter individually ($\delta_p,\delta_c,\delta_r$) and altogether ($\delta_a$). 
The results suggest that the regret depends significantly on the action success probabilities and the least on the action costs.
E.g., the error of 20\% ($\delta_a=0.2$) in the action probabilities results in a strategy with 25\% lower expected utility for the defender than the strategy computed based on the true values.
The same imprecision in action costs or host type rewards result in only 5\% lower utility.

%We also examined the effect of perturbing each component individually, however, due to the space limit, we did not include figures (which are similar). 


\subsection{Case Study}\label{sec:case}

In order to understand what types of errors individual approximations make,
we analyze the differences in strategies computed by the algorithms on a specific game for business network with $T=5$, $n=5$ and $k=2$.
The network basis is $b=(1,0,1,0,1)$, where the elements correspond to the number of databases, firewalls, WS1, WS2 and servers, respectively.
There are $|X|=15$ possible networks, each selected with probability $\delta_x=\frac{1}{15}$.
The defender can deploy honeypots in $|Y|=15$ ways and with honeypot costs as showed in Table~\ref{tab:b552}.
There are 225 network settings partitioned into 70 information sets for the attacker. 
	\begin{table}[t!]
		\centering
		\caption{Algorithm comparison for the case-study.}
		\label{tab:case}
		\begin{tabular}{ | l | c | c |c|c|c|c |c| r | }
			\hline
			algorithm & SSEUB & SLP & SOGS & SOZS1 & SOZS2 & SOZS3 & SOZS4 & PI \\ \hline
			defender's utility & -643 & -645 & -654 & -689 & -665 & -676 & -656 & -699 \\ \hline
			%defender's utility & -347.5 & -358.3 & & &  &  & -359.1 & -372.7 \\ \hline
			runtime [s] & 2.9 & 3.2 & 6027 & 1.3 & 1.5 & 1.3 & 1.5 & 1.4 \\ \hline
		\end{tabular}
	\end{table}
Table~\ref{tab:case} presents the comparison of the strategy qualities computed with the algorithms and their runtime in seconds.
The upper bound for the defender's optimal utility is SSEUB=-643.
The best strategy was computed with SLP algorithm with utility $u_d=-645$.
Although the difference between the utilities is very small, it suggests that the SLP strategy in not necessary optimal.
We compare the strategies of the other algorithms to the SLP strategy.

SOGS computed the second best strategy ($u_d=-654$) and failed to compute the optimal strategy because the restricted game lacks strategies played by attacker in SSE.
For example, both strategies from SOGS and SLP in the network $x_1=(3,0,1,0,1)$ play $y_1=(0,0,1,0,1)$ (adds a WS1 and a server as HPs) with probability 1.
The attacker aims to attack the most valuable host (database) either via WS1 (policy $s_1$) or server (policy $s_2$).
Both have the same probability of interacting with a honeypot $0.5$ and a rational attacker will choose $s_2$ to compromise the server as well.
Attack policy $s_2$ leads to a terminal state with the defender's expected utility -600.
The strategy from SLP, in contrast to strategy from SOGS, additionally plays $y_2=(1,0,0,0,1)$ in network $x_2=(2,0,2,0,1)$ with probability 0.037, which leads to the same information set as action $y_1$ in $x_1$.
The attacker's uncertainty between the two states in the information set changes his optimal attack policy from $s_2$ to $s_1$ for that information set.
Attacking via the WS1 host type has a lower probability of interacting with the HP than via a server, which yields the defender expected utility -538, since the server will not be compromised.
The restricted game in SOGS algorithm did not contain strategy $s_1$, so the computed strategy did not play $y_2$ in $x_2$ at all.

The PI strategy has the lowest defender's utility as it does not exploit the attacker's imperfect information at all.
In this game the defender always adds a server host type as a honeypot to try to stop the attacker at the beginning. % (see Table~\ref{tab:casePst}.
The second honeypot is added by a simple rule: (i) if the database can be compromised only via server and WS1, add honeypot of WS1 host type, otherwise (ii) as a database host type.

SOZS4 computed the best strategy among the ZS approximations.
However, each of them have certain drawbacks.
In SOZS1 and SOZS2 the defender ignores his costs for deploying the honeypots; these strategies often add database hosts as honeypots, which is in fact the most expensive honeypot to deploy.
In SOZS2 and SOZS4 the defender prefers outcomes where the attacker has expensive attack policies.
They often deploy honeypots with motivation for the attacker to have an expensive costs for attack policies (e.g., a strategy computed with SOZS2 adds database as a honeypot in 74\% while the strategy from SLP only in 43\%). %^, in which case the attacker has longest and most expensive attack policies.
Strategies computed with SOZS3 and SOZS4 are difficult to analyze.
The strategies often miss the precise probability distribution between the networks where the attacker is indifferent between the attack policies and therefore chooses the one in favour for the defender.
There is no general error they make in placing the honeypots as with the previous strategies.



%{\footnotesize
%	\begin{table}
%		\centering
%		\begin{tabular}{ | l | c | c |c|c|r | }
%			\hline
%			algorithm & database & firewall & workstation1 & workstation2 & server \\ \hline
%			SLP & 0.43 & 0.01 & 0.46 & 0.02 & 1.08 \\ \hline
%			SO & 0.44 & 0.01  &0.46  &0.04 & 1.05  \\ \hline
%			PI & 0.53 &  0 & 0.47 &  0  & 1 \\ \hline
%			SOZS1 & 0.72 & 0 & 0.19 & 0  &1.1 \\ \hline
%			SOZS2 & 0.74  & 0.01 & 0.24 & 0.02 & 1 \\ \hline
%			SOZS3 & 0.43  & 0 & 0.44  &0.02 & 1.1 \\ \hline
%			SOZS4 & 0.47  &0 & 0.46  & 0.05 & 1.02\\ \hline
%		\end{tabular}
%		\caption{Average number of deployed honeypots of each host type from the computed strategies for every approximation algorithm .}
%	\label{tab:casePst}
%	\end{table}
%}

%For example, in this game consider two outcomes of this game for network after nature plays $x=(1,0,1,0,3)$ and the defender $y=(0,0,1,0,1)$.
%The attacker has two candidate strategies: first leads to outcome $a$ with expected reward $R_a=530$ and cost $C_a=141$ and second to outcome $b$ with $R_b=467$ and $C_b=82$.
%Defender's utilities for $a$ and $b$ are -530 and -467, respectively.
%In the strategy found with SLP the defender acts to motivate the attacker for option $b$.
%However, in SOZS4 approximation the utilities are -389 and -385, making him almost indifferent and actually chooses not to motivate the attacker for option $b$.
%This may cause the SOZS4 approximation to act not optimally.









%  utility,  alg
% -642.9831650431215, CSE

%  utility,  alg
% -646.8732746268956, SLP

% utility, alg
% -654.4023895115477 SO

% zerosumType, alg, utility % 4,ZS, -656.1811614050417

% zerosumType, alg, n,CPLEXTime, totalTime, algBRCTime, utility
% 2,ZS, -665.1601123939205

% zerosumType, alg,utility
% 3,ZS,-675.8797821125946

% zerosumType, alg, utility
% 1, ZS, -689.1330706625972

% utility, k, net, alg, n 
% -699.6844444444444,2,business,PI,5




%Z1: ud = -R
%       HP([2, 1, 1, 0, 1],[0, 0, 1, 0, 1],-) (0.6357979571679392)
%               HP([2, 1, 1, 0, 1],[0, 0, 1, 0, 1],468.0, 82.89699999999999, 70.0) (1.0)
%               -538.0 * 0.6357979571679392 * 1.0 * 0.06666666666666667 = -22.803953397090087
%                385.103 * 0.6357979571679392 * 1.0 * 0.06666666666666667 = 16.323180046616326
%        HP([2, 1, 1, 0, 1],[0, 0, 0, 1, 1],-) (1.4084507042233518E-4)
%                HP([2, 1, 1, 0, 1],[0, 0, 0, 1, 1],0.0, 37.0, 70.0) (1.0)
%                -70.0 * 1.4084507042233518E-4 * 1.0 * 0.06666666666666667 = -6.572769953042309E-4
%                -37.0 * 1.4084507042233518E-4 * 1.0 * 0.06666666666666667 = -3.4741784037509345E-4
%        HP([2, 1, 1, 0, 1],[0, 0, 0, 0, 2],-) (0.3640611977616385)
%                HP([2, 1, 1, 0, 1],[0, 0, 0, 0, 2],924.0, 136.16266666666667, 100.0) (1.0)
%                -1024.0 * 0.3640611977616385 * 1.0 * 0.06666666666666667 = -24.853244433861185
%                787.8373333333334 * 0.3640611977616385 * 1.0 * 0.06666666666666667 = 19.121400214311237
%
%
%SOZS2: ud = -R +C
%HP([2, 1, 1, 0, 1],[],-)
%        HP([2, 1, 1, 0, 1],[0, 0, 1, 0, 1],-) (0.8367749449115103)
%                HP([2, 1, 1, 0, 1],[0, 0, 1, 0, 1],468.0, 82.89699999999999, 70.0) (1.0)
%                -538.0 * 0.8367749449115103 * 1.0 * 0.06666666666666667 = -30.0123280241595
%                385.103 * 0.8367749449115103 * 1.0 * 0.06666666666666667 = 21.48296944068382
%        HP([2, 1, 1, 0, 1],[0, 0, 0, 0, 2],-) (0.16322505508848972)
%                HP([2, 1, 1, 0, 1],[0, 0, 0, 0, 2],924.0, 136.16266666666667, 100.0) (1.0)
%                -1024.0 * 0.16322505508848972 * 1.0 * 0.06666666666666667 = -11.142830427374232
%                787.8373333333334 * 0.16322505508848972 * 1.0 * 0.06666666666666667 = 8.572986142273479
%
%
%SOZS3: ud = -R -H
%P([2, 1, 1, 0, 1],[],-)
%       HP([2, 1, 1, 0, 1],[0, 0, 1, 0, 1],-) (0.9998591549295757)
%               HP([2, 1, 1, 0, 1],[0, 0, 1, 0, 1],468.0, 82.89699999999999, 70.0) (1.0)
%               -538.0 * 0.9998591549295757 * 1.0 * 0.06666666666666667 = -35.861615023474116
%                385.103 * 0.9998591549295757 * 1.0 * 0.06666666666666667 = 25.669917342722957
%        HP([2, 1, 1, 0, 1],[0, 0, 0, 1, 1],-) (1.4084507042433358E-4)
%                HP([2, 1, 1, 0, 1],[0, 0, 0, 1, 1],0.0, 37.0, 70.0) (1.0)
%                -70.0 * 1.4084507042433358E-4 * 1.0 * 0.06666666666666667 = -6.572769953135568E-4
%                -37.0 * 1.4084507042433358E-4 * 1.0 * 0.06666666666666667 = -3.474178403800228E-4
%
%
%
%SOZS4:ud = -R -H +C
%P([2, 1, 1, 0, 1],[],-)
%       HP([2, 1, 1, 0, 1],[0, 0, 1, 0, 1],-) (1.0)
%               HP([2, 1, 1, 0, 1],[0, 0, 1, 0, 1],468.0, 82.89699999999999, 70.0) (1.0)
%               -538.0 * 1.0 * 1.0 * 0.06666666666666667 = -35.86666666666667
%                385.103 * 1.0 * 1.0 * 0.06666666666666667 = 25.673533333333335

%  utility,  alg
% -642.9831650431215, CSE

%  utility,  alg
% -646.8732746268956, SLP

% utility, alg
% -654.4023895115477 SO

% zerosumType, alg, utility
% 4,ZS, -656.1811614050417

% zerosumType, alg, n,CPLEXTime, totalTime, algBRCTime, utility
% 2,ZS, -665.1601123939205

% zerosumType, alg,utility
% 3,ZS,-675.8797821125946

% zerosumType, alg, utility
% 1, ZS, -689.1330706625972

% utility, k, net, alg, n 
% -699.6844444444444,2,business,PI,5




