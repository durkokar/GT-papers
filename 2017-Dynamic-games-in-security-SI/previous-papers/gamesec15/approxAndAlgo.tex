\section{Game Approximations and Algorithms}\label{sec:algorithms}
In this section we present a collection of \emph{approximated games} for Stackelberg deception games that allow us to apply algorithms that solve the games faster. %in polynomial time.
The approximations are based on relaxing certain aspects of the original game.

\subsection{Perfect-Information Game Approximation}
A straightforward game approximation is to remove the attacker's uncertainty about the nature's and defender's action, which results in a perfect-information game.
In~\cite{durkota2015optimal} the authors propose an algorithm to solve a game that corresponds to a single subgame after the nature's move.
In our game we use their algorithm to solve each subgame after the nature moves and combine their strategies to construct a one defender's strategy for the whole game, to which we refer as to PI.


\subsection{Zero-sum Imperfect-Information}\label{sec:zerosum}
In~\cite{korzhyk2011stackelberg} the authors show that under certain conditions approximating the non-zero-sum (nZS) game as a zero-sum (ZS) game can provide an optimal strategy for the original game, which can be solved in polynomial time w.r.t. the size of the game-tree. 
In this section we use a similar idea for constructing ZS game approximations.

Recall that in our game the defender's utility is $u_d(l)=-R_l-H_l$ and the attacker's utility is $u_a(l)=R_l-C_l$ for terminal state $l$.
Our game has a payoff structure where $R_l$ is the ZS component in the utilities and the smaller the difference $|H_l-C_l|$ in the outcomes, the closer our game is to a ZS game.
We propose several variants of ZS game approximations: %, where one or both players utilities' are modified to satisfy the zero-sum condition $u_d(l)=-u_a(l)$. 
(ZS1) both players consider only the expected rewards from the attack policy  $u_d(l) = -R_l$; 
(ZS2) only attacker's utilities are considered  $u_d(l) = -R_l+C_l$;
(ZS3) only defender's utilities are considered  $u_d(l) = -R_l-H_l$; and
(ZS4) the defender keeps his original utility with adversarial motivation to harm the attacker  $u_d(l) = -R_l-H_l+C_l$.


When solving the ZS approximation games, we avoid generating exponential number of attacker's attack policies by using Single-Oracle (SO) algorithm, an adaptation of the algorithm introduced in~\cite{bovsansky2015sequence}. % introduced in~\cite{mcmahan2003planning}.
It is often used when one player's action space is very large (in our case the attacker's).
SO overcomes this difficulty by introducing a \emph{restricted game}, which contains only a subset of the player actions.
It iteratively extends the restricted game until it contains an equilibrium.
In ZS EFG games it was shown in~\cite{bosansky2014exact} that this approach can be used to find the NE, which is equivalent to the SSE in ZS games.
For nZS games the algorithm is well defined, but no guarantee on convergence to an equilibrium holds; however, it may find a suboptimal solution very fast.
Thus, we use SO algorithm to solve ZS approximations (referred as to ZS1-4) as well as the original nZS game, to which we refer as to SO.

In $i$-th iteration the SO (resp. ZS1 through ZS4) algorithm consists of the following two steps:
(i) First, compute the attacker's best response $\hat{\pi}_a^i=BR_a(\sigma_d^{i-1})$ to the defender's strategy $\sigma_d^{i-1}$ in the whole game.
If all actions of the strategy $\hat{\pi}_a^i$ are already contained in the restricted game, algorithm returns SSE of the restricted game.
Otherwise, (ii) extend the restricted game by actions from $\hat{\pi}_a^i$, compute its new SSE strategy profile $(\pi_a^i,\sigma_d^i)$ and go to step (i) with incremented iteration counter $i$.
The SSE of the restricted game is computed using LP if game is zero-sum and using \emph{mixed integer linear program} (MILP)~\cite{bovsansky2015sequence} if the game is nZS.
The algorithm starts with uniform strategy $\sigma_d^0$ (the defender plays every action with a uniform probability), restricted game containing all natures and defender's action and no attacker actions and iteration counter $i=1$.

The main reason why SO algorithm may fail to find SSE for nZS games is due to no guarantee that the restricted game will contain all the attacker's actions played in SSE when algorithm converges, which cannot happen in ZS game.
In Fig.~\ref{fig:SOcounterexample} we show a counterexample where SO does not find SSE.
In this game nature plays $x_1$ and $x_2$, each with probability 1/2; then the defender chooses between actions $y_1$ through $y_4$; and lastly, the attacker chooses between actions $s_1$ through $s_4$.
Sequences of actions $x_1,y_2$ and $x_2,y_3$ the attacker cannot distinguish from each other (creating one information set).
In the leaf states are the defender's utilities (top value) and the attacker's utility (bottom value).
After the first SO iteration, where the attacker plays best response to defender's strategy $\sigma_d^0$ the restricted game will consists of attacker's actions $s_1, s_2$ and $s_4$.
SSE of the restricted game is the strategy where the defender plays $y_2$ and $y_3$ with probability 1 and the attacker's plays $s_1, s_2$ and $s_4$, yielding the defender expected utility -2.5.
At this point restricted game contains the attacker's best response and SO ends.
However, SSE for this game is actually for the defender to play $y_1$ with probability 13/28, $y_2$ with probability 15/28 and $y_3$ with probability 1.
Then the attacker will play action $s_3$ instead of $s_2$ in the information set, yielding the defender's expected utility -2.1964.

\begin{figure}[t!]
	\centering
	\resizebox{6cm}{!}{
		\usebox{\SOcounterexample}
	}
	\caption{Counterexample where SO does not find SSE.}
	\label{fig:SOcounterexample}
\end{figure}

\subsubsection{Optimal Attack Policy for Information Set}
In this section we give a detailed description about the computation the attacker's best response $\pi_a=BR_a(\sigma_d)$ to the defender's strategy $\sigma_d$.
First, we decompose the problem of computing best response $\pi_a$ into subproblems of computing the optimal attack policy for each information set separately given prior belief about the probabilities of the states in the information set.
We calculate the attacker's prior \emph{belief} based on $\sigma_d$ and normalize so the probabilities of the states in an informations set sum to 1.
The networks in an information set have the same attack graph structure.
However, actions may have different probabilities of interacting with the honeypots, depending on the defender's honeypot deployment on the path to that network.

In~\cite{durkota2015optimal} the authors present a MDP approach with several pruning techniques to compute the optimal attack policy for single attack graph.
They translate attack graphs into states in the MDP, where transitions between the states modify the attack graph (by removing the performed action, achieved facts and unperformable actions from the attack graph).
We extend their approach to compute the optimal attack policy for the set of attack graphs with prior belief over them by translating the problem into a partially observable MDP (POMDPs).
The extension is straightforward, while attacking the attacker keeps a probability distribution over the attack graphs.
Since actions may have different probabilities of interacting with the honeypots, we use the standard Bayes rule to update the probability distribution after each attack action.

Let $b(z_i)\in\mathbb{R^+}$ be the probability distribution over the attacked networks in information set $I$, so that $\sum_{z_i\in I}b(z_i)=1$.
Performing action $i$ in information set $I$, leads to a new information set $I'$.
New probability distribution $b'$ over network in $I'$ is computed as $b'(z_j) = \frac{P(z_j,I,I',i) b(z_j)}{\sum_{z_k\in I'}P(z_j,I,I',i)b'(z_j)}$, where $P(z_j,I,I',i)$ is the probability that performing action $i$ in network $z_j$ in information set $I$ lead to information set $I'$ (in the same network).

%Due to the space limit, we present only an example to show the intuition behind the Bayes update rule.
For example, assume that the attacker acts in information set $I_1$ from Fig.~\ref{fig:game} with network belief probabilities $b(z_1)=0.5$ and $b(z_2)=0.5$.
Performing action $3$ has 0.5 probability of touching honeypot in the state $z_1$ and 0 in state $z_2$.
Thus, new probabilities over the networks in information set $I_2$ are $b'(z_1)=\frac{0.2b(z_1)}{0.2b(z_1) + 0.4b(z_2)}=\frac{1}{3}$ and $b'(z_2)=\frac{0.4b(z_2)}{0.2b(z_1)+0.4b(z_2)}=\frac{2}{3}$.

During the optimal attack policy computation, we also use similar pruning techniques as described in~\cite{durkota2015optimal}.
First, we adopt a \emph{memoization} technique: we cache visited probability distribution over the POMPD state during the search, and remember the computed optimal policy for it.
If the same probability distribution is visited again we reused the memorized policy.
Second, we use the Sibling-Class Theorem to prune the exploration of the actions in the POMDP search, that will definitely not produce the optimal attack policy.
The key idea of the theorem is following: in the POMDP state, before exploring each action that the attacker can perform in a POMDP state, try first to detect whether some actions can be pruned without search. %that will definitely be not in the optimal policy and do not explore them.
%It is achieved as follows. 
We can partition the actions into AND/OR classes.
Actions in the OR classes have the same effect and are in OR relationship (if action from the class succeeds, the attacker will not perform rest of the action the that class).
Actions in the AND classes have the same reward, however, all of them must be performed to reach it.
The first part of the theorem states that in an OR class it is optimal to begin with action that has the highest $p_i/c_i$ ratio.
It is intuitive, start with the action with high success probability and low action cost.
Second part of the theorem states that in an AND class it is optimal to begin with action that has highest $(1-p_i)/c_i$ ratio.
Intuitively, since the attacker will have to perform all the actions from that class, it is better to start with low probability action first.
In case it fails, the attacker saves paying for performing the remaining of the actions.
With this theorem we explore only those actions that have highest ratio in their respective classes.
However, this approach does not work for the actions that interact with honeypots.
Those, we explore in any case.


\subsection{Single LP Approximation}\label{sec:slp}
\begin{figure}[t!]
	\centering
	\begin{subfigure}{.49\textwidth}
		\resizebox{7cm}{!}{
			\usebox{\SLP}
		}
		\caption{}
		\label{fig:SOcounterexample}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\includegraphics[width=.7\textwidth]{fig/LPmatrix}
		\caption{}
		\label{fig:EFGtoLP}
	\end{subfigure}
	\caption{Illustration of extensive-form game in (a) translation into two normal-form games in (b).}
	\label{fig:comparison}
\end{figure}
Computing the game's optimal strategy is computationally intractable which makes difficult to measure the qualities of the strategies produced with the approximated models.
Suboptimal strategy with the highest defender's utility produced by approximated models is insufficient as it still does not say how distant is its utility to the utility of the optimal strategy. %, and the best suboptimal strategy could be far from optimal. 
Therefore, finding an upper bound for the defender's utility of the SSE strategy could resolve this issue.
The difference between the utility of an approximated strategy and the upper bound for SSE says by how much the approximation can be worse then the optimum at most.
In this section we introduce a novel approach for computing the upper bound for SSE, referred as to SSEUB.

The authors in~\cite{conitzer2011commitment} present a single linear program (LP) for computing the SSE for a normal-form game (NFG).
The LP finds the probability distribution over the outcomes with maximal utility for the defender under the condition that the attacker plays a best response (does not have incentive to deviate).
We exploit our extensive-form game's structure which allows us to write the game as a collection of normal-form games and formulate the problem as a single LP.
We demonstrate our approach on the example in Fig.~\ref{fig:EFGtoLP}.
For each of the attacker's information sets we construct a normal-form game where the defender chooses an action that leads to the corresponding information set and the attacker chooses an attack policy in the that information set.
In Fig.~\ref{fig:EFGtoLP} in information set $I_1$ the defender chooses between actions $y_1$ and $y_3$ and in $I_2$ between actions $y_2$ and $y_4$.
Each terminal node in the EFG corresponds to an outcome in one of NFG ($p_1$ through $p_8$ in our example).
Furthermore, we incorporate the nature by adding constraint for each nature action $x_i$, that the probability of action $x_i$ equals to the sum of probabilities of outcomes that can be reached via nature action $x_i$.
In our example, the probabilities of outcomes $p_1,p_2$ (defender's action $y1$) and $p_3,p_4$ (defender's action $y2$) must sum to $1/2$, since they root from the same nature's action $x_1$ played with probability $1/2$.
The LP formulation follows:
{\footnotesize
	\begin{subequations}
	\begin{align}
		\textrm{max} &\sum_{x\in X} \sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s)u_d(x,y,s) \\
		\textrm{s.t.}: & (\forall I_a\in \mathcal{I}_a,s_1,s_2\in S_{I_a}): \sum_{(x,y)\in I_a}p(x,y,s_1)u_a(x,y,s_1) \geq \sum_{(x,y)\in I_a}p(x,y,s_1)u_a(x,y,s_2) \\
		%&\sum_{x\in X(I_a)}\sum_{y\in Y(I_a)}P(x,y,s_1)u_a(x,y,s_1) \geq \sum_{x\in X(z)}\sum_{y\in Y(z)}P(x,y,s_1)u_a(x,y,s_2) \\
		& \sum_{x\in X}\sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s)=1 \\
		& (\forall x\in X,y\in Y,s\in S_{(x,y)}): p(x,y,s)\geq 0 \\
		& (\forall x\in X):  \sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s) = \delta_x
	\end{align}
\end{subequations}
}
where the only variables are $p(x,y,s)$, the probability of reaching terminal state of the game where nature plays $x$, the defender $y$ and the attacker $s$.
%The result of the LP is a probability distribution over all terminal nodes of the game.
The solution of the LP is an upper bound for the defender's expected utility in the SSE for the following reasons.
We compare our LP formulation to MILP introduced in \cite{bovsansky2015sequence}.
MILP consists of (i) constraints that the follower plays best response, (ii) constraints that the follower plays pure strategy and (iii) constraints for the probability flow conservation.
The objective is to maximize the leader's utility.
Our LP consists of constraint that the follower plays best response and the probability flow conservation, with the objective to maximize the defender's utility.
Our LP has weaker restrictions on the solution than MILP, hence, it cannot excluded any SSE that is found by MILP.
The value of the LP (referred to as SSEUB) is an upper bound for SSE which can be computed in polynomial time to the size of the game.
We use SSEUB value to measure the quality of the approximation algorithm's strategies.

The drawback of formulating our game as a single LP is that it requires finding all attacker's attack policies for each of his information sets.
As mentioned earlier, there is exponential number of MDP policies for an attack graph.
We reduce this number by considering only \emph{rationalizable} (introduced in~\cite{bernheim1984rationalizable}) attack policies for an information set.
An attack policy is rationalizable if and only if it is attacker's best response to some belief about the position of honeypots.
Set of all rationalizable attack policies is called \emph{closed under rational behaviour} (curb)~\cite{benisch2010algorithms}.
By considering only the curb set for the attacker, we do not exclude any SSE with the following rationale.
Attack policy that is in SSE is the attacker's best responses, thus it must be rationalizable and therefore it must be in the curb set.

First, we compute attacker's curb set for each information set and consider only them in our LP formulation.
We find curb for every information set separately by translating the problem into POMDP as explained in Sec.~\ref{sec:zerosum}.
Then, we use witness algorithm described in~\cite{littman1994witness} to compute the set of undominated policies.
The algorithm is based on value iteration with technique of eliminating the dominated policies during the search.

From the LP result we extract the defender's strategy as a marginal probability for each defender's action: the probability that defender plays action $y\in Y$ in state  $x\in X$ is $\sum_{s\in S_{(x,y)}} P(x,y,s)$.
We will refer to this mixed strategy as $\sigma_d^{LP}$ and to the defender's utility for the strategy profile where the defender plays $\sigma_d^{LP}$ and the attacker pure best response to it as SLP; formally, SLP$=u_d(\sigma_d^{LP}, BR_a(\sigma_d^{LP}))$.

