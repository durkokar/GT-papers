#!/bin/bash

shopt -s nullglob

rm `ls *-crop.pdf`

for i in `ls *pdf`
do
	pdfcrop $i
done
