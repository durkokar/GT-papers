set key top outside horizontal
set xlabel 'Number of different values for bet/raise actions'
set ylabel 'Memory Consumed [GB]'
set grid nopolar
set grid noxtics nomxtics ytics mytics noztics nomztics \
 nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
set grid layerdefault   linetype 0 linewidth 1.000,  linetype 0 linewidth 1.000

#set logscale y 10

set style data linespoints
#set style histogram cluster gap 1
#set style fill solid border -1
#set style fill pattern 0 border
#set boxwidth 0.9

#set palette gray

#set terminal postscript eps enhanced defaultplex leveldefault color colortext solid dashed dashlength 1.0 linewidth 1.0 rounded palfuncparam 2000,0.003 

set terminal postscript eps noenhanced defaultplex \
   leveldefault color colortext \
   dashed dashlength 3.0 linewidth 1.0 butt \
   palfuncparam 2000,0.003 \
   "LMRoman10" 22

set output "poker-graph.eps"

plot 	'data-poker.txt' using 2:xtic(1) ti "FullLP-R1" lc rgbcolor "#000000" linetype 1 lw 3 pointsize 2, \
	'data-poker.txt' using 3:xtic(1) ti "DO-R1" lc rgbcolor "#000000" pointtype 1 linetype 2 lw 3 pointsize 2, \
	'data-poker.txt' using 4:xtic(1) ti "FullLP-R2" lc rgbcolor "#333333" pointtype 2 linetype 1 lw 3 pointsize 2, \
	'data-poker.txt' using 5:xtic(1) ti "DO-R2" lc rgbcolor "#333333" pointtype 2 linetype 2 lw 3 pointsize 2, \
	'data-poker.txt' using 6:xtic(1) ti "FullLP-R3" lc rgbcolor "#666666" pointtype 3 linetype 1 lw 3 pointsize 2, \
	'data-poker.txt' using 7:xtic(1) ti "DO-R3" lc rgbcolor "#666666" pointtype 3 linetype 2 lw 3 pointsize 2, \
	'data-poker.txt' using 8:xtic(1) ti "FullLP-R4" lc rgbcolor "#999999" pointtype 4 linetype 1 lw 3 pointsize 2, \
	'data-poker.txt' using 9:xtic(1) ti "DO-R4" lc rgbcolor "#999999" pointtype 4 linetype 2 lw 3 pointsize 2
