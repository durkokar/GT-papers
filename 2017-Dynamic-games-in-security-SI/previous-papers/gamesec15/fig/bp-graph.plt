set key left vertical
set xlabel 'Search Game Scenarios'
set ylabel 'Time [s] (log scale)'
set grid nopolar
set grid noxtics nomxtics ytics mytics noztics nomztics \
 nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
set grid layerdefault   linetype 0 linewidth 1.000,  linetype 0 linewidth 1.000

set logscale y 10
set format y "10^{%L}"
set yrange [10:18000]

set style data histogram
set style histogram cluster gap 1
set style fill solid border -1
#set style fill pattern 0 border
set boxwidth 0.9

#set palette gray

set terminal postscript eps enhanced defaultplex leveldefault color colortext solid dashlength 1.0 linewidth 1.0 rounded palfuncparam 2000,0.003 "LMRoman10" 22

set output "bp-graph.eps"
#plot 	'data-BP.txt' using 2:xtic(1) ti "FullLP" lt -1 fill pattern 3, \
#	'data-BP.txt' using 3:xtic(1) ti "DO-SAva-ORGbrs" lt -1 fill pattern 2, \
#	'data-BP.txt' using 4:xtic(1) ti "DO-SAva-DEFbrs" lt -1 fill pattern 1, \
#	'data-BP.txt' using 5:xtic(1) ti "DO-UBva-ORGbrs" lt -1 fill pattern 4, \
#	'data-BP.txt' using 6:xtic(1) ti "DO-UBva-DEFbrs" lt -1 fill pattern 0

plot 	'data-BP.txt' using 2:xtic(1) ti "FullLP" lt rgbcolor "#000000", \
	'data-BP.txt' using 3:xtic(1) ti "DO-SAva-ORGbrs" lt rgbcolor "#666666", \
	'data-BP.txt' using 4:xtic(1) ti "DO-SAva-NEWbrs" lt rgbcolor "#999999", \
	'data-BP.txt' using 5:xtic(1) ti "DO-UBva-ORGbrs" lt rgbcolor "#BBBBBB", \
	'data-BP.txt' using 6:xtic(1) ti "DO-UBva-NEWbrs" lt rgbcolor "#EEEEEE"
