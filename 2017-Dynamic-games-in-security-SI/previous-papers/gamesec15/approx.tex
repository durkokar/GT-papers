%!TEX root = main.tex

\section{Game Approximations}\label{sec:approx}

The general cases of computing Stackelberg equilibria of imperfect information games with stochastic events is NP-hard~\cite{letchford2010computing}. The state-of-the-art algorithm for solving this general class of games uses mixed-integer linear programming and the sequence-form representation of strategies~\cite{bosansky2015sequence}.
Our case of attack graph games is also hard because the size of the game tree representation is exponential in natural parameters that characterize the size of a network (number of host types $T$, number of hosts $n$, or number of honeypots $k$), which further limits the scalability of algorithms. 
We focus here on a collection of \emph{approximations} that find strategies close to SSE in polynomial time w.r.t. the size of the game tree.
We present the general idea of several approximation methods first, and discuss the specific details of new algorithms in the next section. 

\subsection{Perfect Information Game Approximation}\label{sec:pi}
A straightforward game approximation is to remove the attacker's uncertainty about the actions of nature and the defender, which results in a perfect information (PI) game.
Although the authors in~\cite{letchford2010computing} showed that in general the PI game with chance nodes is still NP-hard to solve, the structure of our game allows us to find a solution in polynomial time.
The nature acts only once and only at the beginning of game. % (the attacker chooses an attack policy with corresponding expected reward).
After nature's move the game is a PI game without chance nodes, which can be solved in polynomial time w.r.t. the size of the game~\cite{letchford2010computing}.
To solve the separate subgames, we use the algorithm proposed in~\cite{durkota2015optimal}.
It computes the defender's utility for each of the defender's actions followed by attacker's best response.
Next, the algorithm selects the best action to be played in each subgame by selecting the action with maximal utility for the defender.
In Sec.~\ref{sec:OAPasPOMDP} we discuss the algorithm that computes the optimal attack policy.

%\begin{figure}[t!]
%	\centering
%	\resizebox{7cm}{!}{
%		\usebox{\PIbad}
%	}
%	\caption{A game where PI finds very bad strategy.}
%	\label{fig:comparison}
%\end{figure}

\subsection{Zero-Sum Game Approximation}\label{sec:zerosum}
In~\cite{korzhyk2011stackelberg} the authors showed that under certain conditions approximating the general sum (GS) game as a zero-sum (ZS) game can provide an optimal strategy for the GS game.
In this section we use a similar idea for constructing ZS game approximations, for which we compute a NE that coincides with SSE in ZS games. A NE can be found in polynomial time in the size of the game tree using the LP from~\cite{koller1996efficient}.
%Additionally, in~\cite{korzhyk2011stackelberg} it was shown that for ZS games the SSE is equivalent to NE, which can be found in polynomial time to the size of the game tree.

Recall that in our game the defender's utility is $u_d(l)=-R_l-H_l$ and the attacker's utility is $u_a(l)=R_l-C_l$ for terminal state $l\in L$.
Our game has a payoff structure where $R_l$ is a ZS component in the utilities and the smaller $|H_l-C_l|$ in the outcomes, the closer our game is to a ZS game.
We propose several variants of ZS game approximations: %, where one or both players utilities' are modified to satisfy the zero-sum condition $u_d(l)=-u_a(l)$. 
(ZS1) both players consider only the expected rewards from the attack policy  $u_d(l) = -R_l$; 
(ZS2) only the attacker's utilities are considered  $u_d(l) = -R_l+C_l$;
(ZS3) only the defender's utilities are considered  $u_d(l) = -R_l-H_l$; and
(ZS4) the defender keeps his original utility with adversarial motivation to harm the attacker  $u_d(l) = -R_l-H_l+C_l$.

We also avoid generating the exponential number of attack policies by using a single oracle algorithm (Sec.~\ref{sec:so}).
This algorithm has two subroutines: (i) computing a SSE of a ZS game and (ii) finding the attacker's best response strategy to the defender's strategy.
%We compute SSE by finding NE by solving one LP~\cite{shoham2008multiagent}.
The attacker's best response strategy we find by translating the problem into the \emph{Partially Observable Markov Decision Process} (POMDP), explained in Sec.~\ref{sec:OAPasPOMDP}.


%The SSE of the restricted game is computed using LP if game is zero-sum and using \emph{mixed integer linear program} (MILP)~\cite{bosansky2015sequence} if the game is nZS.


%\subsubsection{Single Oracle Algorithm}
%Single oracle algorithm is an adaptation of the double oracle algorithm introduced in~\cite{bosansky2015sequence}.
%It is often used when one player's action space is very large (in our case the attacker's).
%Single oracle overcomes the difficulty by introducing a \emph{restricted game}, which contains only a subset of the player actions.
%It iteratively extends the restricted game until it contains an equilibrium.
%
%For ZS EFG games it was shown in~\cite{bosansky2014exact} that this approach can be used to find the NE, which is equivalent to the SSE in ZS games~\cite{korzhyk2011stackelberg}.

%For nZS games the algorithm is well defined, but no guarantee on convergence to an equilibrium holds; however, it may find a suboptimal solution very fast.
%Thus, we use SO algorithm to solve ZS approximations (referred as to ZS1-4) as well as the original nZS game, to which we refer as to SO.


\subsection{Single Linear Program Game Approximation}\label{sec:slp}
The main motivation for this approximation is the concept of correlated equilibria and an extension of the Stackelberg equilibrium, in which the leader commits to a correlated strategy.
This concept has been used in normal-form games~\cite{conitzer2011commitment} and stochastic games~\cite{letchford2010computing}. %, or security games \cite{xu2015}.
In this case, the leader is not only committing to a mixed strategy but also to signal the follower an action the follower should take such that the follower has no incentive to deviate.
By allowing such a richer set of strategies, the leader can gain at least the same utility as in the standard Stackelberg solution concept. 

Unfortunately, computing commitments to correlated strategies is again an NP-hard problem in general extensive-form games with imperfect information and chance nodes (follows from Theorem 1.3 in \cite{von2008extensive}).
Moreover, the improvement of the expected utility value for the leader can be arbitrarily large if commitments to correlated strategies are allowed \cite{letchford2010computing}.
On the other hand, we can exploit these ideas and the linear program for computing the Stackelberg equilibrium \cite{conitzer2011commitment}, and modify it for the specific structure of our games.
This results in a novel linear program for computing an upper bound on the expected value of the leader in a Stackelberg equilibrium in our game in Section~\ref{sec:slp}.
