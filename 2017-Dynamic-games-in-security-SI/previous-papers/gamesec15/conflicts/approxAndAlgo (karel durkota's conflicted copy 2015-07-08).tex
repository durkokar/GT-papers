\section{Game Approximations and Algorithms}\label{sec:algorithms}
In this section we present a collection of \emph{approximated games} for Stackelberg deception games that allow us to apply algorithms that solve the games faster. %in polynomial time.
The approximations are based on relaxing certain aspects of the original game.

%\textbf{Perfect-Information Game Approximation~~~}
\subsection{Perfect-Information Game Approximation~~~}
A straightforward game approximation is to remove the attacker's uncertainty about the nature's and defender's action, which results in a perfect-information game.
In~\cite{durkota2015optimal} the authors propose an algorithm to solve a game which corresponds to the subgame after the nature's move.
We use their algorithm to solve each of the subgames after the nature move and combine their best strategies to construct an optimal strategy for the defender, to which we refer as PI.
%This game is solvable in linear time w.r.t. the size of the game.
%Solving the approximated game implies finding pure strategy profiles $\pi=(\pi_d^*,\pi_a^*)$ that satisfy the SSE constraints, which can be done by backward induction in linear time w.r.t. the size of the game.
%This is a very pessimistic assumption for the defender, since he has no way of concealing the honeypot allocations.


%\textbf{Zero-sum Imperfect-Information~~~}
\subsection{Zero-sum Imperfect-Information}\label{sec:zerosum}
In~\cite{korzhyk2011stackelberg} the authors show that under certain conditions approximating the non-zero-sum game as a zero-sum game, which can be solved in polynomial time w.r.t. the size of the game-tree, can provide an optimal strategy for the original game.
In the following approximations we use a similar idea for constructing zero-sum game approximations.
%In \cite{korzhyk2011stackelberg} it is showed that in the zero-sum games set of Nash Equilibria (NE) equal to SSE; thus, finding it's NE (which is polynomially hard problem) we solve the game.

Recall that in our game the defender's utility is $u_d=-R-H$ and the attacker's utility is $u_a=R-C$.
Our game has a payoff structure where $R$ is the zero-sum component in the utilities and the smaller the difference $|H-C|$ in the outcomes, the closer our game become a zero-sum game.
We propose several variants of \emph{zero-sum} game approximations, where one or both players utilities' are modified to satisfy the zero-sum condition $u_d=-u_a$. 

We focused on the following four zero-sum setting:
both players consider only the expected rewards from the attack policy (ZS1) $u_d = -R$, 
only attacker's utilities are considered (ZS2) $u_d = -R+C$,
only defender's utilities are considered (ZS3) $u_d = -R-H$, and
the defender keeps his original utility with adversarial motivation to harm the attacker (ZS4) $u_d = -R-H+C$.

When solving the zero-sum approximation games, we avoid generating exponential number of attacker's attack policies by using Single-Oracle (SO) algorithm, introduced in~\cite{mcmahan2003planning}.
It is often used when one player's action space is very large (in our case the attacker's).
SO overcomes this difficulty by introducing a \emph{restricted game}, which contains only a subset of the player actions.
It iteratively extends the restricted game until it contains an equilibrium.
In zero-sum EFG games it was shown in~\cite{jain2011double} that this approach can be used to finds NE, which is equivalent to SSE in zero-sum games.
For non-zero-sum games no such guarantee holds; however, it may find a suboptimal solution fast.
Thus, we use SO algorithm to solve zero-sum approximations (referred as to ZS1-4) as well as the original game, to which we refer as to SOGS.

Each SO iteration consists of two steps.
(i) First, the SSE of the restricted game is computed; for zero-sum games we use LP to find SSE and for general-sum the \emph{mixed integer linear program} (MILP)~\cite{bovsansky2015sequence}.
(ii) Second, the attacker computes best response to the defender's strategy from SSE.
We compute the attacker's best response for each of his information set separately.
The probability of each network in the information set is derived from the defender's strategy, according to which we compute the attacker's optimal attack strategy.
The attack graphs for the network in an information set have the same structure, but the actions may have different probabilities of interacting with the honeypots, depending on the defender's action.
In~\cite{durkota2015optimal} the authors present a MDP approach with several pruning techniques for computing the optimal attack policy for a single attack graph.
We extend their approach by translating the problem of computing optimal attack policy for the set of attack graphs with the prior probability distribution into partially observable MDP (POMDPs).
The attacker has a \emph{belief} about the set of attack graphs he computes optimal attack policy for.
We compute optimal attack policy with value iteration algorithm on POMDPs and use Bayes rule to update the attacker's belief about the attack graphs due to action's different probabilities of interacting with the honeypots.

Due to the space limit, we present an example to show the intuition behind the update rule.
Assume that the attacker acts in information set $z_1$ from Fig.~\ref{fig:game} and his prior belief of being in left and right state of the game is $(a,b)$, respectively, where $a+b=1$.
Performing action $a_3$ has 0.5 probability of touching honeypot in the left state and zero in the right state of the game.
Thus, if action is performed successfully and does not interact with a honeypot, we update the attacker's belief to $(\frac{0.2a}{0.2a + 0.4b},\frac{0.4b}{0.2a+0.4b})$, where numerator is the probability of reaching the state and the denominator is a normalization factor.
Update belief according to this rule for every attacker's action.
While computing the optimal attack policy, we also use similar pruning techniques as described in~\cite{durkota2015optimal}.

\subsection{Single LP approximation}
\begin{figure}
	\centering
	\includegraphics[width=.9\textwidth]{fig/LP}
	\caption{Illustration of extensive-form game translation into set of normal-form games}
	\label{fig:EFGtoLP}
\end{figure}
The authors in~\cite{conitzer2011commitment} present a single linear program (LP) for computing the SSE for a normal-form game (NFG).
The LP finds the probability distribution over the outcomes with maximal utility for the defender under the condition that the attacker plays a best response (does not have incentive to deviate).
We exploit our extensive-form game's structure which allows us to write the game as a collection of normal-form games and formulate the problem as a single LP.
We demonstrate our approach on the example in Fig.~\ref{fig:EFGtoLP}.
For each of the attacker's information sets we construct a normal-form game where the defender chooses an action that leads to the corresponding information set and the attacker chooses an attack policy in the that information set.
In Fig.~\ref{fig:EFGtoLP} in information set $I1$ the defender chooses between actions $y1$ and $y3$ and in $I2$ between actions $y2$ and $y4$.
Each terminal node in the EFG corresponds to an outcome in the NFG ($p_1$ through $p_8$ in our example).
Furthermore, we incorporate nature's actions by adding a constraint that the sum of outcome probabilities for each subgame after nature acts must equal the probability that Nature chooses this subgame.
In our example, the probabilities of outcomes $p_1,p_2$ (defender's action $y1$) and $p_3,p_4$ (defender's action $y2$) must sum to $1/2$, the probability that nature plays action $x1$.
The LP formulation as follows:
{\footnotesize
	\begin{subequations}
	\begin{align}
		\textrm{max} &\sum_{x\in X} \sum_{y\in Y} \sum_{s\in S_{x+y}} p(x,y,s)u_d(x,y,s) \\
		\textrm{s.t.}: & (\forall I_a\in \mathcal{I}_a,s_1,s_2\in S_{I_a}): \sum_{(x,y)\in I_a}P(x,y,s_1)u_a(x,y,s_1) \geq \sum_{(x,y)\in I_a}P(x,y,s_1)u_a(x,y,s_2) \\
		%&\sum_{x\in X(I_a)}\sum_{y\in Y(I_a)}P(x,y,s_1)u_a(x,y,s_1) \geq \sum_{x\in X(z)}\sum_{y\in Y(z)}P(x,y,s_1)u_a(x,y,s_2) \\
		& \sum_{x\in X}\sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s)=1 \\
		& (\forall x\in X,y\in Y,s\in S_{(x,y)}): P(x,y,s)\geq 0 \\
		& (\forall x\in X):  \sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s) = \delta_x
	\end{align}
\end{subequations}
}
where $p(x,y,s)$ is the probability of reaching terminal state of the game where nature plays $x$, the defender $y$ and the attacker $s$.
%The result of the LP is a probability distribution over all terminal nodes of the game.
The solution of the LP is an upper bound on the defender's expected utility in the SSE for following reason.
Our LP maximizes the defender's expected utility under the constraint that the attacker plays best response (which is a constraint for SSE as well) and structural constraints caused by Nature.
However, we do enforce the constraint that the attacker plays a pure best-response (single action) in the information set.
Therefore, our constraints are weaker than the constraints for SSE.
Which implies that this LP does not excluded any SSE.
Thus, the value of the LP (referred as to SSEUB) may be used as an upper bound, and can be computed in polynomial time to the size of the game.

However, formulation our game as a single LP requires finding all attacker's attack policies for each of his information sets.
As we mentioned earlier, there is exponential number MDP policies for an attack graph.
However, we can reduce this number by considering only \emph{rationalizable} (introduced in~\cite{bernheim1984rationalizable}) attack policies.
An attack policy is rationalizable if and only if it is attacker's best response to some  belief state about the attack graphs.
Considering only set of all rationalizable attack policies in the game, we do not exclude any SSE due to the constraint that the attacker plays best-response (which is rationalizable attack policy) in it.
Therefore, we first compute the set of \emph{rationalizable} attack policies for each information set and we consider only them in the LP.
%The attack policy is rationalizable iff there exist probability distribution of the networks in an information set, such that the attack policy is the best-response to it.
We find set of rationalizable attack policies for every information set using our POMDPs approach explained in Sec.~\ref{sec:zerosum}.
We use standard approach as described in~\cite{russell2009artificial} together with techniques of elimination of the dominant policies and the some pruning techniques for solving optimal attack policy for a single attack graph.

From the LP results we extract the defender's strategy as a marginal probability for each action:
the probability that defender's plays action $y\in Y$ in state  $x\in X$ is $\sum_{s\in S_{(x,y)}} P(x,y,s)$.
We will refer to this mixed strategy as $\sigma_d^{LP}$ and to the defender's utility in the strategy profile where the defender plays $\sigma_d^{LP}$ and the attacker best response strategy to as SLP; formally, SLP$=u_d(\sigma_d^{LP}, BR_a(\sigma_d^{LP}))$.

