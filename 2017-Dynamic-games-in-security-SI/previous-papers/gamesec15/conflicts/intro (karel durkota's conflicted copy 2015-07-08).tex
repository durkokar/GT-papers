\section{Introduction}
Networked computer systems support a wide range of critical functions in both civilian and military domains.
Securing this infrastructure is extremely costly and there is a need for new automated decision support systems that aid human network administrators to detect and prevent the attacks.

We focus on network security hardening problems in which a network administrator (defender) reduces the risk of attacks on the network by setting up honeypots (HPs) (fake hosts or services) in their network~\cite{qassrawi2010deception}. 
Legitimate users do not interact with HPs; hence, the HPs act as decoys and distract attackers from the real hosts.
HPs can also send intrusion detection alarms to the administrator, and/or gather detailed information the attacker's activity~\cite{provos2004virtual,grimes2005honeypots}.
Believable HPs, however, are costly to set up and maintain.
On the other hand, a well-informed attacker anticipates the use of HPs and tries to avoid them.
Therefore the problem of deciding which services to duplicate (i.e., \emph{allocating honeypots}) can be modeled using the game-theoretic framework.


%in ~\cite{carroll2011game,cai2009attacker} the authors propose a game-theoretic model that studies various camouflaging signals that honeypots can send to the attacker in order to minimize the chance of being detected.
%Deciding how to optimally allocate honeypots to reduce the risk of attacks on a network presents a challenging decision for the defender.  
%On the other hand, a well-informed attacker should anticipate the use of honeypots and try to avoid them.

Our game-theoretic model extends the long existing line of Stackelberg models used in the physical security domains~\cite{tambe2011}.
The model presented in this paper advances the state of the art by a novel combination of two elements: 
(1) we adopt a compact representation of strategies for attacking computer networks called \emph{attack graphs}, 
(2) the defender uses \emph{deception} by exploiting \emph{imperfect information} the attacker has about the attacked network.
While some form of a compact representation have previously been used \cite{letchford2013optimal,durkota2015optimal}, they were assuming the attacker has much more detailed information and almost perfect information about the network.
Deception that exploits imperfect information of the attacker about the network have been proposed before~\cite{pibil2012game}; however, the existing model uses a zero-sum assumption and very abstract one step attack actions.
Both of these restrictions are removed in our model.

Attack graphs (AGs) compactly represent a rich space of sequential attacker actions for compromising a specific computer network.
AGs can be automatically generated based on known vulnerability databases~\cite{ingols2006practical,ou2006scalable} and they are widely used in the network security to identify the minimal subset of vulnerabilities/sensors to be fixed/placed to prevent all known attacks~\cite{sheyner2002,noel2008optimal}, or to calculate security risk measures (e.g., the probability of a successful attack)~\cite{noel2010measuring,homer2013aggregating}.
We use AGs as a compact representation of an attack plan library, from which the rational attacker chooses the optimal contingency plan to follow.
%However, finding the optimal attack plan in an attack graph is an NP-hard problem~\cite{greiner2006finding}.
%We address this issue by translating attack graphs into an MDP and introducing a collection of pruning techniques that reduce the computation considerably.

%Deploying honeypots changes the structure of the network and increases uncertainty for the attacker.
%In this game model we assume that the attacker knows the number of deployed honeypots and their type (e.g., a database server).
%However, the attacker does not know which specific hosts are honeypots and which are real.
%While the assumption that the attacker knows the number/type of honeypots is strong, it corresponds to a worst-case, well-informed attacker. 
%Our model could also be extended to include uncertainty about these variables, but it would further increase the computational cost of solving the model. 

The action of the defender is to select which services or hosts will be allocated as HPs in order to minimize the costs for the deployed HPs and successful attacks.  
We assume that the attacker knows the overall number of HPs, but does not know which types of services defender actually allocated as HPs. 
This is in contrast to~\cite{durkota2015optimal}, where authors assumed that the attacker knows which types of services contain HPs and the uncertainty was only about which specific server/computer is real or not.

%begin added
%Finding an optimal honeypot allocation in the proposed game is a NP-hard problem which makes the approach impractical for large networks.
%We introduce several relaxation aspects of the problem that overcome the computation complexity at the cost of finding suboptimal solution.
%end edit

Solving games with stochastic events and imperfect information is generally NP-hard~\cite{letchford2010computing}.
Algorithms that compute the optimal solution in this class of games do not scale to real-world settings~\cite{bovsansky2015sequence}. 
Therefore 
(1) we present a novel collection of approximations for Stackelberg deception games that are solvable in polynomial time, and 
(2) we experimentally show that the strategies computed in the approximated models are often very close to the optimal strategies in the original model, e.g., strategy for zero-sum approximation had only 0.13\% of relative regret.
Finally, (3) we propose novel algorithms to compute upper and lower bounds on the expected utility of the defender in the original game to allow the evaluation of the strategies from the approximate models.

%We present five main contributions: (1) a novel game-theoretic model and approximative models of security hardening based on attack graphs, (2) algorithms to solve the proposed approximative models, (3) empirical evaluation of the computational scalability and limitations of the algorithms, and (4) quality analyzing of the hardening solutions.
%We present five main contributions: (1) a novel game-theoretic model of security hardening based on attack graphs, (2) algorithms for analyzing these games, including fast methods based on MDPs for solving the attacker's planning problem, (3) a case study analyzing the hardening solutions for sample networks, (4) empirical evaluation of the computational scalability and limitations of the algorithms, and (5) sensitivity analysis for the parameters used to specify the model.
