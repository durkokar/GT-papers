\section{Introduction}
Networked computer systems support a wide range of critical functions in both civilian and military domains.
Securing this infrastructure is extremely costly and there is a need for new automated decision support systems that aid human network administrators to detect and prevent the attacks.
We focus on network security hardening problems in which a network administrator (defender) reduces the risk of attacks on the network by setting up honeypots (HPs) (fake hosts or services) in their network~\cite{qassrawi2010deception}. 
Legitimate users do not interact with HPs; hence, the HPs act as decoys and distract attackers from the real hosts.
HPs can also send intrusion detection alarms to the administrator, and/or gather detailed information the attacker's activity~\cite{provos2004virtual,grimes2005honeypots}.
Believable HPs, however, are costly to set up and maintain.
Moreover, a well-informed attacker anticipates the use of HPs and tries to avoid them.
To capture the strategic interactions, we model the problem of deciding which services to deploy as honeypots using a game-theoretic framework.

%modified
Our game-theoretic model is motivated in part by the success of Stackelberg models used in the physical security domains~\cite{tambe2011}.
One challenge in network security domains is to efficiently represent the complex space of possible attack strategies, we make use of a compact representation of strategies for attacking computer networks called \emph{attack graphs}.
%We advances the state of the art by introducing a more realistic attacker with imperfect information about the attacked network.}
Some recent game-theoretic models have also used attack graphs~\cite{letchford2013optimal,durkota2015optimal}, but these models had unrealistic assumptions that the attacker has perfect information about the original network structure.
The major new feature we introduce here is the ability to model the imperfect information that the attacker has about the original network (i.e., the network structure before it is modified by adding honeypots). 
Imperfect information of the attacker about the network have been proposed before~\cite{pibil2012game}, however, the existing model uses a zero-sum assumption and very abstract one step attack actions.
Both of these restrictions are removed in our model.

Attack graphs (AGs) compactly represent a rich space of sequential attacks for compromising a specific computer network.
AGs can be automatically generated based on known vulnerability databases~\cite{ingols2006practical,ou2006scalable} and they are used in the network security to identify the minimal subset of vulnerabilities/sensors to be fixed/placed to prevent all known attacks~\cite{sheyner2002,noel2008optimal}, or to calculate security risk measures (e.g., the probability of a successful attack)~\cite{noel2010measuring,homer2013aggregating}.
We use AGs as a compact representation of an attack plan library, from which the rational attacker chooses the optimal contingency plan.

The defender in our model selects which types of fake services or hosts to add to the network as honeypots in order to minimize the trade-off between the costs for deploying HPs and reducing the probability of successful attacks.  
%The action of the defender in our model is to select which types of fake services or hosts will be added to the network as honeypots in order to minimize the trade-off between the costs for deploying HPs and reducing the probability of successful attacks.  
We assume that the attacker knows the overall number of HPs, but does not know which types of services the defender actually allocated as HPs. 
This is in contrast to previous work~\cite{durkota2015optimal}, where the authors assumed a simplified version to our game, where the attacker knows the types of services containing HPs.
The uncertainty in the existing model is only about which specific service/computer is real among the services/computers of the same type.
Our model captures more general (and realistic) assumptions about the knowledge attackers have when planning attacks, and we show that the previous perfect information assumptions can lead to significantly lower solution quality. 

%This is in contrast to~\cite{durkota2015optimal}, where authors assumed that the attacker knows which types of services contain HPs and the uncertainty was only about which specific server/computer is real or not.

%begin added
%Finding an optimal honeypot allocation in the proposed game is a NP-hard problem which makes the approach impractical for large networks.
%We introduce several relaxation aspects of the problem that overcome the computation complexity at the cost of finding suboptimal solution.
%end edit

Generalizing the network hardening models to include imperfect information greatly increases the computational challenge in solving the models, since the models must now consider the space of all networks the attacker believes are possible, which can grow exponentially. 
Computing Stackelberg equilibria with stochastic events and imperfect information is generally NP-hard~\cite{letchford2010computing} and algorithms that compute the optimal solution in this class of games typically do not scale to real-world settings~\cite{bosansky2015sequence}. 
Therefore we 
(1) present a novel collection of polynomial time algorithms that compute approximate solutions by relaxing certain aspects of the game,
(2) experimentally show that the strategies computed in the approximated models are often very close to the optimal strategies in the original model, %, e.g., strategy for the best zero-sum approximation had only 0.13\%\kdnote{<- correct the number according to the new experiments} of relative regret,
%(3) a novel approach for computing the upper bound of the defender's utility for Strong Stackelberg Equilibrium, which we use to measure the quality of the strategies found with approximation algorithms and finally,
and (3) propose novel algorithms to compute upper bounds on the expected utility of the defender in the original game to allow the evaluation of the strategies computed by the approximate models even in large games.

%We present five main contributions: (1) a novel game-theoretic model and approximative models of security hardening based on attack graphs, (2) algorithms to solve the proposed approximative models, (3) empirical evaluation of the computational scalability and limitations of the algorithms, and (4) quality analyzing of the hardening solutions.
%We present five main contributions: (1) a novel game-theoretic model of security hardening based on attack graphs, (2) algorithms for analyzing these games, including fast methods based on MDPs for solving the attacker's planning problem, (3) a case study analyzing the hardening solutions for sample networks, (4) empirical evaluation of the computational scalability and limitations of the algorithms, and (5) sensitivity analysis for the parameters used to specify the model.
