%!TEX root = main.tex
\section{Algorithms}
%We now describe the details of the algorithms for calculating the approximations.

\subsection{Single Oracle}\label{sec:so}
The single oracle algorithm is an adaptation of the double oracle algorithm introduced in~\cite{bosansky2014exact}.
It is often used when one player's action space is very large (in our case the attacker's).
The single oracle algorithm uses the concept of a \emph{restricted game}, which contains only a subset of the player's actions.
%It iteratively extends the restricted game until it contains an equilibrium.

In iteration $m$ the single oracle algorithm consists of the following two steps:
(i) compute the attacker's best response $\hat{\pi}_\at^m=BR_\at(\sigma_\de^{m-1})$ to the defender's strategy $\sigma_\de^{m-1}$ in the full game.
If all actions of the strategy $\hat{\pi}_\at^m$ are already contained in the restricted game, the algorithm returns a SSE of the restricted game.
Otherwise, (ii) extend the restricted game by including the best response action $\hat{\pi}_\at^m$, compute a new SSE strategy profile $(\pi_\at^m,\sigma_\de^m)$ and go to step (i) with incremented iteration counter $m$.
The algorithm starts with uniform strategy $\sigma_\de^0$ (the defender plays every action with a uniform probability), and a restricted game that contains all actions of nature and the defender and none of the actions of the attacker. % and iteration counter $m=1$.
We use this algorithm to solve all four variants of the ZS approximations proposed in Sec.~\ref{sec:zerosum}.
We refer to the strategies found using the SO algorithm on the ZS game approximations as SOZS. 

The SO algorithm is also well defined for GS games and can be directly applied to the original game.
However, it does not guarantee that the computed SSE of the restricted game is also the SSE of the original game.
The reason is that the algorithm can converge prematurely without containing all the attacker's actions in the restricted game that are required to compute SSE of the original game.
Nevertheless, the algorithm may find a good strategy in a short time.
We apply this algorithm to our GS game and use \emph{mixed integer linear program} (MILP), described in~\cite{bosansky2015sequence}, to compute the SSE of the restricted game in each iteration.
Finding a solution for a MILP is an NP-complete problem, so this algorithm is not polynomial.
We refer to this algorithm that applies SO to the GS game directly as SOGS.

%The SSE of the restricted game is computed using LP if game is zero-sum and using \emph{mixed integer linear program} (MILP)~\cite{bosansky2015sequence} if the game is nZS.
%The algorithm starts with uniform strategy $\sigma_d^0$ (the defender plays every action with a uniform probability), restricted game containing all natures and defender's action and no attacker actions and iteration counter $i=1$.

%\subsection{Optimal Attack Policy as MDP}\label{sec:OAPasMDP}

\subsection{Attacker's Optimal Attack Policy}\label{sec:OAPasPOMDP}
The attacker's best response $\pi_\at=BR_\at(\sigma_\de)$ to the defender's strategy $\sigma_\de$ is computed by decomposing the problem into the subproblems of computing the optimal attack policy for each of the attacker's information sets separately.
We can do that because the subgame after an information set does not interleave (do not have any common state) with any subgame originating from a different information set.
%a prior probability distribution of networks in the information set. 
We calculate the probability distribution of the networks in an information set based on $\sigma_\de$, which is the attacker's prior belief about the probabilities about the states in the information set.
The networks in an information set produce the same attack graph structure.
However, the actions may have different probabilities of interacting with the honeypots depending on the defender's honeypot deployment on the path to that network.

\subsubsection{Single Network}\label{sec:mdp}
We first describe an existing algorithm that finds the optimal attack policy for a single attack graph (AG) for a network, introduced in~\cite{durkota2015optimal}.
The algorithm translates the problem of finding the optimal attack policy for an AG into a finite horizon \emph{Markov Decision Process} (MDP), which is then solved using backward induction.
A state in the MDP is represented by: (i) the set of attack actions $\alpha$ available to the attacker in that state (according to the AG), (ii) the set of compromised host types and (iii) the set of host types that the attacker interacted with so far.
In each state the attacker can perform an action from $\alpha$.
The action has a probabilistic outcome of either being performed successfully (s), failing (f), or interacting with a honeypot (h).
The sets that represent the MDP state are updated based on the AG, the performed action and its outcome (e.g., new actions that became available to perform are added to $\alpha$, the performed action is removed from it, etc.), which represents a new MDP state.
If the action successfully compromises a host type $t$, reward $r_t$ is assigned to that transition.
The MDP can be directly incorporated into the game tree, where the attacker chooses an action/transition in each of his states and stochastic events are modeled as chance nodes.
The rewards are summed and presented in the terminal states of the game.
The authors propose several pruning and speed-up techniques, such as dynamic programming, branch and bound and a sibling-class theorem, which we also adopt.

%The transitions are stochastic with probabilities corresponding to the action success probabilities and probabilities of interacting with the honeypots.
%An action changes state of the attack graph in several ways, e.g., (i) performed action is removed from the attack graph, (ii) if action succeeds, then its effects are removed from the attack graph, etc.
%States in MDP correspond to the state of the attack graph and transition between the MDP states corresponds to the actions in the attack graph.
%Actions that succeeds and achieve rewards have corresponding reward for the corresponding transitions in MDP.
%In the game tree in Fig.~\ref{fig:game}, the subgame after the defender's action a part of the MDP.
%E.g., in state $z_1$ the attack graph is full attack graph for network $z_1$, where attacker can perform actions 3, 1 and 0.
%Performing action 3 have can have following outcomes: with probability 0.5 it interacts with a honeypot, with probability 0.2 the action succeeded and with probability 0.3 it failed.
%If action 3 succeeds the attacker is rewarded with $r_3 = 10$ and leads to the modified state with deleted action 3 and its reward (not in the Fig.).
%They find the optimal policy with maximal expected utility with the \emph{value iteration} (also called \emph{backward induction}), which is a bottom-up algorithm.
%The key idea of the algorithm is in the following recursion: in a decision state the attacker explores each actions, assuming that after that he acts optimally, and chooses the one that yields the highest expected reward.
%Then, the algorithm propagates the utility and optimal attack policy from that state to the bottom (including the chosen action in that state).
%This MDP has exponential number of states to the size of the attack graph, hence, finding optimal attack policy is an NP-hard problem.
%We use this algorithm in PI approximation described in Sec.~\ref{sec:pi}.
%More details are in the paper~\cite{durkota2015optimal}.

\subsubsection{Multiple Networks}
The previous algorithm assumes that the MDP states can be perfectly observed.
One of our contributions in this paper is an extension of the existing algorithm that finds the optimal attack policy for a set of networks with a prior probability distribution over them.
The attacker has imperfect observation about the networks.
We translate the problem into a POMDP.
Instead of computing the backward induction algorithm on single MDP, we compute it concurrently in all MDPs, one per network in the information set.
In Fig.~\ref{fig:game} we show a part of the POMDP for information set $I_1$, which consists of two MDPs, one for network $z_1$ and another for $z_2$.

The same action in different MDPs may have different transition probabilities, so we use Bayes rule to update the probability distribution among the MDPs based on the action probabilities.
Let $J$ be the number of MDPs and let $\beta_j(o)\in\mathbb{R^+}$ be the probability that the attacker is in state $o$ and in MDP $j=1,\dots,J$. % in information set $I$. %, so that $\sum_{z_i\in I}b(z_i)=1$.
Performing action $a$ leads to a new state $o'$ with probability $P_j(o,o',a)$. %in information set $I$, leads to a new information set $I'$.
The updated probability of being in $j$-th MDP given state $o'$ is $\beta_j(o') = \frac{P_j(o,o',a) \beta_j(o)}{\sum_{j'=1}^{J}P_{j'}(o,o',a)\beta_{j'}(o)}$. 
%\emph{For example}, let state $o$ be the first MDP state in each information set $I_1$ in Fig.~\ref{fig:game} with probabilities $\beta_1(o)=0.5$ and $\beta_2(o)=0.5$.
%The probability of successfully performing action $3$ is 0.2 in the first MDP and 0.6 in the second.
%After performing successfully action $3$, which leads to state $o'$ (the information set $I_2$ in Fig.~\ref{fig:game}) we update the probabilities as $\beta_1(o')=\frac{0.2b_1(o)}{0.2\beta_1(o) + 0.4\beta_2(o)}=\frac{1}{3}$ and $b_2(o')=\frac{0.4\beta_2(o)}{0.2b_1(o)+0.4\beta_2(o)}=\frac{2}{3}$.

This algorithm returns the policy with the highest expected reward given the probability distribution over the networks.
During the optimal attack policy computation, we use similar pruning techniques to those described in~\cite{durkota2015optimal}.
%This algorithm is at least NP hard since finding optimal attack policy for one MDP is already NP hard.
%We use this algorithm as a subroutine in the SO algorithm for ZS and GS games.


\subsection{Linear Program for Upper Bounds}\label{sec:slp}
In~\cite{conitzer2011commitment} the authors present a LP that computes SSE of a matrix (or normal-form) game in polynomial time.
The LP finds the probability distribution over the outcomes in the matrix with maximal utility for the defender under the condition that the attacker plays a best response.
We represent our game as a collection of matrix games, one per attacker's information set, and formulate it as a single LP. % with additional constraints for the defender's actions probabilities to follow the network flow constraint. % of defender's action to follow the probabilities of the nature actions.
%The solution of LP returns probability of the game outcomes which corresponds to the probability that players choose action which leads to that outcome.
%We exploit our extensive-form game's structure and model our game the as a collection of matrix games and formulate them as a single LP.

%Formally, let $I^\at\subseteq\mathcal{I}$ be the set of all attacker's information sets. % where the attacker selects his attack policy (acts for the first time).
Formally, for each attacker's information set $I_j\in \mathcal{I}$ we construct a matrix game where the defender chooses action $y\in Y$ that leads to a state in the information state $I_j$ and the attacker chooses an attack policy $s_b\in S_{I_j}$ from the set of all attack policies $S_{I_j}$ for information set $I_j$. % from all attack policies information set $I_j$.
%\vlnote{this is strange: $\mathcal{I}$ was previously the set of all attacker's IS, $S_I$ was the optimal strategy in I. Is this inconsistency necessary?}
The  outcomes in the matrix game coincide with the outcomes in the original game.
Additionally, we restrict the sum of probabilities of the outcomes that originate from nature's action $x$ to equal to $\delta_x$.
%Additionally, we restrict the defender's action probabilities to follow the probability flow of the nature's actions.
The LP formulation follows:
%{\footnotesize
	\begin{subequations}
	\begin{align*}
		\textrm{max} &\sum_{x\in X} \sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s)u_d(x,y,s) \\
		\textrm{s.t.}: & (\forall I\in \mathcal{I},s_1,s_2\in S_{I}): \sum_{(x,y)\in I}p(x,y,s_1)u_a(x,y,s_1) \geq \sum_{(x,y)\in I}p(x,y,s_1)u_a(x,y,s_2) \\
		%&\sum_{x\in X(I_a)}\sum_{y\in Y(I_a)}P(x,y,s_1)u_a(x,y,s_1) \geq \sum_{x\in X(z)}\sum_{y\in Y(z)}P(x,y,s_1)u_a(x,y,s_2) \\
		& (\forall x\in X,y\in Y): \sum_{x\in X}\sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s)=1 \\
		& (\forall x\in X,y\in Y,s\in S_{(x,y)}): p(x,y,s)\geq 0 \\
		& (\forall x\in X):  \sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s) = \delta_x,
	\end{align*}
\end{subequations}
where the only variables are $p(x,y,s)$, the probability of reaching a terminal state of the game where nature plays $x$, the defender $y$ and the attacker $s$.


%Furthermore, we incorporate the nature action probabilities by adding constraint one for each nature action $x_a$, that probability $\delta_a$ equals to the sum of probabilities of outcomes that can be reached via nature's action $x_a$.

An example follows to demonstrate our approach on the game in Fig.~\ref{fig:EFGtoLP}.
Information sets $I_1$ and $I_2$ in the game in Fig.~\ref{fig:SLPgame} correspond to the two matrix games in Fig.~\ref{fig:EFGtoLP}.
The defender chooses between action $y_1$ and $y_3$ in $I_1$ and between $y_2$ and $y_4$ in $I_2$.
The probabilities of the terminal states of the game tree correspond to the outcomes probabilities in the matrix games ($p_1$ through $p_8$).
Additionally, the outcome probabilities $p_1$ through $p_4$ must sum to $\delta_1$, since they root from the same nature's action $x_1$ and the same holds for $p_5$ through $p_8$ and $\delta_2$.

This LP has weaker restrictions on the solution compared to the MILP formulation for SSE~\cite{bosansky2015sequence} since it does not restrict the attacker to play a pure best response strategy.
The objective is to maximize the defender's utility, as in the MILP.
Therefore, it does not exclude any SSE of the game.
The value of this LP, referred to as SSEUB, is an upper bound on the defender's expected utility when playing an SSE.

%Although this LP allows the attacker to play mixed best response, the authors proved that in the optimal solution of the LP the attacker will play pure best response.

\begin{figure}[t!]
	\centering
	\begin{subfigure}{.49\textwidth}
		\resizebox{7cm}{!}{
			\usebox{\SLP}
		}
		\caption{}
		\label{fig:SLPgame}
	\end{subfigure}
	\begin{subfigure}{.49\textwidth}
		\centering
		\includegraphics[width=.5\textwidth]{fig/LPmatrix}
		\caption{}
		\label{fig:EFGtoLP}
	\end{subfigure}
	\caption{The extensive-form game in (a) translated into two normal-form games in (b).}
	\label{fig:comparison}
\end{figure}
%We exploit our extensive-form game's structure and write our game the as a collection of normal-form games and formulate them as a single LP.
%We demonstrate our approach on the example in Fig.~\ref{fig:EFGtoLP}.
%Let $I^\at\subseteq\mathcal{I}$ be the set of all attacker's information sets where he chooses his first action (the attack policy $s$).
%For each information set $I_j\in I^\at$ we construct a normal-form game where the defender chooses an action $y_a$ that leads to information set $I_j$ and the attacker chooses an attack policy $s_b\in S_{I_j}$ from all attack policies information set $I_j$.
%E.g., in Fig.~\ref{fig:EFGtoLP} in information set $I_1$ the defender chooses between actions $y_1$ and $y_3$ and in $I_2$ between actions $y_2$ and $y_4$.
%Each terminal node in the EFG corresponds to an outcome in one of NFG ($p_1$ through $p_8$ in our example).
%Furthermore, we incorporate the nature action probabilities by adding constraint one for each nature action $x_a$, that probability $\delta_a$ equals to the sum of probabilities of outcomes that can be reached via nature's action $x_a$.
%In our example, the probabilities of outcomes $p_1,p_2$ (defender's action $y1$) and $p_3,p_4$ (defender's action $y2$) must sum to $1/2$, since they root from the same nature's action $x_1$ played with probability $1/2$.
%
%The LP formulation follows:
%{\footnotesize
%	\begin{subequations}
%	\begin{align}
%		\textrm{max} &\sum_{x\in X} \sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s)u_d(x,y,s) \\
%		\textrm{s.t.}: & (\forall I\in I^\at,s_1,s_2\in S_{I}): \sum_{(x,y)\in I}p(x,y,s_1)u_a(x,y,s_1) \geq \sum_{(x,y)\in I}p(x,y,s_1)u_a(x,y,s_2) \\
%		%&\sum_{x\in X(I_a)}\sum_{y\in Y(I_a)}P(x,y,s_1)u_a(x,y,s_1) \geq \sum_{x\in X(z)}\sum_{y\in Y(z)}P(x,y,s_1)u_a(x,y,s_2) \\
%		& \sum_{x\in X}\sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s)=1 \\
%		& (\forall x\in X,y\in Y,s\in S_{(x,y)}): p(x,y,s)\geq 0 \\
%		& (\forall x\in X):  \sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s) = \delta_x
%	\end{align}
%\end{subequations}
%}
%where the only variables are $p(x,y,s)$, the probability of reaching terminal state of the game where nature plays $x$, the defender $y$ and the attacker $s$.

%Let us compare our LP formulation to the MILP formulation from~\cite{bosansky2015sequence} that computes SSE and explain why our LP finds an upper bound for the defender's expected utility of SSE.
%MILP consists of (i) constraints that the follower plays best response, (ii) constraints that the follower plays pure strategy and (iii) constraints for the probability flow conservation.
%The objective is to maximize the leader's utility.
%Our LP consists of constraint that the follower plays best response and the probability flow conservation, with the objective to maximize the defender's utility.
%Our LP has weaker restrictions on the solution than MILP, hence, it cannot excluded any SSE that is found by MILP.
%The value of the LP (referred to as SSEUB) is an upper bound for SSE which can be computed in polynomial time to the size of the game.
%We use SSEUB value to measure the quality of the approximation algorithm's strategies.

The drawback of formulating our game as a single LP is that it requires finding all attack policies in advance in order to construct the linear program.
As mentioned earlier, there are an exponential number of MDP policies for an attack graph.
We reduce this number by considering only \emph{rationalizable} (introduced in~\cite{bernheim1984rationalizable}) attack policies for an information set.
An attack policy is rationalizable if and only if it is the attacker's best response to some belief about the networks in an information set. %position of honeypots.
The set of all rationalizable attack policies is called \emph{Closed Under Rational Behaviour} (CURB) set~\cite{benisch2010algorithms}.
By considering only the CURB set for the attacker, we do not exclude any SSE with the following rationale.
Any attack policy that is in SSE is the set of attacker best responses, so it must be rationalizable and therefore it must be in the CURB set.
%In Sec.~\ref{sec:CURBasPOMDP} we explain how we compute the curb set.

From the LP result we extract the defender's strategy as a marginal probability for each defender's action: the probability that defender plays action $y\in Y$ in state  $x\in X$ is $\sum_{s\in S_{(x,y)}} P(x,y,s)$.
We will refer to this mixed strategy as $\sigma_\de^{SLP}$ and to the defender's utility in the strategy profile $u_d(\sigma_\de^{SLP}, BR_\at(\sigma_\de^{SLP}))$ as SLP.

\subsubsection{CURB for Multiple Networks}\label{sec:CURBasPOMDP}
We further extend the best response algorithms to compute the CURB set.  
We use the \emph{incremental pruning} algorithm~\cite{cassandra1997incremental}, a variant of the backward induction algorithm that in every attacker decision state propagates the CURB set of attack policies for the part of the POMDP that begins in that decision state.
Let $A$ be a set of actions in a decision state $o$. % and $J$ the number of MDPs in our POMDP. 
The algorithm is defined recursively as follows.
(i) Explore each action $a\in A$ in state $o$ and obtain the CURB set of policies $S^a$ for the part of the POMDP after the action $a$; (ii) for every action $a\in A$ extend each policy $s_b\in S^a$ to begin with action $a$ in the current state $o$ and then continue with policy $s_b$; (iii) return the CURB set from the union of all policies $\cup_{a\in A}S_a$ for state $o$.
In step (iii) we use the standard \emph{feasibility linear program} to check whether policy $s_b$ is in the CURB set by finding if there exists a probability distribution between MDPs where $s_b$ yields the highest utility among $\cup_{a\in A} S^a \setminus s_j$, as described in~\cite{benisch2010algorithms,cassandra1997incremental}.

