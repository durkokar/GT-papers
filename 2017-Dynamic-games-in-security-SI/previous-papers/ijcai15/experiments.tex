\section{Experiments}
The experiments analyze the algorithm for optimal attack policy computation and evaluate game models for network hardening problems.
All experiments were run on one core of Intel i7 3.5GHz processor with 10GB memory limit.
Attack graphs for the experiments were generated with MulVAL~\cite{ou2005mulval} tool, augmented with additional cost and probability information.

Network topologies for experiments are depicted in Fig.~\ref{net:main} (Main-$i$), \ref{net:local} (Local-$i$) and \ref{net:localPlus} (Local+$i$), where $i$ is: (i) number of workstation client vulnerabilities in Local-$i$ , (ii) number of workstations in Local+$i$  and (iii) number of workstation groups in Main-$i$.
Host types are labeled with $(r,l,c)$, where $r$,$l$,$c$ denotes the numbers of \emph{remote}, \emph{local} and \emph{client} vulnerabilities for that type.
The topology of the network and the number and type of vulnerabilities determines the number of actions in the attack graph.


\subsection{Optimal Attack Planning}

\subsubsection{Pruning Techniques}
\begin{figure}[t]
        \centering
        \begin{subfigure}[b]{0.24\textwidth}
		\includegraphics[width=\textwidth]{fig/plots/algScalability/plotForTimesLocal}
                \caption{Local-$i$}
                \label{fig:timeL}
        \end{subfigure}%
        \begin{subfigure}[b]{0.24\textwidth}
		\includegraphics[width=\textwidth]{fig/plots/algScalability/plotForTimesLocalPlus}
                \caption{Local+$i$}
                \label{fig:timeLP}
        \end{subfigure}
\\
\caption{Time comparison of the pruning techniques. Legend: none - no pruning technique, (-LB) - all but lower bound, (-UB) - all but upper bound, (-ST) - all but Sibling Theorem, (all) - all pruning techniques.}
\label{fig:timeComp}
\end{figure}
In this section we evaluate the contribution of the pruning techniques: Sibling Theorem (ST), lower bound (LB), and upper bound (UB) for the branch and bound.
Since techniques may prune the same branches, we measure each technique's contribution by measuring the algorithms slowdown after disabling it.
Thus, we compare 5 settings: \mbox{\emph{none}} (no techniques is used), \mbox{\emph{all}} (all techniques are used), \mbox{\emph{-ST}} (all but ST), \mbox{\emph{-LB}} (all but LB) and \mbox{\emph{-UB}} (all but UB).
Fig.~\ref{fig:timeComp} shows optimal attack policy computation times of networks Local-$i$ and Loca+$i$ using each technique setting.
In Local-$i$ network we add vulnerabilities to the workstation, creating large decision points.
However, ST can effectively prune them and choose the best action (compare \emph{all} to \mbox{\emph{-ST}}).
In Local+$i$ network the branch and bound helps the most (\mbox{\emph{-LB}} and \mbox{\emph{-UB}}).
In the remaining of experiments, we include all techniques since they do not have negative impact and can dramatically speed up the computation.
We refer to the proposed attack planning algorithm as DP (dynamic-programing).

\subsubsection{Comparison with Other Algorithms}
We compare DP to other two approaches that can compute the optimal policy for an attack graph.
The first method converts the AG it to an \emph{Unconstrained Influence Diagram} (UID) as described in~\cite{lisy2013computing} and uses GUIDO~\cite{isa2007guido} to solve it.
The second approach translates AG to a probabilistic planning domain problem, which is then solved by SPUDD~\cite{hoey1999spudd}.
It uses iterative value computation and was 3rd best planner in 2011 at the International Planning Competition (IPC) in the MDP track.
We chose SPUDD because it guarantees to return an optimal contingency policy, while the first two planners from IPC do not.
In Tab.~\ref{tab:tabs}(a) we present computation times of the algorithms.
Our algorithm dramatically outperforms the other two approaches, where the hardest network Local+8 was solved 5x faster than using SPUDD planner.


\begin{table}[t]
	\centering
	\begin{tabular}{@{}cc@{}}
		{\tiny
			\begin{tabular}{|r|c|c|c|c|}
				\hline
				Problem & \# A & DP & UID & SPUDD \\\hline 
				Local3 & 9 &\textbf{$<$1} &	\textbf{$<$1} & \textbf{$<$1} \\
				Local11 & 25 & \textbf{$<$1} &	(OoM) &  3 \\
				Local19 & 41 &\textbf{$<$1} &	(OoM) &  3348 \\ \hline
				Local+3 & 16 &\textbf{$<$1}&	71   & 1  \\ 
				Local+6 & 28 &\textbf{3} &	(OoM) &  125 \\ 
				Local+8 & 36 &\textbf{406} &(OoM) &  1951 \\ \hline
			\end{tabular}
		}
		& 
		{\tiny
			\begin{tabular}{|c|c|c|c|c|}
				\hline
				host & 1 & 2 & 3 & 5 \\\hline
				db & 0 & 0 & 1 & 2 \\
				srv & 1 & 1 & 1 & 2 \\
				vpn & 0 & 1 & 1 & 1 \\
				1grp & 0 & 0 & 0 & 0 \\
				2grp & 0 & 0 & 0 & 1 \\ \hline
		\end{tabular}} \\
		(a) & (b)
	\end{tabular}
	\caption{(a) Computation times (in seconds) of computing optimal attack policies by DP, GUIDO and SPUDD. (OoM) - Out of Memory, (Err) - planner exited with segmentation error. (b) Optimal allocation of different number of HPs (columns) to the host types (rows) in the Main-7 network.}
\label{tab:tabs}
\vspace{-0.2cm}
\end{table}



\subsection{Network Hardening Game Analysis}
\begin{figure*}[t]
        \centering
        \begin{subfigure}[b]{0.24\textwidth}
                \includegraphics[width=\textwidth]{fig/plots/game/scalabilityGame-crop}
                \caption{Scalability}
                \label{fig:scalabilitygame}
        \end{subfigure}%
        \begin{subfigure}[b]{0.24\textwidth}
		\includegraphics[width=\textwidth]{fig/plots/game/plotUtil12andMinAvg-crop}
                \caption{Defender's loss}
                \label{fig:utilityPer}
        \end{subfigure}%
	\begin{subfigure}[b]{0.24\textwidth}
		\includegraphics[width=\textwidth]{fig/plots/game/plotSepUtilityandCost-crop}
		\caption{Defender's costs}
		\label{fig:sepUtilityandCost}
	\end{subfigure}%
	\begin{subfigure}[b]{0.24\textwidth}
		\includegraphics[width=\textwidth]{fig/sens/plotForAdelta}
		\caption{Sensitivity}\label{fig:sens}
		\label{fig:sens}
	\end{subfigure}%
	%\end{subfigure}
		\caption{(a) Time comparison of solving the game for various networks and the number of HPs; (b) Defender's expected loss given the $\gamma$ and allocating HPs optimally and randomly. (c) Individual parts of the defender's loss: the expected cost of having the network attacked (Attack cost) and cost of deploying the HPs (Honeypot cost). (d) Defender relative regret given the perturbation of action costs, success probabilities and rewards in networks with 1, 3 and 6 HP, and 95th percentile for 6 HPs.}\label{fig:utility}
\end{figure*}


In this section we evaluate the network hardening. 
We focus on a typical small business network topology depicted in Fig.~\ref{fig:fig1}c (Main-$i$) and we find honeypot deployment that minimizes the defender's loss in this network.
Attacker's actions costs in AG are set randomly between 1 and 100.
The rewards for compromising the host types are 1000 for workstations (ws), 2000 for vpn, 2500 for server (srv) and 5000 for database (db).
For simplicity, we assume that the defender values ($l_t$) them the same as the attacker. 
However, this payoff restriction is not required in our model.
The defender pays $c_{hp}(t)=\gamma l_t$ for adding honeypot of type $t$, where $\gamma\in[0,1]$ is parameter we alter.
It corresponds to the fact that more valuable hosts are generally more costly to imitate by honeypots.

\subsubsection{Scalability Experiment}
We analyze the scalability of our game model to determine the size of the networks that can reasonably be solved using our algorithms.
For each network Main-$i$, we create a game model and find Stackelberg equilibrium.
In Fig.~\ref{fig:scalabilitygame} we present computation times (note log-scale y-axis) to solve networks Main-6 through Main-8 (individual plots), given the number of HPs that the defender can deploy.
The algorithm scales exponentially with both parameters. 
However, we note that typical real-world cases will have few honeypots. 



\subsection{Case-Study}
In this section we examine in detail the network Main-7 with $\gamma=0.02$.
We analyze the defender's loss dependence on the number of honeypots and his relative regret for modeling the attack graph inaccurately (e.g., if he estimates the action costs, action probabilities or rewards incorrectly).

\subsubsection{Defender's loss}
Using a large number of HPs is not necessarily beneficial for the defender as they may cost more than they contribute to protecting the network.
The defender's expected loss using different number of the HPs is shown in Fig.~\ref{fig:utilityPer} (see ``optimal, $\gamma=0.02$''), where it is optimal to deploy 6 HPs for $\gamma=0.02$ and for 9 HPs for the cheaper $\gamma=0.01$ setting.
In the same figure, we also present the defender's loss for deploying random honeypots instead the optimal suggested by our model, which is roughly twice as bad. % In Fig.~\ref{fig:utilityPer} we also contrast the defender's loss of placing HPs randomly instead of optimally, 
We also note how dramatically merely 3 HPs can decrease the expected loss using the game-theoretic approach.

In Fig.~\ref{fig:sepUtilityandCost} are separate parts of the defender's loss: the expected loss for having the hosts attacked and the costs for deploying the HPs.
Interestingly, with 16 HPs the defender decides to use less expensive HPs and put the network in higher risk, since this choice results in a lower overall expected loss.

In Tab.~\ref{tab:tabs}(b) we present the defender's optimal HP types allocations (rows) for a given total number of deployable HPs (columns).
I.e., with one HP, it is best to duplicate the server (srv).
The server is not the most valuable type (2500 while the database is worth 5000).
However, it is a bottleneck of the network; protecting the server partially defends both the server and the database, rather than only database.
With two HPs it is best to duplicate the server and VPN (again, the bottlenecks).
Only with the 3rd HP does the defender duplicate the database.




\subsubsection{Sensitivity Analysis}
Computing the defender's optimal strategy heavily relies on the Attack Graph structure and knowledge of the action costs, probabilities and rewards, which the defender can only approximate in a real-world scenarios.
In the following experiments we analyze the sensitivity of the defender's strategy and utility to inaccurate approximation of the attack graphs.

We use the following notation: defender's strategy $\bar{d}$ (resp. loss $\bar{l}_{\bar{d}}$) is computed from the attack graph known to the defender, which imprecisely approximates the attack graph truly describing attacker's behavior; $d$ (resp. loss $l_d$) is the optimal strategy based on the true attack graph according which the attacker really behaves; and $l_{\bar{d}}$ is the defender's loss when strategy $\bar{d}$ is used against the real attack graph.


In the sensitivity analysis we compute the \emph{relative regret} as $regret = \lvert l_{\bar{d}} - l_d\rvert / \bar{l}_{\bar{d}}$, which is the defender's relative loss ratio for not knowing the attacker's real attack graph.
In other words, if the defender knew the true attack graph model and acted optimally, this is how much he would gain.
Thus, $regret=0$ means he has no regret and $regret=1$ means that his loss is by 100\% greater than it could have been.



In this experiments the attack graph action probabilities and costs were chosen randomly from the intervals $p_a\in[0,1]$, $c_a\in[0,200]$ in order not to depend on the specific initial attack graph values provided by MulVAL.
To generate the imprecise attack graph available to the defender, we perturbed the generated attack graph based on values $(\delta_p,\delta_c,\delta_r)\in[0,1]^3$, where each value represents the limit on the size of the perturbation on action probabilities ($\delta_p$), costs ($\delta_c$) and fact rewards ($\delta_r$).
	%\item $Norm(\Delta u) = \frac{\Delta u}{u_d(AG^o,\bar{d}_{AG^o},\bar{\xi}_{AG^o}^{\bar{d}_{AG^o}})} = \frac{u_d(AG^r,\bar{d}_{AG^o},\bar{\xi}_{AG^r}^{\bar{d}_{AG^o}}) - u_d(AG^o,\bar{d}_{AG^o},\bar{\xi}_{AG^o}^{\bar{d}_{AG^o}})}{u_d(AG^o,\bar{d}_{AG^o},\bar{\xi}_{AG^o}^{\bar{d}_{AG^o}})}$. $\Delta u$ is the defender's difference between the expected utility of observed AG and expected utility of real AG.
The perturbed attack graph is obtained as follows: for each action $a\in A$ the perturbed probability $\bar{p}_a$ is chosen from an interval $[p_a-\delta_p, p_a+\delta_p]$ restricted to $[0.05,0.95]$ to prevent them becoming impossible ($\bar{p}_a=0$) or infallible ($\bar{p}_a=1$); perturbed cost $\bar{c}_a$ is chosen from $[c_a(1-\delta_c), c_a(1+\delta_c)]$; and for each fact $f\in F$, the perturbed fact reward is $\bar{r}_f \in [ r_f(1-\delta_r), r_f(1+\delta_r)]$, where the values are selected uniformly within the given intervals.
Action probabilities are perturbed absolutely (by $\pm \delta_p$), but the costs and rewards are perturbed relative to their original value (by $\pm \delta_c c_a$ and $\pm \delta_r r_f$).
The intuition behind this is that the higher the cost or reward values the larger the errors the defender could have made while modeling them, which cannot be assumed for probabilities.

In our experiments we perturbed the each component from $0.05$ to $0.95$ by $0.05$ steps and measured the defender's loss.
The results presented are mean values computed from 100 runs.
In Fig.~\ref{fig:sens}  we present the defender's relative regret for increasing error perturbations for deploying 1, 3 and 6 HPs, where we perturbed all three perturbation components.
The results show the solutions are fairly robust, i.e., assuming that the defender models all components inaccurately by at most 30\% ($\delta_c=\delta_p=\delta_r=0.3$) in scenario with 6 HPs, his HP deployment choice could have been better off by only 17\%.
We also examined the effect of perturbing each component individually, however, due to the space limit, we did not include figures (which are similar). 
Out of the three components, the defender's strategy was most sensitive to action probability perturbations, where the relative regret reaches value 0.8 for the case when $\delta_p=0.95$ for 6 HPs.
The sensitivity to reward perturbations reached relative regret loss of about 0.3 for $\delta_r=0.95$.
Results were least sensitive to action cost perturbations, resulting in relative regret loss below the value 0.002 even for $\delta_c=0.95$.
Fig.~\ref{fig:sens} also presents the 95th percentile of the regret values for the case with 6 HPs to show the robustness in extreme cases.

Note that with an increasing number of HPs, the defender's relative regret grows (e.g., Fig.~\ref{fig:sens}), however, this can be misleading.
Further data analysis revealed that the actual \emph{absolute regrets} $\lvert l_{\bar{d}} - l_d\rvert$ for 3 HPs (318) and 6 HPs (340) are very similar.
The reason why relative regret seem to grow with the increasing number of HPs is due to the decrease of the observed loss $\bar{l}_{\bar{d}}$ (denominator it relative regret formula). %in the observed loss'.
The observed loss $\bar{l}_{\bar{d}}$ for 3 HPs is 1994, while for 6 HPs 1049, which shows that the relative regret does not seem to depend on the number of HPs.

