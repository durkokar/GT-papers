\section{Introduction}
%Importance of network security
Networked computer systems support a wide range of critical functions in both civilian and military domains.
Securing this infrastructure is extremely costly and there is a need for new automated decision support systems that aid human network administrators to detect and prevent attacks.

We focus on network security hardening problems in which a network administrator (defender) reduces the risk of attacks on the network by introducing honeypots (fake hosts or services) into their network~\cite{qassrawi2010deception}. %\footnote{Honeypots are used as an example of a real network hardening action that is based on deception, but our methods could be extended to other types of hardening actions.}
Legitimate users do not interact with honeypots; hence, honeypots act as decoys and distract attackers from the real hosts, send intrusion detection alarms to the administrator, and/or gather detailed information the attacker's activity~\cite{provos2004virtual,grimes2005honeypots}.
However, believable honeypots are costly to set up and maintain.
For example, in ~\cite{carroll2011game,cai2009attacker} the authors propose a game-theoretic model that studies various camouflaging signals that honeypots can send to the attacker in order to minimize the chance of being detected.
%To minimTheir signals in different forms to the attacker have been studied using game theoretic approaches to minimize their detection probability~\cite{carroll2011game,cai2009attacker}
Deciding how to optimally allocate honeypots to reduce the risk of attacks on a network presents a challenging decision for the defender.  
On the other hand, a well-informed attacker should anticipate the use of honeypots and try to avoid them.

We use game theory to model this adversarial interaction and to determine the best way to use honeypots against a well-informed attacker.
We introduce a novel game-theoretic model of network hardening using honeypots that extends the existing line of Stackelberg security games~\cite{tambe2011} by combining two elements: 
(1) we adopt a compact representation of strategies for attacking computer networks called \emph{attack graphs}, 
(2) the defender uses \emph{deception} instead of directly allocating resources to harden targets.

Attack graphs (AGs) can represent a rich space of sequential attacker actions for compromising a specific computer network.
AGs can be automatically generated based on known vulnerability databases~\cite{ingols2006practical,ou2006scalable} and they are widely used in the network security to identify the minimal subset of vulnerabilities/sensors to be fixed/placed to prevent all known attacks~\cite{sheyner2002,noel2008optimal}, or to calculate security risk measures (e.g., the probability of a successful attack)~\cite{noel2010measuring,homer2013aggregating}.
We use AGs as a compact representation of an attack plan library, from which the rational attacker chooses the optimal contingency plan to follow.
However, finding the optimal attack plan in an attack graph is an NP-hard problem~\cite{greiner2006finding}.
We address this issue by translating attack graphs into an MDP and introducing a collection of pruning techniques that reduce the computation considerably.

%Attack graphs (AGs) allow representing a rich space of attacker's action sequences across a specific computer network.
%They can be automatically generated based on known vulnerability databases~\cite{ingols2006practical,ou2006scalable}.
%The most common analysis of attack graphs includes identifying the optimal subset of vulnerabilities that need to be removed to prevent all known attacks~\cite{sheyner2002} or finding a minimal set of sensors to be placed to the network to detect all known attacks~\cite{noel2008optimal}.
%Other research focuses on computing network-wide security measures and using them to choose countermeasures that result in the highest network security~\cite{noel2010measuring,homer2013aggregating}.
%Our approach differs significantly in that we compute optimal contingent attack policies rather than some abstract measures.
%These policies provide enough information to determine the actions a rational attacker would use and the order in which they would be executed.
%However, finding the optimal attack plan in an attack graph is an NP-hard problem~\cite{greiner2006finding}.
%We address this issue by translating attack graphs into an MDP and introducing a collection of pruning techniques that speed-up the computation time.

Deploying honeypots changes the structure of the network and increases uncertainty for the attacker.
In this game model we assume that the attacker knows the number of deployed honeypots and their type (e.g., a database server).
However, the attacker does not know which specific hosts are honeypots and which are real.
While the assumption that the attacker knows the number/type of honeypots is strong, it corresponds to a worst-case, well-informed attacker. 
Our model could also be extended to include uncertainty about these variables, but it would further increase the computational cost of solving the model. 

We present five main contributions: (1) a novel game-theoretic model of security hardening based on attack graphs, (2) algorithms for analyzing these games, including fast methods based on MDPs for solving the attacker's planning problem, (3) a case study analyzing the hardening solutions for sample networks, (4) empirical evaluation of the computational scalability and limitations of the algorithms, and (5) sensitivity analysis for the parameters used to specify the model.
