\section{Network Hardening Game Using Honeypots}

In this section we introduce a game-theoretic model for the network hardening problem.
Our model is a Stackelberg game, where the defender acts first, taking actions to harden the network by adding honeypots (HPs). 
The attacker is the follower that selects an optimal attack plan based on (limited) knowledge about the defender's strategy. 
In particular, we assume that the attacker learns the number and type of HPs added to the network, but \emph{not} which specific hosts are real and fake.
%This is a pessimistic assumption for the defender because real attackers may not acquire this information. 
%Using Stackelberg equilibrium instead of Nash equilibrium has significant computational advantages.
%In many security games, it has also been shown that the Stackelberg equilibrium solution is also a Nash equilibrium of the simultaneous version of the game~\cite{korzhyk2011stackelberg}.

An instance of the game is based on a specific computer network like the one shown in Fig.~\ref{fig:fig1}c (based on~\cite{homer2009sound}). 
A network has a set of host types $T$, such as firewalls, workstations, etc. 
Two hosts are of the same type if they run the same services and have the same connectivity in the network (i.e., a collection of identical workstations is modeled as a single type).
All hosts of the same type present the same attack opportunities, so they can be represented only once in an attack graph.
During an attack, a specific host of a given type is selected randomly with uniform probability. % (the selection remains the same until the end of the game).

%\begin{figure}[t!]
%\textbf{function} $sovleGame(y,T,k)$      
%\begin{algorithmic}[1]
%\State $X$ $\gets$ all combinations of choosing 1 through $k$ HPs of $T$ types;
%\State $x^* = \emptyset$;
%\State $l^* = \infty$;
%\ForAll {$x\in X$}
%\State $z \gets x + y$;//new network $z$ with $x_t$ HPs and $y_t$ for each type $t$
%\State $AG \gets$ generate attack graph for $z$:
%\State $\xi^* \gets$ \emph{ \textbf{ compute optimal attack policy for $AG$}};
%\State $l(x)\gets$ return defender's expected loss for action $\xi^*$ in $AG$;
%\If {$l(x)<l^*$}
%\State $l^*\gets l(x)$;
%\State $x^*\gets x$;
%\EndIf
%\EndFor
%\State \Return $(x^*)$;
%\end{algorithmic}
%\caption{Algorithm for computing the defender's optimal allocation of $k$ HP in the network $y$, with $T$ types.}
%\label{alg:game}
%\end{figure}

More formally, a computer network $y\in\mathbb{N}^T$ contains $y_t$ hosts of type $t \in T$.
The defender can place up to $k$ honeypots into the network $y$, so his actions are represented by  $x\in X \subset\mathbb{N}_0^T$ with $\sum_{t\in T}x_t\le k$, specifying that $x_t$ hosts type $t \in T$ will be added to the network as honeypots (e.g., by duplicating the configurations of the real hosts with obfuscated data).
The modified network consists of $z_t=x_t+y_t$ hosts of type $t$.
Adding more HPs of a specific type increases the likelihood that the attacker who interacts with this type of host will choose a HP instead of a real host.
If the attacker interacts with a HP during an attack, he is immediately detected and the attack ends.
The attacker is rational and maximizes the expected utility taking into account the probabilities of interacting with HPs, his actions' costs and success probabilities, and rewards from successful attacks. He selects his attack strategy from set $\Xi$ defined later.
Installing and maintaining HPs has a cost for the defender depending on the host type ($c(t)\; t\in T$) that is duplicated.
The defender minimizes his total expected loss $l$ which consists of (1) the expected loss for the attacked hosts and (2) the cost for adding the HPs into the network. 
%The defender minimizes the expected utility which consists of (1) the negative of the expected reward of the attack and (2) the cost for the HPs. 
The Stackelberg equilibrium is found by selecting the pure action of the defender that minimizes the expected loss under the assumption that the attacker will respond with an optimal attack~\cite{Conitzer2006}. If the attacker is indifferent between multiple attacks, it is typical to break ties in favor of the defender \cite{tambe2011}. The defender's action is

\begin{equation}
x^*=\argmin_{x\in X}  \{l(x,\argmax_{\xi\in\Xi} \{\mathbb{E}(\xi,x)\})\}.
\end{equation}

%In Fig.~\ref{alg:game} we present an algorithm for computing the defender's optimal honeypot allocation up to $k$ HPs into the network $y$ that contains $T$ types.
%For every defender action $x\in X$ the attacker computes the attack policy maximizing his expected reward $\xi^*=\argmax_{\xi\in\Xi} \{\mathbb{E}(\xi,x)$.
%Defender chooses such action $x^*$ minimizing his expected loss $x^*=\argmin_{x\in X} \{l(x^*,\xi^*)\}$.
The minimization over all defender actions is performed by systematically checking each option; however, the main computation burden of computing the optimal attack strategy is substantially reduced by caching and reusing results of subproblems that are often the same.
Computation of this equilibrium relies on computing the optimal attack policy as explained in the following section. 
