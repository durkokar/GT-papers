%j%\documentclass{article}
%j\documentclass{../aamas2015}
%j% Required Packages
%j%\usepackage{aaai}
%j%\usepackage{times}
%j%\usepackage{helvet}
%j%\usepackage{courier}
%j\setlength{\pdfpagewidth}{8.5in}
%j\setlength{\pdfpageheight}{11in}
%j
%j%BEGIN added by Durkota
%j\usepackage{tikz}
%j\usepackage[titletoc,toc,title]{appendix}
%j\usepackage{mathptmx}
%j%\usepackage{amsmath}
%j%\usepackage{amssymb}
%j\usepackage{wrapfig}
%j\usepackage{array}
%j%\usepackage{caption}
%j\usepackage{subcaption}
%j%\captionsetup{compatibility=false}
%j
%j\usepackage{comment}
%j\usepackage{graphicx}
%j\usepackage{multirow}
%j\setcounter{tocdepth}{3}
%j\usepackage{wrapfig}
%j\usepackage[utf8]{inputenc}
%j\usepackage{algpseudocode}
%j\usepackage{url}
%j\urldef{\mailsa}\path|{karel.durkota, viliam.lisy}@agents.fel.cvut.cz|
%j%\newcommand{\keywords}[1]{\par\addvspace\baselineskip \noindent\keywordname\enspace\ignorespaces#1}
%j\makeatletter
%j\def\BState{\State\hskip-\ALG@thistlm}
%j\makeatother
%j\makeatletter
%j\renewcommand{\boxed}[1]{\scalebox{0.7}{\text{\fboxsep=.2em\fbox{#1}}}}
%j\makeatother
%j\newcommand{\boxT}{\boxed{T}}
%j\newcommand{\pre}{\textup{pre}}
%j\newcommand{\eff}{\textup{eff}}
%j\newcommand{\diff}{{\it diff}}
%j\algrenewcommand\algorithmicindent{0.6em}%
%j\input{../notes}
%j\newtheorem{theorem}{Theorem}
%j\newtheorem{definition}{Definition}
%j\newtheorem{proposition}{Proposition}
%j\newtheorem{lemma}{Lemma}
%j
%j\begin{document}
\section{Proofs}
This appendix contains proofs of the propositions from the ``Optimal Network Security Hardening Using Attack Graph Games'' submitted to IJCAI 2015.

\subsection{Sibling-Class Theorem for Attack Graphs}
In this section we proof that Sibling Class Theorem may be used for Attack Graphs.
\begin{theorem}[Sibling class theorem]
	Let $\xi$ be an optimal attack policy for the attack graph $AG$. Then for any actions $x,y$ in the same sibling class, such that $R(y)>R(x)$, $x$ is never performed before $y$ in $\xi$.
\end{theorem}

The proofs for the AND and OR sibling classes are symmetric; hence, we focus only on the OR sibling class. The proof of this theorem in \cite{greiner2006finding} relies on a lemma about attack policies.
If we denote a tree rooted in action $x$ with subtree $\xi_+$ (when $x$ is successful) and subtree $\xi_-$ (when action fails) by $(x;\xi_+;\xi_-)$ then the lemma can be rephrased in the following way:

\begin{lemma}\label{lem:switch}
Let $x,y\in A$ be actions in the same OR-sibling class of an attack graph such that $R(y) > R(x)$. Let $\xi_{xy}$ be an optimal policy for the attack graph in the form $(x;\xi_+;(y;\xi_+;\xi_-))$, then $\xi_{yx}=(y;\xi_+;(x;\xi_+;\xi_-))$ is a valid policy with a higher expected reward than $\xi_{xy}$.
\end{lemma}

The proof of the Sibling class theorem in~\cite{greiner2006finding} works by induction on the number of attack actions in the optimal policy.
It is demonstrated on Figure~\ref{fig:siblings} taken form the article.
For contradiction, assume that there are actions $x$ and $y$ in the wrong order in the optimal attack policy.
Without loss of generality, assume a case with an action $x$ in the root of the policy and its sibling $y$ at several places in the negative branch of $x$ (Figure~\ref{fig:siblings}(a)).
The positive branch does not include $y$, because it is an OR-sibling of $x$, which means that the fact achieved by $y$ is already achieved by $x$, if it is successful.
The proof shows that the expected reward of the policy is not decreased if it starts as the negative branch of $x$ and action $x$ is executed just before action $y$ would be executed in the original policy (Figure~\ref{fig:siblings}(b)).
Then the proof uses the Lemma~\ref{lem:switch} to show that if each instance of the actions $x$ and $y$ are switched (Figure~\ref{fig:siblings}(c)), the expected reward of the policy is increased if the R-ratio of $y$ is higher then R-ration of $x$.
We argue that a very similar proof is applicable for the more general case of attack graphs defined above. 
The differences between the model in~\cite{greiner2006finding} and our model are:

\begin{figure*}[t]
\centering
\includegraphics[width=0.7\textwidth]{fig/siblingsProof}
\caption{Illustration of the proof of the sibling theorem} % taken from \cite{greiner2006finding}.}
\label{fig:siblings}
\end{figure*}

\begin{description}
\item[i)] The attack graph is in general a cyclic graph and not a tree.
\item[ii)] The attack graph has actions with a probability of success and costs in the inner nodes, and not only in the leaves. This creates preconditions for the actions.
\item[iii)] Different attacks give different rewards and it is not necessary to resolve the root node of the attack graph.
\item[iv)] The attack terminates when the expected cost of continuing the attack is higher than the expected reward, not when the root of the AND-OR tree is resolved.
\end{description}

The proof is based on properties of the optimal policy with very little reference to the structure of the AND-OR graph.
The only use of the AND-OR graph is to assure that after the transformations, the policy is still a legal policy.
From the definition of the sibling classes, any action that belongs to a sibling class is a leaf of the attack graph.
It does not have any preconditions and it can be executed at any place in the policy.
The inner nodes of the attack graph are not moved in the tree and the transformations preserve any preconditions the following actions might have.
This means that properties (i) and (ii) do not have any effect on validity of the proof.

The policy in \cite{greiner2006finding} does not have any rewards, only costs for the actions.
The proof assumes that each subtree of the policy has an expected cost of execution, but it never requires the costs of the sub-trees to be positive.
We can define the expected reward (or negative cost) of a leaf node T as the sum of the rewards acquired in the attack branch from the root to the leaf.
Afterwards, we can treat this leaf as any other subtree in the proof. This resolves difference (iii).

The last difference between the models is caused by changing the point when the attack stops.
The attack stops at node T if the expected value of the optimal sub-policy continuing from this node is negative.
This is a solely a property of the successors of a node in the policy tree.
The transformations used in the proof do not change which actions can be executed below a current leaf in the policy and they do not change what other facts can be activated below the current leaves.
Therefore, it cannot be optimal to prolong any branch after the transformations.
Furthermore, no attack would be terminated earlier due to the transformations.
The attacker stops an attack if the expected reward of some subtree would become negative.
The proof initially assumes an optimal strategy and shows that the expected reward of the strategy stays the same when $x$ is moved just before $y$.
If the expected cost of playing the subtree rooted at the new position of $x$ were positive, stopping the attack at that place would increase the reward of the whole strategy, which contradicts the optimality of the original strategy.
The same holds after switching the positions of $x$ and $y$.
The original proof states that the reward is increased when the two are switched.
If in addition, some of the subtrees would have negative expected reward after the switch.
Removing the subtree would increase the expected reward even more, which still means that the original strategy was suboptimal.

%  \subsubsection{Counterexample with Honeypots}
%Interaction with a honeypot has a global effect which violates the monotonicity property of actions in attack graph, required by the Sibling Theorem.
%Here we present a counterexample from Fig.~\ref{fig:counterexample}.
%There are two strategies solving this attack graph.
%The first strategy $\xi_{ab}$ starts with action $a$, and if it fails then try action $b$.
%By interchanging actions $a$ and $b$ we obtain a second strategy, $\xi_{ba}$.
%Ratios of actions $a$ and $b$ are following: $R(a)=\frac{p_a}{c_a}=\frac{1}{10}=0.1$ and $R(b)=\frac{p_b}{c_b}=\frac{0.5}{1}=0.5$.
%The expected reward of strategy $\xi$ is computed as follows: 
%$$
%\mathbb{E}[\xi]=-c+(1-h_a)[p_a(R + \mathbb{E}[\xi^+])+(1-p_a)(\mathbb{E}[\xi^-])],
%$$ where $R$ is an immediate reward and $\mathbb{E}[\xi^+]$ (resp. $\mathbb{E}[\xi^-]$) the expected utilities after action $a$ succeeds (resp. fails).
%The expected rewards of the strategies are folowing: strategy $\xi_{ab}$ has expected reward $\mathbb{E}[\xi_{ab}]=-10+0.5(1*100+0(-1+0.5(0.5*100+0.5*0)))=40$ and strategy $\xi_{ba}$ has expected reward $\mathbb{E}[\xi_{ba}]=-1+0.5(0.5*100+0.5(-10+0.5*(1*100)))=34$.
%The Sibling Theorem states that if $R(b)>R(a)$, then strategy starting with action $b$ will have higher expected cost then the strategy starting with action $a$.
%However, the expected reward of strategy $\xi_{ab}$ is higher then the expected reward of strategy $\xi_{ba}$, which is the opposite of what Sibling Theorem states.


\subsection{Propositions and Lower Bounds}
In this section we proof used propositions and lower bounds.
We begin with the necessary propositions.
\begin{proposition}\label{prop:1}
	Let $\xi$ be an optimal attack policy starting with action $a$, $\mathbb{E}[\xi]$ be the expected cost of the policy $\xi$ and $\mathbb{E}[\xi_+]$ (resp. $\mathbb{E}[\xi_-]$) be the expected cost of subpolicy $\xi$ after action $a$ succeeds (resp. fails).
	Then $\mathbb{E}[\xi] \ge \mathbb{E}[\xi_-]$.
\end{proposition}
\begin{proof}
The first part we prove by contradiction.
Assume that $\mathbb{E}{[\xi]} < \mathbb{E}{[\xi_-]}$.
Then, due to the monotonicity property the attacker could have followed the (sub)policy $\xi_-$ before performing action $a$, which would have saved him the cost of the action $c_a$.
This new policy would have had higher expected value than the policy $\xi$, which contradict the optimality of $\xi$.
%$\blacksquare$
\end{proof}
\begin{proposition}\label{prop:2}
	Let $\xi$ be the optimal attack policy starting with action $a$, $\mathbb{E}[\xi]$ be the expected cost of the policy $\xi$ and $\mathbb{E}[\xi_+]$ (resp. $\mathbb{E}[\xi_-]$) be the expected cost of subpolicy $\xi$ after action $a$ succeeds (resp. fails).
	Then $\mathbb{E}{[\xi_+]} + \rho - c_a/p_a \ge \mathbb{E}{[\xi^-]}$, where $\rho=\sum_{f:f_a\setminus\phi}r_f$ is an pure immediate reward received if action $a$ succeeds.
\end{proposition}
\begin{proof}
	For convenience, we denote $\bar{p_a}=1-p_a$.
	\begin{eqnarray*}
		\mathbb{E}[\xi] = - c_a + p_a(\mathbb{E}[\xi_+] + \rho) + \bar{p_a}\mathbb{E}[\xi_-]\\
		\text{Prop.~\ref{prop:1}}\Rightarrow \mathbb{E}[\xi_-] \le -c_a + p_a(\mathbb{E}[\xi_+] + \rho) + {\bar{p_a}}\mathbb{E}[\xi_-]\\
		p_a\mathbb{E}[\xi_-] \le -c_a + p_a(\mathbb{E}[\xi_+]+\rho) \\
		\mathbb{E}[\xi_-] \le -c_a/p_a + \mathbb{E}[\xi_+] + \rho.
	\end{eqnarray*}
\end{proof}

\subsubsection{Lower Bounds}
In a decision point, the MDP search explores subtrees of all applicable actions one by one.
As each action subtree's expected reward is computed, we can their results to bound the future actions' subtrees.

\begin{proposition}
	Assume the actions $a_1,\dots,a_{k+1}\in A$ to be in the same decision point in state $s=(\alpha,\phi,\tau)$ of the depth-first search tree for MDP from which the subtrees of actions $a_1,\dots,a_k$ have been explored with their maximal expected reward $M$.
Let $\xi$ be the optimal attack policy starting with action $a_{k+1}$, $\mathbb{E}[\xi]$ be the expected cost of the policy $\xi$ and $\mathbb{E}[\xi_+]$ (resp. $\mathbb{E}[\xi_-]$) be the expected cost of subpolicy $\xi$ after action $a$ succeeds (resp. fails).

Strategy $\xi$ starting with action $a_{k+1}$ and followed optimally has higher expected reward then strategies starting with actions $a_1,\dots,a_{k+1}$ (and followed optimally) iff 
$$
\mathbb{E}[\xi_+] > M - \rho + \frac{c_{a_{k+1}}}{p_{a_{k+1}}}
$$ and 
$$
\mathbb{E}[\xi_-] > \tfrac{M+c_{a_{k+1}} - p_{a_{k+1}} (\mathbb{E}[\xi_+]+\rho)}{1-p_{a_{k+1}}},
$$where $\rho=\sum_{f:f_a\setminus\phi}r_f$ is the pure immediate reward for successfully performed action $a$.

\end{proposition}
	The proof of the first inequality:
\begin{proof}
\begin{align*}
	\mathbb{E}[\xi] = -c_{a_{k+1}} + p_{a_{k+1}} (\mathbb{E}[\xi_+] + \rho) + \bar{p_a} \mathbb{E}[\xi_-] > M \\
	-c_{a_{k+1}} + p_{a_{k+1}} (\mathbb{E}[\xi_+] + \rho) + \bar{p_{a_{k+1}}} (\mathbb{E}[\xi_+] +\rho - \frac{c_{a_{k+1}}}{p_{a_{k+1}}}) > M \\
	\mathbb{E}[\xi_+] > M - \rho + \frac{c_{a_{k+1}}}{p_{a_{k+1}}}.
\end{align*}
\end{proof}
The proof of the second inequality:
\begin{proof}
\begin{align*}
\mathbb{E}[\xi] &> M\\
	-c_a + p_a (\mathbb{E}[\xi_+] + \rho) + \bar{p_{a_{k+1}}} \mathbb{E}[\xi_-] &> M \\
	\mathbb{E}[\xi_-] > \tfrac{M+c_{a_{k+1}} - p_{a_{k+1}} (\mathbb{E}[\xi^+]+ \rho)}{\bar{p_{a_{k+1}}}}.
\end{align*}
\end{proof}

%\bibliographystyle{abbrv}
%\bibliography{../bib}
%\end{document}



