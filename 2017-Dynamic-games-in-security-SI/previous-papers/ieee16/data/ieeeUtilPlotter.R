library(plyr)
library(ggplot2)
library(reshape2)
library("grid")
library(scales)
setwd("~/Dropbox/ieee15/data/")
pdfTo <<- "~/Dropbox/ieee15/fig/"


printToPdf <- function(p,filename,width,height){
  pdf(paste(pdfTo,filename,sep=""),height=height,width=width)
  plot(p)
  dev.off()
}

printToPng <- function(p,filename,width,height){
  jpeg(paste(pdfTo,filename,sep=""))
  plot(p)
  dev.off()
}

datafile <- "utilCompare_singlePI"
#datafile <- "utilCompare"
saveTo <- paste(pdfTo, "utility",sep="")
filename <- "PIdefUtil"

data <- read.csv(datafile, header=FALSE, strip.white=TRUE, comment.char="#")
#c <- seq(1, length(data), 2)
#realNames <- as.character(unlist(data[1,c]))
#ealNames <- gsub("-","",realNames)
#c2 <-seq(2, length(data), 2)
#keeps <- names(data)[c2]
#data<- data[keeps]
#names(data) <- realNames

data <- subset(data, data$V1 <= 3)

#ddata <- ddply(data, .(k), summarize, meanTime = mean(TIME), sdTime = sd(TIME))
names(data)[names(data) == 'V7'] <- 'Penalty'
names(data)[names(data) == 'V8'] <- 'method'
names(data)[names(data) == 'hpCostFactor'] <- 'HP_cost_factor'
data$Penalty <- factor(data$Penalty)
m <- melt(data, id.vars = c("V1","method"), measure.vars = c("V2"))

plot <- ggplot(m, aes(x=V1, y=value, linetype=method, color=method)) + 
  geom_line() +
  xlab("number of honeypots") +
  ylab("defender's average utility") +
  scale_x_continuous(breaks = 0:9) +
  theme_light() 
#scale_colour_manual("", values = c("aaa" = "red", "bbb" = "black", "ccc" = "blue", "ddd" = "green"),labels = c('Company D','Company A','Company C','Company B')) +
  #scale_y_discrete(limits = c(-3000, -1000)) +
plot

printToPdf(plot, paste(filename,".pdf",sep=""), 5.5, 2.5)
#printToPng(plot, paste(filename,".png",sep=""), 5.5, 2.5)


