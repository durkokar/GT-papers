#!/bin/bash
for i in `ls *.dot`
do
	name=`echo $i | sed -e "s/.dot//"`;
	dot -Tpdf $i > ${name}.pdf;
	pdfcrop ${name}.pdf ${name}-crop.pdf;
done
