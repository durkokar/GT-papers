#!/bin/bash

for file in `ls *.svg`
do
	inkscape $file --export-pdf=${file:0:-3}pdf
	inkscape $file --export-dpi=250 --export-png=pngs/${file:0:-3}png
done
