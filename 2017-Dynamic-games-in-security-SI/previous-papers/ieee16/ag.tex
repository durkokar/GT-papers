\section{Model Overview}
%Our game is based on a specific computer network like the one shown in Figure~\ref{fig:ag}.
We model a network as a set of \emph{host-types}, e.g., WebServer or PC in Figure~\ref{fig:ag}.
Two hosts are of the same host-type if they run the same services, have the same network connectivity, and the same value. 
For example, a collection of workstations that run the same OS image are modeled as the same type.
Host-type PC in Figure~\ref{fig:ag} has ten equivalent hosts, all of which present the same attack opportunities.  
By representing each type only once in the attack graph we can scale with the number of unique types rather than individual hosts.   

%During an attack, a specific host of a given host-type is selected randomly with uniform probability. % (this can be extended for non-uniform selection without increasing the complexity of the models). % (the selection remains the same until the end of the game).

\subsection{Defending}
The defender places $k$ honeypots into the network by duplicating the configurations of existing hosts (with obfuscated data) or creating new host-types.
The defender pays a cost dependent on the host-type for each honeypot.  
Adding more honeypots of a specific host-type increases the likelihood that the attacker will interact with a honeypot instead of a real host.
If the attacker interacts with a honeypot during an attack, an alert is sent to the defender who immediately stops the attack or applies other countermeasures.

\subsection{Attacking}

We consider \emph{exploit} actions that target a specific vulnerability in a host.
Successfully executing an exploit results in the attacker gaining \emph{privileged} or \emph{non-privileged access} to that host.
%Pivot actions represent a facts that attacker can attack new host from a host already compromised.

{\bf An attack graph (AG)} compactly represents all known sequences of exploit actions for the host in the network. 
It captures the dependencies between the exploit \emph{actions} and true/false \emph{facts} that represent logical statement about the network state.
%Pivot actions represent attacker's ability to attack new host from a host already compromised.
Figure~\ref{fig:ag} depicts a possible AG for the network. 
Each action (rounded rectangular node) has preconditions and effects, represented by incoming and outgoing edges, respectively.
All preconditions must be true to perform the action.
If an exploit action succeeds, then all its effects become true and the attacker obtains rewards.
Initially true facts are represented with rectangle nodes, and initially false facts with diamond nodes.

In Figure~\ref{fig:ag}, action "2:Exploit Vulnerability" has preconditions: (Fact \#4) vulnerability exists and (Fact \#5) the attacker can access the Database on port 139.
If he successfully performs the action, he will obtain root-privileges to the Database (Fact \#1) and reward +1,000.
The \emph{success probability} is the likelihood that an exploit will succeed \emph{given} that all preconditions are met.
The \emph{cost} represents the attacker's monetary cost and effort for attempt to perform an action and the consequences of possibly disclosing the exploit.
Action \#2 in Figure~\ref{fig:ag} has success probability 0.4 and cost 5.
%Pivot actions have zero cost, since they represent only attacker's knowledge.

We use MulVAL~\cite{ou2006scalable} to automatically construct AGs from information collected by network scanning tools (i.e., Nessus or OpenVAS).
The action costs and success probabilities can be estimated using the Common Vulnerability Scoring System~\cite{mell2006common} or other data sources.   

%When the administrator duplicates an existing host-type as a honeypot, he increases the probability that the attacker who attacks that host-type will interact with honeypot instead of a real host.
%Adding a honeypot of new host-type creates new ``fake'' vulnerabilities in the network and the attack graph, which the attacker may attempt to exploit.

\begin{figure*}[t!]
	\includegraphics[width=\textwidth]{fig/surveyGameTree/gameTree3.pdf}
	\caption{Game tree for a simple network with host-types A and B, with their attack success probabilities 0.2 and 0.8, respectively.
		Chance plays uniformly into three possible networks, each contains two hosts.
		The defender chooses possible combinations of allocating two honeypots in each possible network; the blue probability values is a possible defense strategy (OPT$_d$ discussed in Section~\ref{sec:survey}).
		The attacker selects attack policy AP$_1$ through AP$_n$ according to the networks' attack graphs.
	}
	\label{fig:gameTree}
\end{figure*}

In our game models, the attacker chooses the optimal attack policy that fully characterizes the attacker's behavior at every point during the attack.
It specifies the \emph{order} of the actions to be executed by a rational attacker.
The problem of computing the optimal attack policy can be represented as a finite horizon Markov Decision Process (MDP). 
We use domain-specific MDP algorithms to find the optimal strategies~\cite{durkota2015optimal}.
