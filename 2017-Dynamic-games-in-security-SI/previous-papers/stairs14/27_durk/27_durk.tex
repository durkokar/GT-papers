\documentclass{IOS-Book-Article}

\usepackage{mathptmx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{graphicx}

%\usepackage{times}
%\normalfont
%\usepackage[T1]{fontenc}
%\usepackage[mtplusscr,mtbold]{mathtime}
%
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{conjecture}[theorem]{Conjecture}
%\newtheorem{example}[theorem]{Example}
\newcommand{\argmin}{\arg\!\min}

\newenvironment{proof}[1][Proof]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}
\newenvironment{definition}[1][Definition]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}
\newenvironment{example}[1][Example]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}
\newenvironment{remark}[1][Remark]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}]}{\end{trivlist}}

\newcommand{\pre}{\textit{pre}}
\newcommand{\eff}{\textit{eff}}

\newcommand{\qed}{\nobreak \ifvmode \relax \else
      \ifdim\lastskip<1.5em \hskip-\lastskip
      \hskip1.5em plus0em minus0.5em \fi \nobreak
      \vrule height0.75em width0.5em depth0.25em\fi}


\begin{document}
\begin{frontmatter}              % The preamble begins here.

%\pretitle{Pretitle}
\title{Computing Optimal Policies for Attack Graphs with Action Failures and Costs}
\runningtitle{Computing Optimal Policies for Attack Graphs}
%\subtitle{Subtitle}

\author{\fnms{Karel} \snm{Durkota}
}
and
\author{\fnms{Viliam} \snm{Lisy}}

\address{Agent Technology Center, Department of Computer Science, Faculty of Electrical Engineering, Czech Technical University in Prague\\
\texttt{\{karel.durkota, viliam.lisy\}@agents.fel.cvut.cz}}


\begin{abstract}
An attack graph represents all known sequences of actions that compromise a system in form of an and-or graph. We assume that each action in the attack graph has a specified cost and probability of success and propose an algorithm for computing an action selection policy minimizing the expected cost of performing an attack. We model the problem as a finite horizon MDP and use forward search with transposition tables and various pruning techniques based on the structure of the attack graph. We experimentally compare the proposed algorithm to a generic MDP solver and a solver transforming the problem to an Unconstrained Influence Diagram showing a substantial runtime improvement.
\end{abstract}

\begin{keyword}
optimal policy, attack graph, markov decision process, and-or graph
\end{keyword}
\end{frontmatter}

\thispagestyle{empty}
\pagestyle{empty}

\section*{Introduction}
Attack graphs (AG) are a popular tool for analysing and improving security of computer networks, but they can be used in any domain, where attacks consist of multiple interdependent attack actions. Attack graphs capture all the known sequences of actions that may lead to compromising a system, and they can contain additional information, such as the cost of individual actions and the probability that the actions will be successfully executed. AGs can be used to evaluate risks and design appropriate countermeasures.

In analysis of attack graphs, it is often of interest to identify the optimal strategy of the attacker (i.e., which actions to execute in what situation) and its expected cost.
For example, comparing the expected cost of the attack to the expected reward of successfully compromising the target indicates if a rational attacker would attack the system at all \cite{buldas2012upper}. In penetration testing, following the optimal attack strategy can save a lot of valuable time \cite{sarraute2011algorithm}. Computing the optimal strategy for the attacker is also a building block in solving various game-theoretic models of interaction between the attacker and defender of a system. Furthermore, a problem of computing the optimal attack strategy can also be seen as a complex variant of the generic problem of probabilistic and-or tree resolution analysed in AI research \cite{greiner2006finding}. 

In this paper, we propose an algorithm for computing the optimal attack strategy for an attack graph with action costs and failure probabilities. Unlike previous works assuming that the attack graph is a tree (e.g, \cite{sarraute2011algorithm}) and/or computing only a bound on the actual value (e.g., \cite{buldas2012upper}), we compute the exact optimum and we do not impose any restriction on the structure of the attack graph. Specifically, our approach allows the attack graph to contain (even oriented) cycles and to have actions with probabilities and costs as inner nodes of the attack graph.

The drawback of our approach is that even a simplified variant of this problem has been shown to be NP-hard in \cite{greiner2006finding}. As a result, we solve it by a highly optimized search algorithm and experimentally evaluate its scalability limitations. We show that the problem can be mapped to solving a finite horizon Markov decision process (MDP) and how the information about the structure of the attack graph can be used do substantially prune the search space in solving the MDP. We compare the proposed approach to recently published method for solving this problem \cite{lisy2013computing} and to a recent version of a generic MDP solver from the International Planning Competition 2011 \cite{spudd}, showing that the proposed method scales orders of magnitude better.


\section{Background and Definition}

\subsection{Attack Graph}
AG is a directed graph consisting of two types of nodes: (i) \emph{fact nodes}, that represent facts that can be either true or false, and (ii) \emph{action nodes}, that represent actions that the attacker can perform. Each action has \emph{preconditions} -- a set of facts that must be true before action is performed and \emph{effects} -- a set of facts that becomes true if action is successfully performed. Moreover, every action has associated probability $p \in (0,1]$ -- which denotes the probability that action succeeds and its effects become true, and with probability $1-p$ action fails and attacker cannot repeat this action anymore. We assume that attacker cannot repeat actions for couple of reasons: (i) if actions are correlated and have static dependencies (installed software version, open port, etc.), another attempts to use the same action would result alike, and (ii) if we allow infinitely many repetitions, optimal attack policy (explained further) would collapse into a linear plan with attempting for each action until action succeeds\cite{buldas2012upper}. Finally, each action has associated cost $c$; if attacker decides to perform action $a$, he will pay the cost $c$, regardless whether the action is successful or not.


\begin{definition}

 Let Attack Graph be a 5-tuple $AG = \langle \mathrm{F}, \mathrm{A}, \mathrm{g}, \mathit{p}, \mathrm{c} \rangle$, where:
 \begin{itemize}
 \item $\mathrm{F}$ is a finite set of facts
 \item $\mathrm{A}$ is a finite set of actions, where action $a: \pre \rightarrow \eff$, where $\pre \subseteq \mathrm{F}$ is called \emph{preconditions} (we refer to them as $\pre(a)$) and $\eff \subseteq \mathrm{F}$ is called \emph{effects we refer to them as $\eff(a)$}
 \item $\mathrm{g} \in \mathrm{F}$ is the goal
 \item $\mathit{p}: \mathrm{A} \rightarrow (0,1]$ is the probability of action to succeed (we use notation $p_a$ for probability of action $a$ to succeed, and with $p_{\bar{a}} = 1 - p_a$ the probability of action to fail)
 \item $\mathrm{c}: \mathrm{A} \rightarrow \mathbb{R}^{+}$ is cost of the action (we use notation $c_a$ for cost of the action $a$).
 \end{itemize}

\end{definition}
 We use the following terminology: we say that fact $f$ \emph{depends} on action $a$ if $f \in \eff(a)$, and similarly, action $a$ depends on $f$ if $f \in \pre(a)$
 

\begin{figure}[h!] 
  \centering 
    \includegraphics[width=0.7\textwidth]{fig/attackgraph} 
  \caption{Simple attack graph which shows possible ways how to achieve access to the database (fact node "\texttt{Access DB}"). Diamonds are the inner fact-nodes (initially false) that can be turned true, while rectangles are the leaf fact nodes, which are always true. Ellipses depict actions that attacker can perform with success probability $p$ and cost $c$.} 
  \label{fig:attackgraph} 
\end{figure} 
 
 
Example of such Attack Graph is in Fig.~\ref{fig:attackgraph}. Diamonds are the inner fact-nodes, that are initially false, but can be activated performing any action on which the facts depend upon. Rectangles represent leaf fact-nodes, that are initially true. Ellipses are the actions that attacker can perform with probability of success $p$ and cost $c$. In our example we represent action with its name and the couple $(p,c)$. Attacker's goal is to activate fact "\texttt{Access DB}" (obtain an access to DB).
 
The probabilities and costs of the actions can be obtained using Common Vulnerability Scoring System (CVSS)\footnote{www.first.org/cvss} from, i.e., National Vulnerability Database, which scores different properties of vulnerabilities. Probabilities could be computed for example from the access complexities, exploitabilities or availability impacts of the vulnerabilities, whereas costs could be computed from number of required authentication in order to a vulnerability, etc.

\subsection{Attack Policy}

Solving the AG means to find a policy, that describes what action should attacker perform in every possible evolution of the attack procedure. Fig.~\ref{fig:realstrategy} depicts optimal policy $\xi_{opt}$ for the problem from Fig.~\ref{fig:attackgraph}, where attacker first attempts to perform action "\texttt{Send MW Email}"; if action is successful, he follows the right (sub)policy (solid arc), thus performing action "\texttt{Remote Exploit}", otherwise the left (sub)policy (dashed arc), thus action "\texttt{Exploit Firewall}", and so on. 

\begin{figure}[h!] 
  \centering 
    \includegraphics[width=0.6\textwidth]{fig/realStrategy} 
  \caption{Optimal policy for a simple attack graph from Fig.~\ref{fig:attackgraph}. Attacker should follow solid arcs if previous action was successful, otherwise follow dashed line. Values at arcs represent the expected costs for attacker. } 
  \label{fig:realstrategy} 
\end{figure} 


\begin{comment}
\begin{definition}
A \emph{history} (or \emph{branch}) is a sequence actions with indicator whether an action was successful $a$ or not ($\bar{a}$). For instance, history $\mathit{<a_1,a_2,\bar{a_3},\bar{a_4}>}$ denotes an attack starting with a successful action $a_1$, followed by another successful action $a_2$, which is followed by two failed actions $a_3$ and $a_4$. 
\end{definition}
\end{comment}


\begin{definition}
An \emph{attack policy} is an oriented binary tree $\xi$ for evaluating attack graph AG. Nodes of $\xi$ are actions, arcs are labeled $+$ or solid line (if parent action was successful) and $-$ or dashed line (if parent action was unsuccessful), and whose leaf-nodes are either $\boxplus$ resp. $\boxminus$ representing successful reps. unsuccessful attack.
\end{definition}


\begin{definition} 
The \emph{expected cost} of an attack policy $\xi$ is a cost over all possible evolutions of the policy. Let $\phi_\chi$ be (sub)tree of $\xi$ rooted at a node $\chi$ labeled with an action $a$, then expected cost of $\phi_\chi$ can be computed recursively as follows:
$$ 
\mathcal{E}(\phi_{\chi}) = c_a + p_a \times \mathcal{E}(\phi_{\chi^+}) + p_{\bar{a}} \times \mathcal{E}(\phi_{\chi^-}) 
$$
where $\phi_{\chi^+}$ ($\phi_{\chi^-}$) is the subtree rooted at $\chi$'s $+$ branch ($-$ branch) and in the leaf-nodes of the policy is a penalty if attack is unsuccessful $\mathcal{E}(\boxminus)=penalty$ and reward if successful $\mathcal{E}(\boxplus)=reward$.
\end{definition} 
When we decide which of the two policies, either $\phi_\chi$ rooted at $\chi$ labeled with an action $a$ or $\phi_\psi$ rooted at $\psi$ labeled with action $b$ have lower expected cost, we assume that after performing action $a$, resp. $b$, attacker follows an optimal policy. In this case, we override our notation of $\mathcal{E}(\phi_{\chi})$ resp. $\mathcal{E}(\phi_{\psi})$ to simply $\mathcal{E}(a)$ resp. $\mathcal{E}(b)$.

We assume that our attacker is a \emph{motivated} attacker, that is they continue in attack as long as there are actions that may lead to the goal, regardless of the cost of the attacks. Motivated attacker ceases the attack only when there is no sequence of actions that could result in achieving the goal. Having assumed this type of attacker and the fact that attacks are \emph{monotonic}, meaning that consequence of attack preserves once is achieved~\cite{ammann2002scalable} (once the fact becomes true, it cannot become false again), it can be shown that every policy, regardless on the order of the action, will have equally the same probability of achieving the goal.  Note, that the expected cost of the policy consist of two parts: the probability of achieving the reward or the penalty and the expected cost of the action costs. The probability of successful attack is always the same, thus every policy will have the same expected cost of the penalty/reward. Thus, it essentially makes no difference whether we choose to reward the attacker for successful attack or penalize for an unsuccessful attack. In fact, distinct policies have different expected costs only because of the different action ordering which imposes different sequences of their costs. 

\begin{definition} 
A policy $\xi_{opt}$ is \emph{optimal} if it has the minimal expected cost among all possible policies, thus $\forall \xi \in \Xi: \mathcal{E}(\xi_{opt}) \le \mathcal{E}(\xi) $, where $\Xi$ is a set of all policies. 
\end{definition} 

\begin{comment}
\begin{proposition}
In the optimal policy $\xi_{opt}$ for every history $\mathrm{hist}$ and action $\mathrm{a}$, following property holds: $\mathcal{E}(\xi_{(\mathrm{hist},a)}) - c_a \ge \min\{\mathcal{E}(\xi_{(\mathrm{hist}\cup \bar{a},e_f(a))}),\mathcal{E}(\xi_{(\mathrm{hist}\cup \bar{a},e_f(a))}\} $
\end{proposition}
 PROVE IT

\begin{proposition}
In the optimal policy $\xi_{opt}$ for every history $\mathrm{hist}$ and action $\mathrm{a}$, following property holds: $\mathcal{E}(\xi_{(\mathrm{hist},a)}) \ge \mathcal{E}(\xi_{(\mathrm{hist}\cup a,e_s(a))} $
\end{proposition}
 PROVE IT

\begin{proposition}
In the optimal policy $\xi_{opt}$ for every history $\mathrm{hist}$ and action $\mathrm{a}$, following property holds: $\mathcal{E}(\xi_{(\mathrm{hist}\cup a,e_s(a))}) \le \mathcal{E}(\xi_{(\mathrm{hist}\cup \bar{a},e_f(a))})$. 
\end{proposition}
 PROVE IT
\end{comment}

\begin{proposition}
In the optimal policy $\xi_{opt}$ for every (sub)policy $\phi_\chi$ rooted at a node $\chi$ labeled with an action $a$ following is true: $\mathcal{E}{(\phi_{\chi})} \le \mathcal{E}{(\phi_{\chi^-})}$.
\end{proposition}
We will prove it by contradiction. Assume that $\mathcal{E}{(\phi_{\chi})} > \mathcal{E}{(\phi_{\chi^-})}$ is true. Then due to the monotonicity property the attacker could have followed the (sub)policy $\phi_{\chi^-}$ even before performing action $a$, which would have saved him the cost of the action $c_a$. But then this new policy would have had lower expected cost than the policy $\phi_\chi$, which violates our assumption that $\phi_\chi$ is an optimal policy.

\begin{proposition}
In the optimal policy $\xi_{opt}$ for every (sub)policy $\phi_\chi$ rooted at a node $\chi$ labeled with an action $a$ following is true: $\mathcal{E}{(\phi_{\chi^-})} \ge \mathcal{E}{(\phi_{\chi^+})} $.
\end{proposition}
\begin{align}
\mathcal{E}{(\phi_{\chi})} =& c_a + p_a*\mathcal{E}{(\phi_{\chi^+})} + p_{\bar{a}}*\mathcal{E}{(\phi_{\chi^-})} \\
\mathcal{E}{(\phi_{\chi^-})} \ge& c_a + p_a*\mathcal{E}{(\phi_{\chi^+})} + p_{\bar{a}}*\mathcal{E}{(\phi_{\chi^-})} \\
\mathcal{E}{(\phi_{\chi^-})} \ge& c_a + p_a*\mathcal{E}{(\phi_{\chi^+})} + c_a/p_a
\end{align}

\subsection{Markov Decision Process}
We solve this problem by modeling it as Markov Decision Processes (MDP)~\cite{bellman1956dynamic} which is defined as 4-tuple $\langle S, A, P_{\cdot}(\cdot,\cdot), R_{\cdot}(\cdot,\cdot) \rangle$, where:
\begin{itemize}
\item $S$ is a finite set states, in our case state is a set of performed actions and label whether the action $a$ was successful ($a$) or not ($\bar{a}$);
\item $A$ is a set of actions, which is equal to the set of actions in the attack graph
\item $P_a(s, s')$ is a probability that action $a$, performed in state $s$, will lead to state $s'$; in our case, if action $a$, with probability $p_a$ is successful, then state $s' = s\cup \{a\}$; if action $a$ is unsuccessful, then state $s' = s\cup\{\bar{a}\}$
\item $C_a(s, s')$ is an immediate cost payed after transition to state $s'$ from state $s$; in our case $C_a(s,s') = c_a$ in all transitions, except when $s'$ is a terminal state, then $C_a(s,s') = c_a - reward$ if goal is achieved in $s'$ and $C_a(s,s') = c_a + penalty$ if goal is not achieved in $s'$.
\end{itemize}

Optimal solution is such a policy of MDP, that minimizes an overall expected cost. 

\section{Algorithm}

\subsection{Basic Approach}
Basic approach is to use MDP with finite horizon, e.g. exhaust every possible action at every decision point and select action having minimal expected cost. In fact, we use this approach with several pruning techniques which speed up this computation. In Fig.~\ref{fig:decisionPoint} is an example of MDP search of our running example from Fig~\ref{fig:attackgraph}. The root of the MDP is a decision point where we need to decide which of the action among \texttt{"Exploit Firewall"}, \texttt{"Send MW Email"} and \texttt{"Create Dictionary"} is the best to perform. In a naive approach we explore every possible scenario and compute their expected costs $\mathcal{E}(\texttt{"Exploit Firewall"}), \mathcal{E}(\texttt{"SendMW Email"})$ and $\mathcal{E}(\texttt{"Create Dictionary"})$. We choose the action with the minimal expected cost.
 
 \begin{figure}[h!]
   \centering
     \includegraphics[width=0.7\textwidth]{dots/decisionPoint.pdf}
   \caption{In naive approach we explore every possibility and select action having minimal expected cost.}
   \label{fig:decisionPoint}
 \end{figure}
 
For performance  enhancement, we make use of \emph{transposition tables}, that is: we cache states for which we have computed expected cost and an optimal (sub)policy and reuse these results in future, should we encounter the same state again. 


\subsection{Sibling-Class Theorem}
In~\cite{greiner2006finding} authors deal with "probabilistic and-or tree resolution" (PAOTR) problem, mainly for and-or trees with independent tests without preconditions, for which they constructed and proved the Sibling-Class Theorem. Independently, authors in \cite{buldas2012upper} show the same theorem. The Sibling-Class Theorem states, that the leaf-node actions can be grouped into the sibling-classes within which actions' ordering can be determined by simply sorting their R-ratios; hence, no state-search exploration is necessary within sibling-class, only between the sibling classes. Two actions belong to the same sibling class if they have common parent in the and-or tree. As they consider inner nodes to be either AND or OR node, naturally there are two types of sibling classes: the AND-sibling classes and the OR-sibling classes. R-ratios of an action is computed as follows:
\begin{align}
R(a) &= \frac{p_{a}}{c_a} \textrm{ if action $a$ is in OR-sibling class} \\
R(a) &= \frac{p_{\bar{a}}}{c_a} \textrm{ if action $a$ is in AND-sibling class}
\end{align}

\begin{comment}
\begin{definition}
\emph{Parent-leaf} node is such an inner-node, who's at least one child is a leaf-node.
\end{definition}
\end{comment}


\begin{conjecture}
Sibling-Class Theorem for and-or trees without preconditions can be applied to an and-or graph with precondition using following rules for creating OR-Sibling Classes:
\begin{itemize}
\item actions $a$ and $b$ belong to the same OR-Sibling class \emph{iff}: $\pre(a)=\pre(b) \wedge |\pre(a)|=|\pre(b)|=1$.
\end{itemize}
and following rules for AND-Sibling Class:
\begin{itemize}
\item action a and b belong to the same AND-Sibling class \emph{iff}: $\pre(a)\neq \pre(b) \wedge |\pre(a)|=|\pre(b)|=1 \wedge (\exists c\in A:\pre(a)\in \eff(c) \wedge \pre(b)\in \eff(c))$.
\end{itemize}
Action a$ \in A$ cannot be pruned \emph{iff}: $|\pre(a)|>1 \vee (\exists c_1,c_2\in A: c_1 \neq c_2 \wedge pre(a)\in\eff(c_1) \wedge pre(a)\in\eff(c_2))$.
\end{conjecture}
The Sibling Theorem is proved only for and/or trees, while we empirically checked and use it for and/or graphs.
\begin{comment}
Since Sibling-Class Theorem does not consider any inner structure of an and-or tree but only the relationships between the parent-leafs and leafs, we can use it on and-or graph similarly. Thus, if parent-leafs and leafs have a tree-like structure (leaf has exactly one parent), we can use the theorem as it is. In a contrary case, we simply choose to compute expected cost of the action corresponding to the leaf. Should we concern that and-or graph has dependency actions in an inner AND-nodes with associated probabilities and costs, while and-or tree does not have any dependency actions (inner nodes represent pure logical conjunction or disjunction)? Not really, because as search progresses, eventually it will encounter an inner dependency action nodes and search will consider them at that moment by computing its expected cost.

Thus, when we consider which action to perform in a decision point, we can consider only those with the highest R-ratios in each Sibling-Class and those that have multiple parents or grand-parents.
\end{comment}

\begin{example}

Assume we have the same problem as in Fig.~\ref{fig:decisionPoint} and we come to the same decision point as previously. But now we computed $R(\texttt{"Exploit Firewall"}) = \frac{0.27}{5.0} = 0.054$, $R(\texttt{"Send MW Email"}) = \frac{0.23}{2.0} = 0.115$ and $R(\texttt{"Create Dictionary"}) = \frac{1.0}{11.0} = 0.091$ and we know that actions \texttt{"Exploit Firewall"} and \texttt{"Send MW Email"} have the same parent node \texttt{"Net Access"}, thus belong to the same OR-sibling class, while action \texttt{"Create Dictionary"} belongs to a separate sibling class. Now we explore only actions that have maximum R-ratios in each sibling class, thus only actions \texttt{"Send MW Email"} and \texttt{"Create Dictionary"}, and action \texttt{"Exploit Firewall"} surely will not be the first action in the optimal policy. Fig.~\ref{fig:decisionSibling} depicts nodes that must be explored (white nodes), and nodes that are pruned (grey nodes).
 \begin{figure}[h!]
   \centering
     \includegraphics[width=0.7\textwidth]{dots/decisionSibling.pdf}
   \caption{This figure presents nodes that are explored (white) and nodes that can be pruned (grey) in the MDP search if we know that actions \texttt{"Exploit Firewall"} and \texttt{"Send MW Email"} are in the same sibling class and action R-ratios.}
   \label{fig:decisionSibling}
 \end{figure}
 
\end{example}


\subsection{Branch and Bounds}
As another pruning technique we use branch and bounds. For this technique we reuse previously computed expected costs of the subtrees of the MDP to prune future subtrees if know that an optimal solution cannot exist there. Specifically, when we face the decision either utilize (sub)policy $\phi_a$ -- starting with an action $a$ -- or (sub)policy $\phi_b$ -- starting with action $b$ -- we choose policy $\phi_b$ only if $\mathcal{E}(\phi_b) < \mathcal{E}(\phi_a)$, implying:

\begin{align}
\mathcal{E}(\phi_{b}) &< \mathcal{E}(\phi_{a})\\
c_b + p_b * \mathcal{E}(\phi_{b^{+}}) + p_{\bar{b}} * \mathcal{E}(\phi_{b^-}) &< \mathcal{E}(\phi_a)\\
c_b + p_b * \mathcal{E}(\phi_{b^+}) + p_{\bar{b}} * \mathcal{E}(\phi_{b^+}) &< \mathcal{E}(\phi_a)\\
c_b + \mathcal{E}(\phi_{b^+}) &< \mathcal{E}(\phi_a)\\
\mathcal{E}(\phi_{b^+}) &< \mathcal{E}(\phi_a)- c_b
\end{align}
where from (8) to (9) we used property of the optimal policy that $\mathcal{E}(\phi_{b^+}) \le \mathcal{E}(\phi_{{b^-}})$, and then fact that $p_b + p_{\bar{b}} = 1$. Thus, $\mathcal{E}(\phi_a) - c_b$ is an upper-bounds for $\mathcal{E}(\phi_{b^+})$. If anytime during the computation it exceeds this bound, we can immediately stop the computation of the $b^+$ branch. 

Similarly, having computed the $\mathcal{E}(\phi_{b})$, we can bound again the branch $\mathcal{E}(\phi_{{b^-}})$ as follows

\begin{align}
\mathcal{E}(\phi_b) &< \mathcal{E}(\phi_a)\\
c_b + p_b * \mathcal{E}(\phi_{b^+}) + p_{\bar{b}} * \mathcal{E}(\phi_{b^-}) &< \mathcal{E}(\phi_a) \\
p_{\bar{b}} * \mathcal{E}(\phi_{{b^-}}) &< \mathcal{E}(\phi_a)- c_b - p_b * \mathcal{E}(\phi_b) \\
\mathcal{E}(\phi_{{b^-}}) &< (\mathcal{E}(\phi_a)- c_b - p_b * \mathcal{E}(\phi_{b}))/{p_{\bar{b}}}
\end{align}

\begin{example}
Assume different example, where we face the problem of choosing the best (sub)policy $\phi_a$, $\phi_b$, $\phi_c$ and $\phi_d$. Branches $\mathcal{E}(\phi_{a^+})$ and $\mathcal{E}(\phi_{a^-})$ we must compute to obtain $\mathcal{E}(\phi_a)$. Next, we compute expected cost $\mathcal{E}(\phi_{b^+})$ and assume that it turns out to be higher than $\mathcal{E}(\phi_a)$, hence, we can prune the computation of the branch $b^-$, as action $b$ will never have less expected cost then action $a$. Next, let's say $\mathcal{E}(\phi_{c^+})$ in the branch $c^+$ turned out to be lower than $\mathcal{E}(\phi_a)$. It means that we can upper bound the $\mathcal{E}(\phi_{c^-})$ by $\frac{\mathcal{E}(\phi_a)- c_c - p_c * \mathcal{E}(\phi_{c})}{p_{\bar{c}}}$. Let's say that $\mathcal{E}(\phi_{c^-})$ obeyed the bound, thus, action $c$ is the best action that attacker can perform so far. Note, that it is unnecessary to compute total expected cost of $\mathcal{E}(\phi_c)$ to determine that it is lower then $\mathcal{E}(\phi_a)$, it is direct implication from the fact that it satisfied both bound conditions. Finally, during the computation of branch $d^+$ it turned out to violate new upper bound $\mathcal{E}(\phi_c) - c_d$, thus its computation was terminated and branch $d^-$ was pruned as well.

 \begin{figure}[h!]
   \centering
     \includegraphics[width=0.7\textwidth]{dots/decisionBB.pdf}
   \caption{This figure presents nodes that are explored (white) and nodes that can be pruned (grey) due to the branch and bound technique.}
 \end{figure}
   \label{fig:decisionSibling}
\end{example}


\subsection{Heuristics}
Finally, we designed heuristic approach to compute the lower bound of the expected cost of an attack graph by setting costs of the actions to zero and taking into account only the actions' probabilities. This relaxation gives us freedom in action ordering as any (valid) ordering will produce exactly the same probability of success of the policy and thus the overall expected cost. We use this heuristics in two ways: (i) if computed heuristics exceeds the given upper bound, this branch of computation can be pruned, and (ii) according the heuristics we order the action in which we compute remaining actions' expected costs. If we start with the most promising action, more of the future branches might get pruned.

Nevertheless, we came across an issue in this approach due to the fact that our attack representation is not a tree but a directed cyclic graph. Which means that performing an action can be beneficial in several possible branches at once if action has more then one root-node paths in the attack graph, which results, that the probability of the action will be count multiple times into the overall probability of success is increased. This causes expected cost, computed as $(1-probability)*penalty$ to decrease. Since we minimize the expected cost this issue keeps the heuristics still admissible. 


\section{Experiments}

We experimentally compared our algorithm with two other approaches, namely Unconstrained Influence Diagrams, and using probabilistic planner from International Planning Competition.

\subsection{Guido approach}
This approach, described in~\cite{lisy2013computing}, converts an attack graph into an Unconstrained Influence Diagrams (UID) --- a graphical representation of a decision situations using probabilistic interference --- upon which existing solvers can be run. We ran a Guido solver as described in the article. 
This approach showed to be insufficiently scalable for the problems with large ($>$20 actions) attack graphs. 

\subsection{Probabilistic planning}
As an other approach, we decided to use a domain independent probabilistic planner SPUDD that competed in International Plannign Competition (IPC) in 2011. SPUDD is based on iterative value computation of MDP and uses own specification language. Since it computes MDP, it needs to have set either discount factor $\gamma = [0, 1)$, or  $\gamma = 1$ and the horizon set to an integer. For our purposes, discount factor $\gamma$ must be set to 1, hence horizon had to be chosen appropriately. To ensure that SPUDD finds an optimal solution, we chose to set the horizon to number of actions in the attack graph.

\subsection{Experiment settings}
We experimentally ran and compared our algorithm DynProg, Guido and SPUDD approaches on the three different realNetwork frameworks with different configurations. We ran experiments on Intel 3.5GHz with memory resource up to 10GB. In DynProg we set the $penalty = 10^9$ and $reward = 0$. In Tab.~\ref{tab:experiments} we present running times of each approach.
\begin{table}
\scalebox{0.8}{
\begin{tabular}{|r|c|c|c|}
  \hline
  Problem & DynProg [ms] &	Guido [ms] & SPUDD [ms] \\\hline \hline
  Local+2 & \textbf{51}& 85  & 1000 \\ \hline
  Local+3 & \textbf{155} 	&546   &  11000 \\ \hline
  Local+4 & \textbf{443} &76327 &  70000 \\ \hline
  Local+5 & \textbf{5389} &(OoM) &  656000 \\ \hline
  Local+6 & (OoM) &(OoM) &  \textbf{6152000} \\ \hline\hline
  Cross2 & \textbf{4} &408 	  &  1000 \\ \hline
  Cross3 & \textbf{38} &23796  &  9000 \\ \hline
  Cross4 & \textbf{504} &(OoM)  &  287000 \\ \hline
  Cross5 & \textbf{3587} &(OoM)  &  8373000 \\ \hline
  Cross6 & \textbf{60351} &(OoM)  &  (OoT) \\ \hline \hline
  LocalChain3-3 & \textbf{0} &9 &  \textbf{0} \\\hline
  LocalChain4-4 & \textbf{0} &70 &  1000 \\\hline
  LocalChain5-5 & \textbf{0} &1169 &  3000 \\\hline
  LocalChain6-6 & \textbf{0} &17133 &  23000 \\\hline
\end{tabular}
}
\caption{Time comparison of DynProg, Guido and SPUDD approaches over three types of problems with different complexities. Shortcuts: (OoM) - Out of Memory, (OoT) - Out of Time ($>10^7$ms).}
\label{tab:experiments}
\end{table}

\section{Conclusion and Future Works}
Our algorithm showed to outperform other two approaches in time complexity and scalability. Unfortunately, it ofter runs out of the memory due to the transposition tables and very large search state-space anyway. Other optimizations can be proposed, as better representation of a state or more accurate heuristics for better pruning. 
This algorithm can be used in game theoretic manner in couple of ways. Here we present two directions: (i) determine what honeypot configurations maximize the probability that an attacker would be detected during their attacks on the realNetwork and (ii) for security hardening determining which subset of vulnerabilities should administrator fix in order to secure the realNetwork, that is, that for the attacker it is not worth to attack to begin with.

\section*{Acknowledgement}\footnotesize\rmfamily\upshape%
This research was supported by the Office of Naval Research Global (grant no. N62909-13-1-N256) and Czech Ministry of Interior grant number VG20122014079.

\bibliography{27_durk}
\bibliographystyle{plain}

\end{document}
