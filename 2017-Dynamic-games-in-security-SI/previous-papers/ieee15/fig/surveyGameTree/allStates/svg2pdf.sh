#!/bin/bash

for i in `ls *.svg`
do
	inkscape --without-gui --export-pdf=${i}.pdf $i
done
