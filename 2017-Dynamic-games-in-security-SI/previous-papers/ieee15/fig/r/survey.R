
datafile <- "survey-histogram"
data <- read.csv(datafile, strip.white=TRUE)

# fill NA

for ( d in unique(data$defense) ) {
  for ( a in unique(data$attack) ) {
    for ( p in as.numeric(unique(data$payoff) )) {
      if ( dim(data[data$defense==d & data$attack==a & data$payoff==p,])[1] == 0 ) {
        #dim(data[data$defense=="UNI" & data$attack=="MR" & data$payoff==50,])[1]
        data <- rbind(data, c(d, a, p, NaN))
      }
    }
  }
}
data$payoff <- as.numeric(data$payoff)
data$freq <- as.numeric(data$freq)


#data <- data[order(data$payoff),]

ggplot(data, aes(x=factor(payoff), y=freq)) + 
  geom_bar(aes(fill = defense, shape= defense), position = "dodge", stat = "identity") + 
  scale_y_continuous(breaks = seq(0, 1, by = 0.1)) +
  facet_grid(. ~ attack) +
  xlab("Payoff") + 
  ylab("Probability") +
  theme_light()






data2 <- data.frame(data.frame(var1=character(), var2=character(), var3=numeric(), var4=numeric(), stringsAsFactors = F))
for ( d in unique(data$defense) ) {
  for ( a in unique(data$attack) ) {
    for ( p in seq(-100, 800, by = 905/5) ) {
      s <- sum(as.numeric(data[data$defense==d & data$attack==a & data$payoff>=p & data$payoff < p+905/5,]$freq))
      print(c(d, a, as.numeric(p), as.numeric(s)))
      #data2 <- rbind(data2, c(d, a, as.numeric(p), as.numeric(s)))
    }
  }
}

datafile <- "survey-hist2"
data <- read.csv(datafile, strip.white=TRUE)

# fill NA

for ( d in unique(data$defense) ) {
  for ( a in unique(data$attack) ) {
    for ( p in as.numeric(unique(data$payoff) )) {
      if ( dim(data[data$defense==d & data$attack==a & data$payoff==p,])[1] == 0 ) {
        #dim(data[data$defense=="UNI" & data$attack=="MR" & data$payoff==50,])[1]
        data <- rbind(data, c(d, a, p, NaN))
      }
    }
  }
}
data$payoff <- as.numeric(data$payoff)
data$freq <- as.numeric(data$freq)

s <- seq(-100, 800, by = 900/5)
#data <- data[order(data$payoff),]
data <- data[data$defense!="R-d",]
positions <- c("OPT-d", "MR-d", "UNI-d", "BR-d")
data$defense <- factor(data$defense, levels = positions)


plot <- ggplot(data, aes(x=factor(-payoff), y=freq, fill = defense )) + 
  geom_bar(aes(order = p), position = "dodge", stat = "identity") + 
  scale_y_continuous(breaks = seq(0, 1, by = 0.1)) +
  facet_grid(. ~ attack) +
  xlab("Defender's utility") + 
  ylab("Probability") +
  theme_light() +
  scale_fill_grey(start = 0, end = .8) + 
  scale_x_discrete(labels=c(expression("1.[-800,-620]"),"2.[-620, -440]", "3.[-440,-260]", "4.[-260,-80]", "5.[-80,100]"))
plot
printToPdf(plot, "surveyHist2.pdf", 12, 2.5)
