
\section{Introduction}

Computer network security is an example of asymmetric, strategic conflict between defenders and attackers.
Attackers perform a wide range of intrusion actions such as scanning the network and exploiting vulnerabilities, while defenders counter with actions such as intrusion detection and filtering.
Different defense mechanisms have varying costs and effectiveness against specific types of attacks.
Network administrators face challenging decisions in how to assess the effectiveness of security measures, and how to optimize the way these measures are selected and deployed.  
Defenses that appear to be effective in the short term may become less effective as strategic attackers learn and adapt to the latest security policies.  

Game theory is a mathematical framework with models and algorithms that can be used to study the problem of optimizing network security defenses against strategic attackers.   
This framework allows us to capture the complex interactions between intelligent decision makers with opposing interests in trying to defend or attack a computer network.  
Game theory also provides methods for reasoning systematically about uncertainty, strategic randomization, and information manipulation.  
Computing solutions to specific game models allows us to find optimal defense strategies to mitigate attacks, and can also provide a quantitative measure of security improvement.  
We demonstrate this methodology for cases where a network security administrator uses \emph{honeypots} (fake hosts or services added to a computer network) to harden a network against possible attacks~\citep{qassrawi2010deception}. 
Legitimate users do not interact with honeypots, so the honeypots act as decoys that distract the attackers from real hosts and waste their resources.
Honeypots can also send intrusion detection alerts to the administrator, and/or gather detailed information about the attacker's activity~\citep{provos2004virtual,grimes2005honeypots}.
Believable honeypots, are costly to set up and maintain, and a well-informed attacker may anticipate the use of honeypots and try to avoid them.
To capture the strategic decision of both attackers and defenders, we model the problem of deciding which services or host to deploy as honeypots using a dynamic game-theoretic framework.

One of the main challenges in applying this methodology to real computer networks is to represent the very complex space of possible attack strategies efficiently.
We make use of a compact representation of strategies for attacking computer networks called \emph{logic-based attack graphs}.
They can be automatically generated based on known vulnerability databases \citep{ingols2006practical,ou2006scalable} and they are used in network security to identify the minimal subset of vulnerabilities/sensors to be fixed/placed to prevent all known attacks~\citep{sheyner2002,noel2008optimal}, or to calculate security risk measures (e.g., the probability of a successful attack)~\citep{noel2010measuring,homer2013aggregating}.
We use attack graphs to represent an attack plan library, from which a rational attacker will choose an optimal contingency plan.  

In real attack scenarios attackers do not know everything about the target network, and must make decisions based on imperfect information.  
We model this uncertainty that attackers have by introducing attack graph games with imperfect information. 
Here, the main idea is that attackers have beliefs about what realistic networks look like, and use this to distinguish between hosts/services that are likely to be real and those that are likely to be honeypots.
However, generalizing our models in this way dramatically increases the computational challenge of solving the models, since attackers may believe that a vast number of networks are possible. 

We present methods for computing Stackelberg equilibria for attack graph games with imperfect information, including both exact and approximation methods and a variety of heuristic methods that allow for much greater scalability with some tradeoff in solution quality. 
Stackelberg equilibrium assumes that attackers can observe the defender's strategy before choosing an attack plan, which functions as a worst-case assumption that an attacker has perfect surveillance or insight into the defense strategy. 
In strongly adversarial games (i.e., zero-sum games), the Stackelberg equilibria are equivalent to Nash equilibria.
Stackelberg equilibrium is typically used in the security games \citep{tambe2011}, but computing these equilibria in games with stochastic events and the imperfect information is generally NP-hard~\citep{letchford2010computing} and algorithms that compute the optimal solution in this class of games typically do not scale to real-world settings~\citep{bosansky2015sequence}. 

We present four main contributions.
First, we present a game theoretic model that captures the interaction between a defender who hardens multiple networks by deploying honeypots, and an attacker who attacks them based on the attack graphs of the networks.
Second, we present algorithms for computing the attacker's optimal attack plans and develop a novel approach to reduce the game size to increase the scalability of the algorithms that compute the solution concept.
Third, we present a collection of algorithms that find both optimal and suboptimal defender strategies.
The suboptimal heuristic algorithms are based on the algorithms that optimally solve games with more restrictive assumptions, such as zero-sum utilities, the attacker having perfect information about the  network, or allowing for a correlation device.
We compare the quality of the heuristic algorithms to the approximation algorithm, and show that they find strategies with a small relative utility regret for the defender, outperforming all non-game-theoretic baseline approaches we consider.
Finally, we use our model to quantitatively investigate an impact of the honeypots on the network security and to answer interesting network security and honeypot related questions.
. %with different levels of uncertainty for the attacker.
%One of them uses a concept of closed under rational behavior, which significantly reduced the game and allows us finding optimal strategies for smaller problems.

The structure of the paper is as follows.
In Section~\ref{sec:rw}, we present the related work; in Section~\ref{sec:ag}, we introduce attack graphs, which are used to compute the attacker's attack policies.
In Section~\ref{sec:game}, we introduce our game-theoretic model and concepts of the heuristic approaches and in Section~\ref{sec:algs} we describe the algorithms we use to computing Strong Stackelberg equilibrium.
In Section~\ref{sec:experiments}, we analyze our algorithms, including the scalability and quality of the strategies that they found as well as network security related questions using our models.
The notation we use across the paper can be found in the Appendix of the paper.
%\kdnote{Partially published work should be in intro or related work?}
