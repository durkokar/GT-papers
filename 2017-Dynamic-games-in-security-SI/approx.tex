\subsection{Heuristic Approaches}\label{sec:approx}

Computing Stackelberg equilibria of imperfect information games with stochastic events is in general case NP-hard, as shown in \citep{letchford2010computing}.
The state-of-the-art algorithm for solving this class of games, presented in \citep{bosansky2015sequence}, uses mixed-integer linear programming (MILP) and the sequence-form representation of strategies.
Our case of attack graph games is also hard because there is exponential number of attack policies for the attacker to consider with the natural parameters that characterize the size of a network $T$.
Additionally, the set of actions for the defender grows combinatorially with increasing parameter $k$, which further limits the scalability of algorithms. 
%To compute the optimal strategy using MILP requires representing \emph{all} attack strategies in CURBs, that the attacker can perform in every information set.
Therefore, we focus here on a collection of \emph{heuristic} algorithms that find strategies close to SSE in polynomial time w.r.t. the size of the game tree. % or do not require us to enumerating attack policies in advance.
These algorithms compute the optimal solution profiles of different solution concepts or pose more restrictive assumptions on the game.
We present the general idea of several heuristic methods first, and then discuss the specific details of the algorithms.

\subsubsection{Correlated Stackelberg Equilibrium Heuristic (CSE)}\label{sec:slp}
The main motivation for this heuristic is the concept of correlated equilibria and an extension of the Stackelberg equilibrium in which the leader commits to a correlated strategy.
In \citep{conitzer2011commitment} this concept has been used in normal-form games, in \citep{cermak2016using} for extensive-form games and in \citep{letchford2010computing} for stochastic games. %, or security games \citep{xu2015}.
In this case, the leader is not only committing to a mixed strategy but also signals to the follower an action that the follower should take such that the follower has no incentive to deviate.
By allowing a richer set of strategies, the leader can gain at least the same utility as in the standard Stackelberg solution concept. 
However, the defender's commitment to signal the attacker the action to play impedes this solution concept from being applied in practice, since the defender may not be able to communicate with the attackers.

Unfortunately, computing commitments to correlated strategies is again an NP-hard problem in general extensive-form games with imperfect information and chance nodes (follows from Theorem 1.3 in \citep{von2008extensive}).
Moreover, in \citep{letchford2010computing} it was shown, that the improvement of the expected utility value for the leader can be arbitrarily large if commitments to correlated strategies are allowed.
On the other hand, we can exploit these ideas and use the linear program for computing the Stackelberg equilibrium \citep{conitzer2011commitment,durkota2015approximate}, and modify it for the specific structure of our games.
This results in a linear program that computes the strategy for the leader which could be close to the strategy in SSE.
We describe this algorithm in Section~\ref{sec:slp}.

\subsubsection{Perfect Information Game Heuristic (PI)}\label{sec:pi}
A straightforward game relaxation is to remove the attacker's uncertainty about the actions of nature and the defender, which results in a perfect information game.
Although the authors in \citep{letchford2010computing} showed that in general, the PI game with chance nodes is still NP-hard to solve, the structure of our game allows us to find a solution in polynomial time.
Nature acts only once and only at the beginning of a game. % (the attacker chooses an attack policy with corresponding expected reward).
After nature's move the game is a PI game without chance nodes, which can be solved in polynomial time w.r.t. the size of the game, as shown in \citep{letchford2010computing}.

In our game it corresponds to the defender securing each network separately, without exploiting the attacker's imperfect information.
In \citep{durkota2015optimal} it was shown that when the defender protects a network using honeypots he usually (i) either duplicates the targeted host, if few honeypots are available or (ii) if more honeypots are available, then duplicates the host types at the "choking points" of a network (hosts, through which the attacker must always pass to compromise the network, e.g., all entry nodes into the network).
This approach is likely to find these suboptimal strategies.


%\begin{figure}[t!]
%	\centering
%	\resizebox{7cm}{!}{
%		\usebox{\PIbad}
%	}
%	\caption{A game where PI finds very bad strategy.}
%	\label{fig:comparison}
%\end{figure}

\subsubsection{Zero-Sum Game Heuristic (ZS)}\label{sec:zerosum}
In \citep{korzhyk2011stackelberg} the authors showed that under certain conditions approximating the general sum (GS) game as a zero-sum (ZS) game can provide an optimal strategy for the GS game.
In this section we use a similar idea for constructing ZS game heuristic, for which we compute a NE that coincides with SSE in ZS games.
A Nash equilibrium can be found in polynomial time in the size of the game tree using the LP from \citep{koller1996efficient}.
%Additionally, in~\citep{korzhyk2011stackelberg} it was shown that for ZS games the SSE is equivalent to NE, which can be found in polynomial time to the size of the game tree.

Recall that in our game the defender's utility is $u_d(\sigma_d,\sigma_a)=R_d(\sigma_d,\sigma_a)-C_{d}(\sigma_d)$ and the attacker's utility is $u_a(l)=R_a(\sigma_d,\sigma_a)-C_{a}(\sigma_d,\sigma_a)$.
Our game has a payoff structure where $R_d(\sigma_d,\sigma_a)=-R_a(\sigma_d,\sigma_a)$ is the zero-sum component in the utilities and the smaller difference $|C_{d}(\sigma_d)-C_{a}(\sigma_d,\sigma_a)|$ is the closer our game is to a zero-sum game.
We propose several variants of ZS game: %, where one or both players utilities' are modified to satisfy the zero-sum condition $u_d(l)=-u_a(l)$. 
(ZS1) both players consider only the expected rewards from the attack policy  $u_d(\sigma_d,\sigma_a) = R_d(\sigma_d,\sigma_a)$; 
(ZS2) only the attacker's utilities are considered  $u_d(\sigma_d,\sigma_a) = R_d(\sigma_d,\sigma_a)+C_{a}(\sigma_d,\sigma_a)$;
(ZS3) only the defender's utilities are considered  $u_d(\sigma_d,\sigma_a) = R_d(\sigma_d,\sigma_a)-C_{d}(\sigma_d)$; and
(ZS4) the defender keeps his original utility with adversarial motivation to harm the attacker  $u_d(\sigma_d,\sigma_a) = R_d(\sigma_d,\sigma_a)-C_{d}(\sigma_d)+C_{a}(\sigma_d,\sigma_a)$.

Let us examine the consequences of modifying the player utilities to be zero sum.
For example, in ZS4 the defender is willing to put his network at higher risk if it results in more expensive attack policies for the attacker.
However, in reality the defender does not care about the attacker's individual costs.
The larger the individual costs $C_a$ and $C_d$, the further the original game is from the zero sum game, therefore, the worse strategies the zero sum heuristic will find, as was shown in \citep{durkota2015approximate}.
These approaches could perform well in games with relatively small player costs compared to their rewards.


%We also avoid generating the exponential number of attack policies by using a single oracle algorithm (Sec.~\ref{sec:so}).
%This algorithm has two subroutines: (i) computing a SSE of a ZS game and (ii) finding the attacker's best response strategy to the defender's strategy.
%The attacker's best response strategy we find by translating the problem into the \emph{Partially Observable Markov Decision Process} (POMDP), explained in Sec.~\ref{sec:OAPasPOMDP}.


%\subsubsection{Single Oracle Algorithm}
%Single oracle algorithm is an adaptation of the double oracle algorithm introduced in~\citep{bosansky2015sequence}.
%It is often used when one player's action space is very large (in our case the attacker's).
%Single oracle overcomes the difficulty by introducing a \emph{restricted game}, which contains only a subset of the player actions.
%It iteratively extends the restricted game until it contains an equilibrium.
%
%For ZS EFG games it was shown in~\citep{bosansky2014exact} that this approach can be used to find the NE, which is equivalent to the SSE in ZS games~\citep{korzhyk2011stackelberg}.

%For nZS games the algorithm is well defined, but no guarantee on convergence to an equilibrium holds; however, it may find a suboptimal solution very fast.
%Thus, we use SO algorithm to solve ZS approximations (referred as to ZS1-4) as well as the original nZS game, to which we refer as to SO.


