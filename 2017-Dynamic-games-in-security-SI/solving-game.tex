\subsection{Solving Games} \label{sec:alg-so}

\subsubsection{Optimal and Approximation Algorithm (MILP)} \label{sec:alg-so}

We compute Strong Stackelberg equilibrium using a variant of the MILP from \citep{bosansky2015sequence} with notations for our specific game:

	\begin{subequations}
		\begin{align}
			{\textrm max} &\sum_{z\in L} p_zu_d(n_z,y_z,\xi_z) \\
			{\textrm s.t.}: & (\forall I\in \topinfo,\xi\in\kappa(I)): \nonumber \\
			&\hspace{1cm} v_a(I)=s_{\xi,I} + \sum_{z\in I}\sigma_d(y_z,n_z)u_a(n_z,y_z,\xi)\delta_{n_z} \label{milp:br} \\
			& (\forall n\in \mathcal{N}): \sum_{y\in Y}\sigma_d(n,y) = 1 \\
			& (\forall I\in \topinfo): \sum_{\xi\in \kappa(I)}\sigma_a(\xi,I) = 1 \label{milp:attSumOne} \\
			& (\forall I\in\topinfo,\xi\in\kappa(I)): 0 \leq s_{\xi,I} \leq (1 - \sigma_a(\xi,I))M \label{milp:slack} \\
			& (\forall z\in L): 0\leq p_z \leq \sigma_d(y_z,n_z)\delta_{n_z} \label{milp:defFlow} \\
			& (\forall z\in L): 0\leq p_z \leq \sigma_a(\xi_z,I_z)\delta_{n_z} \label{milp:attFlow} \\
			& \sum_{z\in L}p_z = 1 \\
			%& (\forall I\in\topinfo, \xi\in\kappa(I)): \sigma_a(\xi,I)\in\{0,1\} \\
			& (\forall z\in L): \sigma_a(\xi_z,I_z)\in\{0,1\} \label{milp:attPure} \\
			& (\forall z\in L): 0\leq\sigma_d(y_z,n_z)\leq 1 
		\end{align}
	\end{subequations}
where $I_z$ refers to information set $I\in\topinfo$, such that state $z\in I$.
	The variables in MILP are: $p_z$ for each $z\in L$, $\sigma_d(n,y)$, $\sigma_a(\xi,I)$, $v_a(I)$ for each $I\in\topinfo$ and slack variables $s_{\xi,I}$ for each $I\in\topinfo, \xi\in\kappa(I)$.
It finds a probability distribution $p_z$ over terminal states $L$, such that the defender's expected utility is maximized (objective) and the attacker plays a pure best response.
Constraints~(\ref{milp:br}) ensure that the attacker's expected value $v_a(I)$ in each information set $I\in\topinfo$ equals to the value of the best response in $I$ against the defender's strategy $\sigma_d$.
It is enforced by the slack variables $s_{\xi,I}$, since they must be positive (\ref{milp:slack}) and at least one must be zero, which is a consequence of constraints~(\ref{milp:attSumOne}),(\ref{milp:attPure}) and (\ref{milp:slack}).
Constraints~(\ref{milp:attPure}) ensures that the probability for reaching state $z$ using strategies $\sigma_d$ and $\sigma_a$ is the same as the probability $p_z$.
For more details about MILP we refer the reader to~\citep{bosansky2015sequence}.


For every attacker's policy $\xi$, the MILP formulation requires creating one float variable (slack $s_{\xi,I}$) and one binary variable ($\sigma_a(\xi,I)$).
Since there are exponentially many attack policies for the attack graph, the MILP formulation would result in many binary variables that make solving MILP computationally very challenging.
Restricting the attacker's policies to CURB sets allows solving games for smaller or medium sized network topologies.
To the MILP where approximation CURB $\kappa^*(I)$ is used for computing attacker's policies in the information sets we refer as approximated MILP.
In the remainder of this section, we present a collection of \emph{heuristic} algorithms that find strategies close to SSE in polynomial time w.r.t. the size of the game tree.
%We present the general idea of several approximation methods first, and discuss the specific details of algorithms in the Section~\ref{sec:algs}. 

%\begin{figure}[t!]
%	\centering
%	\resizebox{7cm}{!}{
%		\usebox{\PIbad}
%	}
%	\caption{A game where PI finds very bad strategy.}
%	\label{fig:comparison}
%\end{figure}





\subsubsection{Correlated Stackelberg Equilibrium (CSE)}\label{sec:alg-slp}

%Our last algorithm, Single Linear Program (SLP), computes SEFCE which we use twofold: (i) it provides another strategy for the defender, which we compare with the remaining of the approximations, and (ii) the value of SEFCE returns an upper bound on the SSE value.
%Since computing exact defender's utility in SSE is computationally intractable, we compare quality of the strategies to SEFCE utility.
%To compute SEFCE, we need to generate all relevant attack policies in each of the attacker's information sets.
%For that, we use algorithm described in Section~\ref{sec:CURBasPOMDP}.


In~\citep{conitzer2011commitment} the authors present an LP that computes SSE of a matrix (or normal-form) game in polynomial time.
The LP finds the probability distribution over the outcomes in the matrix with maximal utility for the defender under the condition that the attacker plays a best response.
Here we represent our game as a collection of matrix games, one per attacker's information set, and formulate it as a single LP, as presented in~\citep{durkota2015approximate}. 
Formally, for each attacker's information set $I\in \topinfo$ we construct a matrix game where the defender chooses action $y\in Y$ that leads to a state in the information set $I$ and the attacker chooses an attack policy $\xi\in \kappa(I)$ (or $\kappa^*(I)$) from the set of all attack policies $\kappa(I)$ (or $\kappa^*(I)$) for information set $I$.
The  outcomes in the matrix game coincide with the outcomes in the original game.
Additionally, we restrict the sum of probabilities of the outcomes that originate from nature's action $n$ to be equal to $\delta_n$.
The LP formulation follows:
%{\footnotesize
	\begin{subequations}
	\begin{align}
		\textrm{max} &\sum_{z\in L} p_zu_d(n_z,y_z,\xi_z) \\
		\textrm{s.t.}: & (\forall I\in \topinfo,\xi_1,\xi_2\in \kappa(I)): \\
		&\hspace{1cm} \sum_{z\in I}p_{z'=(n_z,y_z,\xi_1)}u_a(n_z,y_z,\xi_1) \geq \sum_{z\in I}p_{z'=(n_z,y_z,\xi_1)}u_a(n_z,y_z,\xi_2) \label{slp:ineq}\\
		& \sum_{z\in L} p_z=1 \\
		& (\forall z\in L): p_z\geq 0 \\
		& (\forall n\in N): \delta_n = \sum_{y\in Y} \sum_{\xi\in \kappa(n,y)} p_{z=(n,y,\xi)}
	\end{align}
\end{subequations}
where the only variables are $p_z$.
It finds probability distribution over terminal states that maximizes the defender's expected utility (objective).
Constraint~(\ref{slp:ineq}) ensures that the attacker plays policy $\xi_1$, which yields higher expected utility than any other alternative $\xi_2$, therefore, plays best response.
Because there is no constraint that forces the attacker to play pure strategy, the attacker can correlated his strategy with the correlation device that chooses outcome of a game $z\in L$ with probability $p_z$.


%Furthermore, we incorporate the nature action probabilities by adding constraint one for each nature action $x_a$, that probability $\delta_a$ equals to the sum of probabilities of outcomes that can be reached via nature's action $x_a$.
An example follows to demonstrate our approach on the game in Figure~\ref{fig:EFGtoLP}.
Information sets $I_1$ and $I_2$ in the game in Figure~\ref{fig:SLPgame} correspond to the two matrix games in Figure~\ref{fig:EFGtoLP}.
The defender chooses probability distribution over states in $I_1$ with action $y_1$ and $y_3$ (similarly in $I_2$ with actions $y_2$ and $y_4$).
The probabilities of the terminal states of the game tree correspond to the probabilities of the outcomes in the matrix games ($p_1$ through $p_8$).
Additionally, the probabilities of outcomes $p_1,p_2,p_3,p_4$ must sum to $\delta_1$, since they root from the same action by nature $n_1$ (similar case for $\delta_2$).

This LP has weaker restrictions on the solution compared to the MILP formulation for SSE~\citep{bosansky2015sequence} since it does not restrict the attacker to play a pure best response strategy.
The objective is to maximize the defender's utility, as in the MILP.
Therefore, it does not exclude any SSE of the game.
However, formulating game with a CSE has similar drawback as MILP, namely, that it requires finding CURB $\kappa(I)$ for each attacker's information set in advance.

%Although this LP allows the attacker to play mixed best response, the authors proved that in the optimal solution of the LP the attacker will play pure best response.

\begin{figure}[t!]
	\centering
	\subfloat[]{\label{fig:SLPgame}\resizebox{8cm}{!}{ \usebox{\SLP} }}
	\subfloat[]{\label{fig:EFGtoLP}\includegraphics[width=.3\textwidth]{fig/game/SLPmatrix}}
	\caption{The extensive-form game in (a) translated into two normal-form games in (b)}
	\label{fig:comparison}
\end{figure}
%We exploit our extensive-form game's structure and write our game the as a collection of normal-form games and formulate them as a single LP.
%We demonstrate our approach on the example in Figure~\ref{fig:EFGtoLP}.
%Let $I^\at\subseteq\mathcal{I}$ be the set of all attacker's information sets where he chooses his first action (the attack policy $s$).
%For each information set $I_j\in I^\at$ we construct a normal-form game where the defender chooses an action $y_a$ that leads to information set $I_j$ and the attacker chooses an attack policy $s_b\in S_{I_j}$ from all attack policies information set $I_j$.
%E.g., in Figure~\ref{fig:EFGtoLP} in information set $I_1$ the defender chooses between actions $y_1$ and $y_3$ and in $I_2$ between actions $y_2$ and $y_4$.
%Each terminal node in the EFG corresponds to an outcome in one of NFG ($p_1$ through $p_8$ in our example).
%Furthermore, we incorporate the nature action probabilities by adding constraint one for each nature action $x_a$, that probability $\delta_a$ equals to the sum of probabilities of outcomes that can be reached via nature's action $x_a$.
%In our example, the probabilities of outcomes $p_1,p_2$ (defender's action $y1$) and $p_3,p_4$ (defender's action $y2$) must sum to $1/2$, since they root from the same nature's action $x_1$ played with probability $1/2$.
%
%The LP formulation follows:
%{\footnotesize
%	\begin{subequations}
%	\begin{align}
%		\textrm{max} &\sum_{x\in X} \sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s)u_d(x,y,s) \\
%		\textrm{s.t.}: & (\forall I\in I^\at,s_1,s_2\in S_{I}): \sum_{(x,y)\in I}p(x,y,s_1)u_a(x,y,s_1) \geq \sum_{(x,y)\in I}p(x,y,s_1)u_a(x,y,s_2) \\
%		%&\sum_{x\in X(I_a)}\sum_{y\in Y(I_a)}P(x,y,s_1)u_a(x,y,s_1) \geq \sum_{x\in X(z)}\sum_{y\in Y(z)}P(x,y,s_1)u_a(x,y,s_2) \\
%		& \sum_{x\in X}\sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s)=1 \\
%		& (\forall x\in X,y\in Y,s\in S_{(x,y)}): p(x,y,s)\geq 0 \\
%		& (\forall x\in X):  \sum_{y\in Y} \sum_{s\in S_{(x,y)}} p(x,y,s) = \delta_x
%	\end{align}
%\end{subequations}
%}
%where the only variables are $p(x,y,s)$, the probability of reaching terminal state of the game where nature plays $x$, the defender $y$ and the attacker $s$.

%Let us compare our LP formulation to the MILP formulation from~\citep{bosansky2015sequence} that computes SSE and explain why our LP finds an upper bound for the defender's expected utility of SSE.
%MILP consists of (i) constraints that the follower plays best response, (ii) constraints that the follower plays pure strategy and (iii) constraints for the probability flow conservation.
%The objective is to maximize the leader's utility.
%Our LP consists of constraint that the follower plays best response and the probability flow conservation, with the objective to maximize the defender's utility.
%Our LP has weaker restrictions on the solution than MILP, hence, it cannot excluded any SSE that is found by MILP.
%The value of the LP (referred to as SSEUB) is an upper bound for SSE which can be computed in polynomial time to the size of the game.
%We use SSEUB value to measure the quality of the approximation algorithm's strategies.

%By considering only the CURB set for the attacker, we do not exclude any SSE with the following rationale.
%Any attack policy that is in SSE is the set of attacker best responses, so it must be rationalizable and therefore it must be in the CURB set.
%From the LP result we extract the defender's strategy as a marginal probability for each defender's action: the probability that defender plays action $y\in Y$ in state  $n\in N$ is $\sum_{\xi\in \kappa{n,y}} p(n,y,\xi)$.
%An attack policy is rationalizable if and only if it is the attacker's best response to some belief about the networks in an information set. %position of honeypots.
%The set of all rationalizable attack policies is called \emph{Closed Under Rational Behaviour} (CURB) set~\citep{benisch2010algorithms}.
%In Section~\ref{sec:CURBasPOMDP} we explain how we compute the curb set.
%We will refer to this mixed strategy as $\sigma_\de^{SLP}$ and to the defender's utility in the strategy profile $u_d(\sigma_\de^{SLP}, BR_\at(\sigma_\de^{SLP}))$ as SLP.






\subsubsection{Solving Perfect Information Heuristic (PI)}\label{sec:alg-pi}
We solve the game using backward induction.
After the nature acts, for every defender action we compute the attacker's optimal attack policy using the algorithm described in Section~\ref{sec:MDP}, and choose a defender action that maximizes his expected utility.
These games can be solved in polynomial time w.r.t. the size of the game~\citep{letchford2010computing}.

\subsubsection{Solving Zero-Sum Heuristic (ZS)}\label{sec:alg-zs}

In zero-sum games, any Nash equilibrium yields the same utility to the leader (defender) as a Stackelberg equilibrium.
We use a single oracle (SO) algorithm which is a variant of double oracle from \cite{bosansky2014exact}, to find Nash equilibrium of our game, which avoids generating all of the attacker's attack policies.

Single oracle algorithm starts with a restricted game $G'$, that consists of a subset of attacker's actions (in our case with policy consisting only termination action $a_T$) and all the defender actions.
In iteration $m$ it does two steps:
(i) computes the attacker's best response $\hat{\pi}_\at^m=BR_\at(\sigma_\de^{m-1})$ to the defender's strategy $\sigma_\de^{m-1}$ in full game.
If all actions of the strategy $\hat{\pi}_\at^m$ are already contained in the restricted game $G'$, the algorithm terminates and returns NE of the restricted game.
Otherwise, (ii) extend the restricted game by including the best response action $\hat{\pi}_\at^m$, compute a new NE strategy profile $(\pi_\at^m,\sigma_\de^m)$ and go to step (i) with incremented iteration counter $m$.
The algorithm starts with a uniform strategy $\sigma_\de^0$ (the defender plays every action with a uniform probability), and a restricted game that contains all actions of nature and the defender and only termination actions for the attacker.
We use this algorithm to solve all four variants of the zero-sum (ZS) heuristics proposed in Section~\ref{sec:zerosum}.
The attacker's best response strategy we find by translating the problem into the \emph{Partially Observable Markov Decision Process} (POMDP), explained in Section~\ref{sec:POMDP}.

%We refer to the strategies found using the SO algorithm on the ZS game approximations as SOZS. 

%The SO algorithm is also well defined for general sum games and can be directly applied to the original game.
%However, it is guaranteed to converge only to NE, not to SSE.
%The reason is that the algorithm can converge prematurely without containing all the attacker's actions in the restricted game that are required to compute SSE of the original game.
%Consider an example in Figure~\ref{tab:so-sse}.
%The leader is row player and the follower is the column player.
%If the leader begins with uniform strategy, follower's best-response adds L to the restricted game and algorithm stops with strategy profile (U,L) with utilities (1,2).
%However, in SSE the leader plays U with probability 1/3 and follower plays R, with utilities (3, 2/3).
%Nevertheless, the algorithm may find a good strategy in a short time.
%We apply this algorithm to our GS game and use \emph{mixed integer linear program} (MILP), described in~\citep{bosansky2015sequence}, to compute the SSE of the restricted game in each iteration.
%Finding a solution for a MILP is an NP-complete problem, so this algorithm is not polynomial.
%We refer to this algorithm that applies SO to the GS game directly as SOGS.


%\begin{table}[b]
%  \centering
%  \caption{Example, where single oracle will not find SSE.}
%  \label{tab:so-sse}
%  \begin{tabular}{ll|l|l|l}
%    \cline{3-4}
%    &   & \multicolumn{2}{c|}{follower} &  \\ \cline{3-4}
%    &   & L             & R             &  \\ \cline{1-4}
%    \multicolumn{1}{|l|}{\multirow{2}{*}{\rotatebox[origin=c]{90}{leader}}} & \parbox[c][5mm]{3mm}{U} & 1,2           & 3,0           &  \\ \cline{2-4}
%    \multicolumn{1}{|l|}{}                        & \parbox[c][5mm]{3mm}{D} & 0,0           & 2,1           &  \\ \cline{1-4}
%  \end{tabular}
%\end{table}




%The SSE of the restricted game is computed using LP if game is zero-sum and using \emph{mixed integer linear program} (MILP)~\citep{bosansky2015sequence} if the game is nZS.
%The algorithm starts with uniform strategy $\sigma_d^0$ (the defender plays every action with a uniform probability), restricted game containing all natures and defender's action and no attacker actions and iteration counter $i=1$.

%\subsection{Optimal Attack Policy as MDP}\label{sec:OAPasMDP}

