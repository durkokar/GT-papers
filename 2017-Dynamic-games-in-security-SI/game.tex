\section{Attack Graph Deception Game}\label{sec:game}

Honeypots in network security are a form of a deception. 
We can model deception in games where one player has more information about the game state than the other (e.g., network structure, honeypot locations).
Predicting the player's behavior is more difficult in these games because it depends on their beliefs about the likelihood of possible states.
We model the honeypot deployment problem as an extensive-form game that captures the attacker’s limited information about the network.

%For example, he knows that in a bank network there will certainly be some office computers, a client database, and an internet banking backend, but he does not know how many offices there are or if the bank uses a VPN server.
In this work, we assume that the defender protects a set of networks (e.g, networks of different companies, or different subnetworks of a single company, etc.) and the attacker has a prior belief about the possible networks that he may encounter.
The game has three stages.

In the \emph{first stage} of the game, we model the attacker's beliefs about possible networks by \emph{Chance} (or nature) player who makes an initial random.
The probability of each move corresponds to the attacker's belief of the likelihood of each network, as shown in Figure~\ref{fig:gameTree}.
There are three networks that the nature chooses from, each with probability 1/3.
In the \emph{second stage}, the defender chooses the number and type of the honeypots to deploy into each network.
From the perspective of the attacker, two networks can look identical after the defender adds honeypots.
In the \emph{third stage} of the game, the attacker observes the network resulting from the choices of both nature and the defender, and attacks it optimally based on the attack graph for each observed network.
We explain each stage of the game in more detail along with a simple example in Figure~\ref{fig:gameTree}.

\begin{figure}[t!]
    \includegraphics[width=\textwidth]{fig/game/gameTree4.pdf}
	\caption{The game tree for a simple network with host types A, B, router and target (T). %, with their attack success probabilities 0.2 and 0.8, respectively.
        Chance plays uniformly into three possible networks; each contains two hosts of A or B.
	The defender chooses possible combinations of allocating two honeypots in each possible network; the defender's optimal strategy is depicted with probability values of choosing each action ($y_1, y_2$ or $y_3$).
        The attacker selects attack policy $\xi_1$ through $\xi_n$ according to the networks' attack graphs
    }
    \label{fig:gameTree}
\end{figure}


\subsection{Chance Actions}
We represent the attacker's beliefs about possible networks by a Chance player who makes an initial random move with probability corresponding to the attacker's belief of likelihood of each network.
The attacker is uncertain which of the possible networks after the Chance move is the real network \emph{before} the defender deploys honeypots. 
We assume that the attacker’s belief is common knowledge (if not, the defender can approximate it by analyzing the frequency of the networks in the real-world).
In our example, the attacker is uncertain about the number of hosts of types A and B, but knows that total number of hosts of A and B is two.

$\mathcal{N}$ denotes the set of networks the Chance chooses form, where each network $n\in\mathcal{N}$ consists of $n^t\in\mathbb{N}_0$ hosts of type $t\in T$.
Chance selects network $n$ with probability $\delta_n$.
In our example, we have four host types $T=(A,B,router,target)$ and Chance chooses from three networks in $\mathcal{N}=$\{(2,0,1,1), (1,1,1,1), (0,2,1,1)\} each with probability $\delta_n=\frac{1}{3}$.

\subsection{Defender's Actions}
The defender chooses at most $k$ honeypot and their types to deploy into each network $n\in\mathcal{N}$.
This additionally increases the computational complexity of the problem compared to previous work \citep{durkota2015approximate}, where the defender deployed exactly $k$ honeypot.
%Although the defender knows the actual network; the strategy specifies the honeypots to deploy to every possible network.
Formally, the set of all the defender's actions is $Y = \{y\in\mathbb{N}_0^T | \sum_{t\in T}y^t\leq k \}$, where $k$ is the number of available honeypots and $y^t$ the number of honeypot of type $t$.
Performing action $y\in Y$ on network $n\in \mathcal{N}$ results in a network with $n^t$ real hosts of type $t$ and $y^t$ honeypots of type $t$.
The attacker's action on host type $t$ interacts with a honeypot with probability $\frac{y^t}{n^t+y^t}$. 
In the example in Figure~\ref{fig:gameTree} the defender deploys exactly $k=1$ honeypot and the set of the defender's actions is $Y=\{(2,0,0,0),(1,1,0,0),(0,2,0,0)\}$.
Applying all actions in each network results in 9 different networks for the attacker.

Let $\sigma_d\in\Sigma_d$ denote the defender's mixed strategy and $\Sigma_d$ defenders set of all strategies. %is a probability distribution over all pure strategies.
With $\sigma_d(n,y)$ we denote the probability that the defender chooses action $y$ in network $n$.
%The defender's pure strategy $\pi_d$ prescribes an action $y\in Y$ for each network $n\in \mathbb{N}$.
%Let $\Pi_d$ be the set of all pure strategies.
%Let $Z=N\times Y$ be the set of all networks created after all defender's decision.
%We also define $c^h_t \in \mathbb{R}_+$ to be the cost that the defender pays for adding and maintaining a honeypot of type $t$.


\subsection{Attacker's Information and Actions}
After the defender moves, the attacker observes the network and chooses an attack actions for each network according to the network's attack graphs.
We assume that the attacker knows that the defender can deploy up to $k$ honeypots; therefore, he must reason about what the defender would do in each possible situation.
This creates an interesting information structure.
If the defender deploys two honeypots of type A in Network 1, which results in Network 1-1, then the attacker can be confident that two out of four hosts of host type A are honeypots.
However, if the defender deploys one honeypot to A and one to B in Network 1 and two honeypots to A in Network 2, the resulting Networks 1-2 and 2-1 look precisely the same to the attacker.
When the attacker observes this network, he cannot be sure if the host in B is a honeypot or not.

An \emph{information set} is a set of indistinguishable networks by the attacker. % and common attack path performed by the attacker.
In Figure~\ref{fig:gameTree} we depict information sets $I_1$ through $I_5$ for the attacker, where he makes his first decision about an attack action.
The attacker can execute the same set of actions in all states in an information set. % (otherwise he could tell apart the states based on the available actions).
Let $A(I)$ denote the attacker's atomic actions available in information set $I$ based on the networks in $I$ and the attacker's so-far executed attack path that led to $I$.
If the attacker successfully performs action $a\in A(I)$ in $I_1$ and he is not detected, he reaches another information set $I'$ (not depicted in Figure~\ref{fig:gameTree}), where he chooses next available attack action from the set $A(I')$, and so on.
Let $\allinfo$ be the set of all information sets for the attacker in the game, and $\topinfo\subseteq\allinfo$ be set of all information sets with empty attack paths.
Let $Z$ denote all states of a game, and $z=(n_z,y_z,path_z)\in Z$ denote a state, where nature chose $n_z\in \mathcal{N}$, the defender chose $y_z\in Y$ and the attacker attack path $path_z$.
$Z$ includes the game's inner states, where $path_z$, or both $y_z$ and $path_z$ are possibly empty.
Formally, two states $z=(n_z,y_z,path_z)$ and $w=(n_w,y_w,path_w)$ belong to the same information set $I\in\allinfo$ if and only if $\forall t\in T:n_z^t+y_z^t=n_w^t+y_w^t~\&~path_z=path_w$.
In words, if two networks after the defender's move result in the same number of hosts of each host type and the attack performed exactly the same attack path on both of them, than the attacker still cannot tell the two networks apart.
Networks in states $z,w\in I\in\allinfo$ have the same set of vulnerabilities, therefore, the same attack graph structure for the attacker.
The only difference is in probabilities of interacting with honeypots.
However, the attacker must choose the same attack action for all networks within an information set since they are indistinguishable.
But different information sets may have different attack actions to choose from (e.g., in Networks 1-1 only host-type A can be attacked, while in Network 3-3 only host-type B).
%The defender controls the attacker’s observations about the network to a large extent, leading to the potential for deception and a complex decision problem for the attacker.

\subsubsection{Attack Policy as Attacker's Action}
In our algorithms, we use an alternative representation of the attacker's actions, which is based on the contingency attack policies.
It allows us to consider only relevant policies for the attacker that are needed to solve the game, therefore, makes the algorithms more efficient.
Since contingency policy $\xi$ fully describes the attacker's behavior we use it to represent the attacker's single action in each information set $\topinfo$, after which the games ends.
%If the attacker chooses to play $\xi$ in $\topinfo$, it directly leads to a terminal state of the game.
Let $\Xi(I)$ denote all attack policies for the attacker in the information set $I\in\topinfo$ (recall, that the network in each state $z\in I$ have identical attack graph structure, therefore, the same set of available attack policies).
Each attack policy $\xi\in\Xi(I)$ results in a potentially different pair of utilities specified in the terminal states of the game tree.
We reuse our notation for the game state $z=(n_z,y_z,\xi_z)$ to denote the nature's, the defender's and the attacker's choices $n_z,y_z$ and $\xi_z$, respectively, that lead to state $z$.
Let $\sigma_a\in\Sigma_a$ be the attacker's mixed strategy, where $\Sigma_a$ is set of all attack strategies for the attacker.
$\sigma_a(\xi,I)$ denotes the probability of choosing policy $\xi$ in information set $I$.

%Let $\Xi(z)$ be the set of all valid attack policies for network $z$ based on its attack graph.
%Therefore, we can denote with $\Xi(I)$ the set of attack policies of any of the network in the information set $I$.
%The attacker pure strategy $\pi_a$ chooses an attack policy $\Xi(I)$ for each information set $I\in\mathcal{I}$.
%Let $\Pi_a$ be the attacker set of all pure strategies.
%We define attacker's mixed strategy $\sigma_a$ as a probability distribution over set $\Pi_a$, where $\Sigma_a$ is set of all attacker mixed strategies.
%Executing the attack policy leads to the terminal state of the game.
%In the example in Figure~\ref{fig:gameTree}, the attacker observes six different information sets, three singletons (contain only one network) and three information sets that contain two networks (denoted with dashed lines).



\subsection{Players' Utilities}
For each host type $t$, $r_t$ denotes its value to both players (e.g., value for possessing classified documents in that host).
The defender's cost for deploying honeypot of type $t$ is $c_d(t)=\gamma_c r_t$, which is proportional to $r_t$ by factor of $\gamma_c$.
Let $L\subseteq Z$ denote all terminal states of the game.
For each terminal state $z=(n_z,y_z,\xi_z)\in L$, the attacker's expected utility is $u_a(z)=R_a(z)-C_a(z)$ and the defender's expected utility is $u_d(z)=R_d(z)-C_d(z)$, where:
\begin{enumerate}
	\item $R_a(z)$ -- is the attacker's expected reward for executing policy $\xi_z$ on network $n_z$ with honeypots deployed according to $y_z$.
This component can be viewed as a zero-sum component (the more the attacker gains the more the defender loses), therefore, the defender's reward is $R_d(z)=-R_a(z)$.
\item $C_d(z)=\sum_{t\in T}c_d(t)y_z^t$ -- the defender's cost for deploying honeypots according to $y_z$.
\item $C_a(z)$ -- the attacker's cost for executing policies $\xi_z$ on network $n_z$ with honeypots deployed according to $y_z$.
\end{enumerate}
The values for $R_a(z)$ and $C_a(z)$ we compute equivalently to Equations~(\ref{eq:reward}) and (\ref{eq:cost}).
The only difference is in the case that action $a$ interacts with host type $t$ for the first time along its attack path as described in formulas.
Then with probability $\frac{y_z^t}{n_z^t+y_z^t}$ the attacker receives zero and the attack ends, and with probability $\frac{n_z^t}{n_z^t+y_z^t}$ the attack continues.
In Figure~\ref{fig:gameTree} the first value is the defender’s utility and the second value is the attacker’s utility.

\begin{definition}
We define \emph{solution profile} as a pair of strategies $(\sigma_d,\sigma_a)$, where the defender plays $\sigma_d$ and the attacker $\sigma_a$.
\end{definition}
In solution profile $(\sigma_d,\sigma_a)$ the defender's expected utility is $u_d(\sigma_d,\sigma_a)=R_d(\sigma_d,\sigma_a)-C_d(\sigma_d)$, where the first component is the defender's loss and the second component is the honeypot deployment cost, which depends only on the defender's strategy $\sigma_d$.
The attacker's expected utility is $u_a(\sigma_d,\sigma_a)=R_a(\sigma_d,\sigma_a)-C_a(\sigma_d,\sigma_a)$, where $R_a(\sigma_d,\sigma_a)$ is the expected reward and $C_a(\sigma_d,\sigma_a)$ is the expected policy execution cost, which depends on both, the defender's and the attacker's strategies.
For example, if the attacker interacts with a honeypot in the beginning of the attack, the attacker is detected does not pay for executing the remaining of the attack policy.
%Our game model has certain pay-off function restrictions noteworthy pointing out.
%Second, there is constraint $R>C$ that must be satisfied, otherwise the attacker would not be motivated to perform any attack.
%Third, there is constraint $R>H$ which motivates the defender to secure the network from attacking.
%These restrictions limits the players utilities $|u_a+u_d|\leq|(H-C)/2|$-distanced from the zero-sum game.

%Formally, the Honeypot Selection Game (HSG) is defined by the tuple $G = \langle \mathcal{N},T,n,k,b,\delta,\mathcal{I},S,\Pi,u \rangle$, where:
%    \begin{itemize}
%        \item $\mathcal{N}=\{d,a\}$ are the players in the game called the \emph{defender} and the \emph{attacker}. To ease definitions, we also refer to any of the players as $i$ and to his opponent as player $-i$.
%        \item $T$ is the number of host types in the network.
%        \item $n$ is the number of real hosts in the network.
%        \item $k$ is the number of honeypots.
%        \item $b\in \mathbb{N}^T_0$ is the network basis.
%    \item $X=\{x\in\mathbb{N}^T_0 | \sum_t x_t = n \land \forall t\in T: b_t\leq x_t\}$ is the set of networks that \emph{Nature} creates.
%        \item $\delta: X \rightarrow [0,1]$ is the Nature's action probability distribution.
%        \item $Y = \{(x,y)\in\mathbb{N}^T_0\times\mathbb{N}^T_0| x\in X \land \sum_t y_t=k \}$ is the set of defender's possible honeypot allocations actions.
%        \item $S_z$ is the set of attacker's attack policies for network $z\in\mathbb{N}_0^T$.
%        \item $u_i:X\times Y \times S \rightarrow \mathbb{R}$ denotes the $i$-th player utility.
%    \end{itemize}

\input{solution-concepts}
\input{approx}
