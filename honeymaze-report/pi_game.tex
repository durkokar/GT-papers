\subsection{Perfect Information Honeypot Selection Game}


\begin{figure}[h!]
	\center
	\usebox{\mainNet}
	\caption{The basic network.}\label{fig:network} 
\end{figure}

The basis of the game model is a specific computer network that includes different types of network resources, e.g., web server, a router with a firewall, and workstations.
The game is modelled as a two-player \emph{Stackelberg game}. 
In a Stackelberg game we assume that the first player (the \emph{defender}) commits to a publicly known pure strategy.
The second player (the \emph{attacker}) plays pure best response to the (observed) strategy of the defender.
The set of actions for the defender corresponds to deploying one or more honeypots in the network.
We assume there is a limited set of possible options and we restrict the defender to use only honeypots that are an exact copy of an existing host on the network in terms of configuration and connectivity (i.e., defender is duplicating a host already in the network).
This has several advantages. 
First, copying an existing machine and obfuscating the useful data on the machine can be done almost automatically with lower cost than configuring a brand new host. 
Second, it can create much more believable honeypots that will make it hard for the attacker to distinguish them from production systems. 
Third, if the attacker uses some unknown exploit on the honeypot, we know that the same exploit is available on one of the production machines.
We can check these machines to determine if they were compromised the same way, try to remove the vulnerability and/or update the attack graph to include it in further analysis.
Finally, we do not introduce any fundamentally new attack vectors to penetrate the network.
The honeypot can take the place of a production machine in the attack plan, which is a desired effect, but we do not open any new opportunities for the attacker. 
Hence, it is very unlikely that this could reduce the security of the network. 

The actions of the attacker are attack plans in the modified computer network; in the Stackelberg model the attacker responds optimally so we are interested only in the optimal attack plan. 
Since we restrict the possible changes by the defender, the attack graph created for the original network also represents the possible attacks on the modified network.
We assume that the attacker is able to discover the exact strategy played by the defender (i.e., how many honeypots were deployed and which hosts were duplicated). However, the attacker does not know which of the duplicated hosts is real and which ones are honeypots.
Therefore, the effect of adding honeypots to the network is modeled by the probabilities of interacting with the honeypots while using different resource types. 
If some action uses a host that has fake duplicates, this probability represents the chance that the attacker chooses a honeypot instead of a real machine. 
The formal game definition, the experiments and their results are described in detail in~\cite{durkota2015ijcai}.

\subsubsection{Optimization}
Let $H$ be the number of honeypots and $T$ the number of types in the network, then there are $C_{H}(T) = \frac{(T+H-1)!}{H!(T-1)!}$ possible honeypot configurations. 
To compute the defender's optimal honeypot configurations using up to $H$ honeypots, we compute the optimal attack plan for $\sum_{h=1}^{H}\left(\frac{(T+h-1)!}{h!(T-1)!}\right)$ possible configurations of the honeypots.
However, as mentioned above, the network with all the configurations produce alike attack graph, merely the action probabilities change.
Therefore, the solving of a game for a specific number of honeypots $h$ can be speed-up by re-using information and calculated values from the setting with $h-1$ honeypots.
Fig.~\ref{fig:cacheGame} shows the computation time for solving the game on the smallest network.
The results show that the impact of this simple improvement is quite dramatic (note the logarithmic y-scale) and re-using the precomputed values from games with fewer honeypots allow us to scale much better.
We therefore use the global cache and analyze the scalability of our approach on different networks (Fig.~\ref{fig:scalabilitygame}).

\begin{figure}[t]
        \centering
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth]{fig/plotMemMemlessComp}
                \caption{}
                \label{fig:cacheGame}
        \end{subfigure}
        \begin{subfigure}[b]{0.45\textwidth}
                \includegraphics[width=\textwidth]{fig/scalabilityGame}
                \caption{}
                \label{fig:scalabilitygame}
        \end{subfigure}%
       % \begin{subfigure}[b]{0.3\textwidth}
       %         \includegraphics[width=\textwidth]{fig/plotUtility}
       %         \caption{}
       %         \label{fig:utilityPer}
       % \end{subfigure}%
        \caption{\ref{fig:cacheGame} - time comparison of the computation with and without using the cache between the games; \ref{fig:scalabilitygame} - time comparison of varying number of honeypots and number of types}\label{fig:utility}
\end{figure}

\subsubsection{Attack Graph with Honeypots}
We study game-theoretic models where the network administrator (defender) chooses $y\in\mathbb{N}^T$, where $y_t$ is the number of deployed honeypot of host type $t\in T$.
The resulting network $z=(x,y)$ contains $z_t=x_t+y_t$ hosts of type $t$, where $x_t$ are real and $y_t$ are honeypots.
If attacker performs an attack action on a host type $t\in T$, we assume that specific host of that type $t$ is chosen with uniform probability.
In other words, attack action of type $t$ will interact with honeypot with probability $\frac{y_t}{z_t} = \frac{y_t}{x_t+y_t}$.
With probability $\frac{x_t}{z_t} = \frac{x_t}{x_t+y_t}$ the action will interact with the real host, and any future action on type $t$ will interact with the same host, therefore, never with honeypot.

The defender can either create honeypot as a duplicate of existing host type or create honeypot of a new host type.
The former does not change the attack graph structure, only adds probabilities that certain actions can interact with a honeypot.
In Fig.~\ref{fig:netWithDuplHp} we show a network where black are the real hosts and red are the honeypots.
In Fig.~\ref{fig:AGWithDuplHp} is the corresponding attack graph, where first value in the action tuple represents the probability that the action will interact with honeypot.
Note that duplicated host types are Database and Workstation, therefore, action ``Send MW Email'' has 0.25 probability of interacting with honeypot (three real hosts and one honeypot), and actions ``Remote Exploit'' and ``Pwd Brute Force'' have probability 0.6 of interacting with honeypot (there is one real host and two honeypots of Database host type).
While VPN is a honeypot of new host type, which introduces a new vulnerability into the network, which exploitation will be detected for sure.

The second option of honeypots, where the defender creates new host type introduces fake vulnerabilities of that host type into the attack graph.
E.g., VPN host types in Fig.~\ref{fig:netWithDuplHp} is only honeypot.
This host type extends the attack graph in Fig.~\ref{fig:AGWithDuplHp} with new attack action ``Pwd Broute Force'' on VPN which interacts with honeypot with probability 1.

\begin{figure}[t]
	\centering
		\begin{subfigure}[b]{0.3\textwidth}
			\includegraphics[width=\textwidth]{fig/planning/netWithHPNew}
			\caption{}
			\label{fig:netWithDuplHp}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.5\textwidth}
			\includegraphics[width=\textwidth]{fig/planning/attackGraphWithHPNew}
			\caption{}
			\label{fig:AGWithDuplHp}
		\end{subfigure}
		\caption{(a) Network with honeypots (red hosts). (b) Attack Graph for (a) with actions extended with probabilities of interacting with the honeypot (red values) and facts and actions for new honeypot host types VPN (red).}\label{fig:fig1}
\end{figure}


\subsubsection{Computing an Optimal Attack Graph with Honeypots}\label{sec:OAPwithHP}
In our game we assume, that when attacker performs an action on honeypot and is detected, the attacker's activity in the network is immediately ceased by the administrator (i.e. blocking his IP address).
This assumption modifies the computation accordingly of the attacker's expected utility in following way:
\begin{equation}
\mathbb{E}_{att}[\xi_\chi] = c_a + \bar{\tau_\chi}\left[p_a(\mathbb{E}[\xi_{\chi^+}]+I(\chi,\chi^+)) + \bar{p_a}\mathbb{E}[\xi_{\chi^-}]\right].
\end{equation}
where $\bar{\tau_\chi}$ is the probability that action $a$ does not touch any honeypot in state $\chi$.

\paragraph{Sibling-Class Theorem with Honeypots}
The global effect of the honeypots can violate the monotonicity property of actions in attack graph that is used to prove the Sibling Theorem presented previously.
The results can still be used in some cases, but applying the result is somewhat more complicated because we must take into account the presence of honeypots.

Consider the following counterexample: actions $x$ and $y$ have the same parent OR node and $(c_x, p_x,\tau_x) = (10, 1, \frac{1}{2})$ and $(c_y, p_y, \tau_y) = (1,\frac{1}{2},\frac{1}{2})$, and each action interacts with the different honeypot. 
Although $R_y > R_x$, the expected reward of strategy $\xi_{xy}$, depicted in Fig.~\ref{fig:counterexample}, is $\mathbb{E}[\xi_{xy}] = -10+\frac{1}{2}(1\cdot100+0(-1+\frac{1}{2}(\frac{1}{2}\cdot100+\frac{1}{2}\cdot0))) = 40$ is higher than $\mathbb{E}[\xi_{yx}] = -1+\frac{1}{2}(\frac{1}{2}\cdot100+\frac{1}{2}\cdot(-10 + \frac{1}{2}(100))) = 34$. 
%\end{remark}

Now assume the same example as previously, however both actions interact with the same honeypot.
Meaning, that if the attacker performs first action, which did not touch the honeypot, second action certainly will not operate on honeypot either.
In this case, the expected reward of the strategy $\mathbb{E}[\xi_{xy}] = -10+\frac{1}{2}(1\cdot100+0(-1+\frac{1}{2}(\frac{1}{2}\cdot100+\frac{1}{2}\cdot0))) = 40$, while the strategy $\mathbb{E}[\xi_{yx}] = -1+\frac{1}{2}(\frac{1}{2}\cdot100+\frac{1}{2}\cdot(-10 + 1\cdot100)) = 46.5$.
And what about the case when action $x$ touches two honeypots $a$ and $b$ and the action $y$ touches honeypots $a$ and $c$?
The honeypots complicate the usage of the Sibling-Class Theorem, thus we used it only in the case, when neither of the actions interact with the honeypots.

\begin{figure}[h!]
  \centering
\includegraphics[width=0.3\textwidth]{fig/stCounterExample}
\caption{Counterexample for arbitrary honeypot probabilities.}\label{fig:counterexample}
\end{figure}

\subsubsection{Touching the honeypot}
In our work, we assumed that the defender immediately blocks the attacker's activity when the attacker touches the honeypot.
However, there are more possibilities how the defender could react, i.e., the more sophisticated defender might observe the attacker's activity to learn about his intentions or to even trace him back to find out how did the attacker got into the network (if the interacted honeypot is not immediately accessible from the internet, but lies in the inner structure of the network) in the first place.
The simple blocking of the attacker leaves him the knowledge how he reached to the honeypot and which types of machines contain a honeypot.
So we can assume that the attacker knows how to reach the state right before he performed the action that interacts with the honeypot.
Nevertheless, in this point could perform the action again, hoping that this time it would be on a real machine instead of the honeypot.
We could model this type of attacker by allowing him to repeat the action and paying some penalty each time he is detected by the honeypot, which is a reasonable assumption.
There are various ways of setting the penalty, i.e., proportionally to cost of actions he performed just before interacted with the honeypot, as he has to perform them again (with the different IP).
However, there are other options for the attacker, i.e., having bounded number of repetitions of each action.

