\subsection{Attack Policy}\label{sec:AP}
Attack policy describes what action the attacker should perform in every possible scenario in the attack procedure to maximize the attacker's utility.
In Fig~\ref{fig:attackPolicy} we depict a possible attack strategy of the attack graph in Fig.~\ref{fig:attackGraph}.

\begin{definition}
	An \emph{attack policy} is an oriented binary tree $\xi$ for acting on an attack graph AG.
	The nodes of attack policy $\xi$ are actions, and arcs represent possible outcomes of the previous action: either action succeeded (solid line) or failed (dashed line).
	Action \boxT indicates that the attacker terminates an attack.
\end{definition}

%This model represents a rational attacker who maximizes his reward by penetrating a specific network, but it also constitutes a form of worst case for less rational attacker.
%Optimizing the network configurations (e.g., IDS) against the smart attacker leads to a good protection against less sophisticated attackers as well.
%The main idea of our approach is to search through possible network configurations the defender can apply to the network and for each of them compute the optimal attack strategy of the attacker.
%Solving optimally an attack graph means to find a policy that minimizes the expected cost of the attack, which is an NP-hard problem.
%The policy is a binary action tree with two outgoing edges from each action -- for case when action was successful and the case when action failed (Fig.~\ref{attackGraph}).

\begin{definition} 
The \emph{expected utility} of attack policy $\xi$ is the expected value of the rewards and costs over all possible evolutions of the policy. 
Let $\xi_\chi$ be the (sub)tree of $\xi$ rooted at a node $\chi$ and labeled with an action $a$. 
The expected utility of $\xi_\chi$ can be computed recursively as follows:
$$ 
\mathbb{E}[\xi_{\chi}] = - c_a + p_a \times (\mathbb{E}[\xi_{\chi^+}] + I(\chi,\chi^+))  + \bar{p_{a}} \times \mathbb{E}[\xi_{\chi^-}],
$$
where $\xi_{\chi^+}$ (resp. $\xi_{\chi^-}$) is the subtree rooted at $\chi$'s successful branch (resp. fail branch) and in the leaf-nodes of the policy is $\mathbb{E}[\boxT]=0$,
$
I(\chi,\chi^+) = \sum_{f\in \eff(\chi^+)\setminus\eff(\chi)}{r_f}
$ is an immediate reward obtained if action $a$ was successful at node $\chi$.
\end{definition} 
Note, that the expected utility can be also computed as $\mathbb{E}[\xi_\chi] = \mathbb{E}_{reward}[\xi_\chi] - \mathbb{E}_{cost}[\xi_\chi]$, where $\mathbb{E}_{reward}[\xi_\chi]$, the \emph{expected reward}, is computed as follows:
$$
\mathbb{E}_{reward}[\xi_{\chi}] =  p_a \times (\mathbb{E}_{reward}[\xi_{\chi^+}] + I(\chi,\chi^+))  + \bar{p_{a}} \times \mathbb{E}_{reward}[\xi_{\chi^-}]
$$ 
and $\mathbb{E}_{cost}[\xi_\chi]$, the \emph{expected cost}, is computed as follows:
$$
\mathbb{E}_{cost}[\xi_{\chi}] = - c_a +(p_a \times\mathbb{E}_{cost}[\xi_{\chi^+}] + \bar{p_{a}} \times \mathbb{E}_{cost}[\xi_{\chi^-}].
$$

If the expected reward of the attacker is smaller than the expected cost of his attack, we consider the network to be well secured.
The defender then chooses the configuration that leads to worst optimal attack of the attacker, possibly taking into account his own costs.
The key component of this approach is computing the optimal attack strategy for a specific network represented by its attack graph.

Initially, we considered the attacker to be: (i) highly motivated with and (ii) having a single goal in the attack graph, what is usually assumed in the literature\cite{buldas2012upper,greiner2006finding}.
The highly motivated attacker performs the attack as long as there is any chance to reach the one goal.
%and thus it was unnecessary to have a reward function in the attack graph.
However, it is not realistic assumption that the attacker has a single goal, as there might be valuable information for the attacker in more than one machines in the network.
By allowing the attacker to have multiple goals, it is necessary to set how much each goal motivate the attacker (does all the goals have equal motivation for the attacker, or they differ?).
With this, we removed the assumption of the attacker to be highly motivated which lead to allowing him to cease the attack as soon as it is not worth for him to continue in attack (because the expected reward of goals does not exceed the expected reward of achieving the goals).
Thus, the new attacker model has modified attacker: (i) he has multiple goals with specified rewards in the attack graph and (ii) can cease the attack as soon as it is not worth for him attacking anymore, by which we model a more realistic attacker then in the previous version.


\subsubsection{Finding the Optimal Attack Policy}
We base our algorithm on the existing AI results summarized in~\cite{greiner2006finding}.
Namely, we generalize the DynProg algorithm designed for AND-OR graphs without cycles and without dependencies among actions.
We remove all these requirements, and generalize their algorithm for arbitrary dependency AND-OR graph with cycles, which allows solving any attack graph.
Moreover, we extended the algorithm by the branch and bound technique for pruning the search tree and heuristic for estimating the upper bound of the expected rewards.
Thus, we use four concepts for finding the optimal policy: (i) dynamic programming, (ii) formal properties of AND and OR sibling classes identified in (Greiner et al. 2006), (iii) branch and bound pruning and (iv) heuristic.
In comparison with our previous algorithm for solving attack graphs~\cite{lisy2013computing}, this method can generally solve twice as large attack graphs in the same time and for some specific classes of problems, it even reduces their computational complexity form exponential to linear.
We believe that the scalability of the algorithm can be still improved by utilizing better Upper and Lower Bound heuristics or using more general Sibling Class theorem.
An optimal attack policy is a policy with the maximal expected reward.

The first step in solving an attack graph is to model the attack process as a finite horizon Markov Decision Processes (MDP)~\cite{bellman1956dynamic} as is defined in~\cite{durkota2015ijcai}.
We define MDP as a tuple $\langle B, S, P, \rho \rangle$, where:
\begin{description}
	\item[$B$]$=A\cup\{\boxT\}$ is a set of actions;
\item[$S$] is a finite set of MDP states, where state $s\in S$ is defined as $s = (\alpha_s,\phi_s,\tau_s)$, where $\alpha_s\subseteq B$ is the set of actions that the attacker can still perform, $\phi_s \subseteq F$ is the set of achieved facts so far, and $\tau_s\subseteq T$ is the set of host types that the attacker has interacted with so far. 
	The initial MDP state is $s_0=(B,\emptyset,\emptyset)$.
\item[$P^a_{ss'}$] is the probability that action $a$ in state $s$ will lead to state $s'$. 
	In our case, performing action $a$ may have two outcomes:
(i) with probability $p_a$ action succeeded, which leads to new state $s' = \textsc{Succ}(s, a)$, and with probability $(1-p_a)$ action $a$ did not succeed, which leads to state $s' = \textsc{Fail}(s,a)$. 
\item[$\rho^a_{ss'}$] is the immediate cost/reward received after a transition to state $s'$ from state $s$. 
	It is based on the set of facts that become true and the action cost $c_a$.
\end{description}
The functions $\textsc{Succ}(s,a)$ and $\textsc{Fail}(s,a)$ are described in Figure~\ref{alg:alg}. 
In function \textsc{Succ}, first we remove currently performed action and its effects from the state (lines 1 and 2). 
Next we obtain a set of actions that are redundant from now on, since their effects are either achieved by the current action $a$ or there is no action which needs their effects in preconditions and at the same time those effects have no reward (line 3). 
Then we recursively remove redundant actions from the state and the remaining set of actions and facts are returned.
In function \textsc{Fail} we begin similarly with the removal of the currently performed action from the state (line 1).
Next we remove actions that cannot be achieved anymore due to the failure of the action $a$. 
Then remove the redundant actions similarly as in function \textsc{Succ} and return the remaining actions and facts.
Our MDPs have a finite horizon because there is a finite number of actions, and every action can be executed at most once. 

  \begin{figure}[b!]
    \centering
    \begin{subfigure}{.49\textwidth}
\textbf{function} $\textsc{Succ}(s=(\alpha_s,\phi_s,\tau_s),a)$      
\begin{algorithmic}[1]
\State $\alpha' \gets \alpha_s\setminus a$
\State $\phi' \gets \phi_s \setminus \eff(a)$
\State $\tau' \gets \tau_s\cup\tau_a$
\State $red\gets \{b\in \alpha':\forall f\in\eff(b):f\in\eff(a)\vee(r_f=0 \wedge (\nexists c\in \alpha':f\in\pre(c)))\}$
\State $\alpha' \gets \alpha' \setminus red$
\ForAll {$b\in red$}
 \State $(\alpha^t,\phi^t,\tau^t)\gets \textsc{Succ}((\alpha',\phi',\emptyset),b)$
 \State $\alpha' \gets \alpha'\cap \alpha_s^t$
\EndFor
\State \Return $(\alpha',\phi',\tau')$
\end{algorithmic}

    \end{subfigure}
                \hfill
    \begin{subfigure}{.49\textwidth}
\textbf{function} $\textsc{Fail}(s=(\alpha_s,\phi_s,\tau_s),a)$      
\begin{algorithmic}[1]
\State $\alpha' \gets \alpha_s\setminus a$
\State $\alpha'\gets\alpha'\setminus \{b\in \alpha':\forall f\in\pre(b):f\in\eff(a)\implies(\nexists c\in \alpha:f\in\eff(c))\}$
\State $red\gets \{b\in \alpha':\forall f\in\eff(b):f\in\eff(a)\vee(r_f=0 \wedge (\nexists c\in \alpha':f\in\pre(c)))\}$
\ForAll {$b\in red$}
 \State $(\alpha^{t},\phi^{t},\tau^t)\gets \textsc{Fail}((\alpha',\phi',\emptyset),b)$
 \State $\alpha' \gets \alpha'\cap \alpha_{t}$
\EndFor
\State \Return $(\alpha',\phi',\tau')$
\end{algorithmic}
    \end{subfigure}
    \caption{Algorithms $\textsc{Succ}(s,a)$ and $\textsc{Fail}(s,a)$ that produce successor states in MDP given state $s$ and action $a$.}\label{alg:alg}
  \end{figure} 

\subsubsection{Optimal Attack Policy Properties}
In \cite{durkota2015ijcai} we introduce several properties of the optimal attack policy, which we further make use i.e. in branch and bound.
Here we list all properties that we analyzed in the optimal attack policy.

\begin{proposition}\label{prop1}
	In the optimal policy $\xi^{opt}$ for every (sub)policy $\xi_\chi$ rooted at a node $\chi$ labeled with an action $a$ the expected reward of the whole policy is not lower than the expected reward of its negative subpolicy: $\mathbb{E}{[\xi_{\chi}]} \ge \mathbb{E}{[\xi_{\chi^-}]}$. 
\end{proposition}
Assume for contradiction that $\mathbb{E}{[\xi_{\chi}]} < \mathbb{E}{[\xi_{\chi^-}]}$. 
Then, due to the monotonicity property the attacker could have followed the (sub)policy $\xi_{\chi^-}$ before performing action $a$, which would have saved him the cost of the action $c_a$. 
This new policy would have had higher expected value than the policy $\xi_\chi$, which contradict the optimality of $\xi_\chi$. $\blacksquare$

\begin{proposition}\label{prop2}
	Let $\xi_{\chi}$ be the subpolicy rooted at a node $\chi$ labeled with action $a$, then:
	$\mathbb{E}{[\xi_{\chi^+}]} + I(\chi,\chi^+) - c_a/p_a \ge \mathbb{E}{[\xi_{\chi^-}]}$.
\end{proposition}
\begin{eqnarray}
	&\hspace{6mm}\mathbb{E}[\xi_{\chi}] &= - c_a + p_a(\mathbb{E}[\xi_{\chi^+}] + I(\chi,\chi^+)) + \bar{p_a}\mathbb{E}[\xi_{\chi^-}]\\
	\text{Prop. 1}\Rightarrow &\hspace{6mm}\mathbb{E}[\xi_{\chi^-}] &\le -c_a + p_a(\mathbb{E}[\xi_{\chi^+}] + I(\chi,\chi^+)) + {\bar{p_a}}\mathbb{E}[\xi_{\chi^-}]\\
	\bar{p_a}=(1-p_a)\Rightarrow &p_a\mathbb{E}[\xi_{\chi^-}] &\le -c_a + p_a(\mathbb{E}[\xi_{\chi^+}]+I(\chi,\chi^+)) \\
			      &\hspace{6mm}\mathbb{E}[\xi_{\chi^-}] &\le -c_a/p_a + \mathbb{E}[\xi_{\chi^+}] + I(\chi,\chi^+) 
\end{eqnarray}

\begin{proposition}\label{prop3}
	Let $\xi_{\chi}$ be the subpolicy rooted at a node $\chi$ labeled with action $a$, then:
	$\mathbb{E}{[\xi_{\chi^+}]} + I(\chi,\chi^+) - c_a/p_a \ge \mathbb{E}{[\xi_{\chi}]}$.
\end{proposition}


Other properties are stated and proved in appendix in \cite{durkota2015ijcai}.
Also note that the proposition \ref{prop2} does not hold if attacker is not highly motivated and cannot terminate the attack when it is not worth for him to continue.


\subsubsection{Cycles in Attack Graph}
In the literature it is usually to dealt with a directed acyclic attack graphs, since introduction of the cycles in the attack graphs may convey certain difficulties.
In~\cite{wang2008attack} authors deal with the three types of cycles, depicted in~\ref{fig:cycles}.
For each of the cycles they propose how to deal with them: either they can be ignored, broken or must be dealt with in a non-trivial way.
The cycle types differ from each other in a way they are entered.
We argue, that our MDP approach can solve all mentioned cycles without need of their removal or other procedures.

An example of the first type of cycles is depicted in~\ref{fig:cycle1}.
Notice, that there is only one entering point to the cycle, through the action $e_1$. 
However, this action requires the fact $c_3$ to be true and following its preconditions, it can be easily seen that this cycle cannot be ever entered.
Our MDP approach will never enter this cycle either, because it will never explore the possibility of performing the action $e_1$, as it's both preconditions will never be true.

An example of the second type of the cycle is depicted in~\ref{fig:cycle2}, which has an entering point through the fact $c_2$.
In the contrary to the first type, this cycle can be entered; however, it can be broken into the structure without the cycle. 
The new attack graph created by the removal of $e_2$ and $c_3$ and corresponding incoming and outgoing edges would produce alike optimal attack policy as the original attack graph.
In our MDP's approach when the cycle is entered, the \textsc{Succ} procedure detects the irrelevant actions and removes them from the candidate list.
Thus the action $e_2$ is immediately removed and is never considered to be performed, which essentially, breaks the cycle and solves the problem.

Finally, the third type, where two (or more) entering points exist, and both are through the fact nodes.
According to the authors in~\cite{wang2008attack}, this cycle cannot be either removed nor simply broken.
Although there are two ways to break it, either removing node $e_4$ and corresponding edges or the action $e_3$ and corresponding edges neither case will result in the attack graph that will produce equivalent attack policy to the original one.
Still, for our approach this is not an obstacle either, as our MDP breaks the cycle in either way, depending if action $e_1$ or $e_2$ is considered to be performed.
If action $e_1$ (resp. $e_2$) is successfully performed, the \textsc{Succ} procedure will remove all unnecessary actions from the candidate list, including the action $e_4$ (resp. $e_3$).

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{fig/cycle1}
		\caption{}
		\label{fig:cycle1}
	\end{subfigure}%
	~ 
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=\textwidth]{fig/cycle2}
		\caption{}
		\label{fig:cycle2}
	\end{subfigure}
	~ 
	\begin{subfigure}[b]{0.35\textwidth}
		\includegraphics[width=\textwidth]{fig/cycle3}
		\caption{}
		\label{fig:cycle3}
	\end{subfigure}
	\caption{Three types of cycles in attack graph from~\cite{wang2008attack}}\label{fig:cycles}
\end{figure}



\subsubsection{Optimizations}

Our basic approach for solving these MDPs is to use an exhaustive depth-first search that evaluates every possible action at every decision point and selects the policy that has the maximum expected reward using backward induction. 
We improve the performance of this basic search method using several techniques to prune suboptimal actions and cache results for known states.
Figure~\ref{fig:decisionPoint} is an example of the MDP search for our running example from Figure~\ref{fig:attackGraph}. 
The root of the MDP is a decision point where we need to decide which action among \texttt{"Exploit Firewall"}, \texttt{"Send MW Email"} and \texttt{"Create Dictionary"} is best.
In a na\"{i}ve approach we explore every possible scenario and compute their expected rewards $\mathbb{E}[\texttt{"Exploit Firewall"}], \mathbb{E}[\texttt{"SendMW Email"}]$ and $\mathbb{E}[\texttt{"Create Dictionary"}]$.
We choose the action with the maximum expected reward.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\textwidth]{fig/decisionPoint.pdf}
	\caption{An example of na\"{i}ve MDP search.}
	\label{fig:decisionPoint}
	\vspace*{-0.4cm}
\end{figure}

\paragraph{Dynamic Programming}
We examined several techniques for the performance enhancement of the optimal policy planning.
One of the most beneficial was usage of the \emph{dynamic programming}.
MDP States can often be reached via many different sequences of actions (e.g., different orders of previous actions with the same cumulative effects).
Therefore, we cache the values of states of which we have computed an expected rewards and an optimal (sub)policy and reuse these if the state is encountered again.


\paragraph{Sibling-Class Pruning}\label{par:st}

The complexity of optimal attack planning originates in the need to explore all possible orderings of actions.
The sibling-class theorem states that in some situations, the optimal order for executing actions can be determined directly without any search. This theorem was proven in \cite{greiner2006finding} in the context of ``probabilistic AND-OR tree resolution" (PAOTR). In \cite{buldas2012upper}, the AND part of the theorem is  proven in the context of simplified attack graphs. In both of these works the actions can be present only in the leafs of the AND-OR graph, without any preconditions. The inner nodes represent only the conjunctions and disjunctions and do not have any costs or probabilities of success. Moreover, the theorem in \cite{greiner2006finding} is proven only for AND-OR trees in which no node can have more than one parent. There is no notion of reward and the AND-OR tree is evaluated until the root of the tree is resolved.

In the attack graphs generated by the existing network security tools (e.g., MulVAL \cite{ou2006scalable}), attack actions are present in the inner nodes of the attack graphs, which can also be cyclic graphs.
Furthermore, we assume that the attacker can obtain intermediate rewards for activating subsets of fact nodes and not only for reaching the root of the attack graph.
Here we generalize the sibling-class theorem to handle these additional cases.
Due to space limitations, we provide only proof sketch here based on the original proof.
We find that the original proof holds with minor modifications.
To describe the theorem we first need to define several additional concepts.

\begin{definition}
	Action $a$ is \emph{executable} if and only if it's preconditions are satisfied.
\end{definition}

\begin{definition}
	\emph{OR-sibling class} is a set of executable actions that have the set of effect facts $f \subseteq F$ in the attack graph and do not have any other effect besides $f$.\\
	\emph{AND-sibling class} is a set of executable actions $X\subseteq A$ in the attack graph, such that there is a ``grandchild'' action $g\in A$ that can be executed if an only if all of the actions in $X$ are successful ($\pre(g) \subseteq \bigcup_{a\in X}\eff(a) \;\;\&\;\; \forall b\in X\; pre(g) \setminus \bigcup_{a\in X\setminus\{b\}}\eff(a)\neq \emptyset$) and none of the actions has an effect that would help enable other actions, besides $g$:
	\[\forall a_1,a_2\in X; g_1,g_2\in A \;\; \eff(a_1) \subseteq \pre(g_1) \& \eff(a_2) \subseteq \pre(g_2) \Rightarrow g_1=g_2.\]
\end{definition}

\begin{definition}
	We define the \emph{R-ratio} of actions in sibling classes as
	\begin{align}
		R(a) &= \frac{p_{a}}{c_a} \textrm{ if action $a$ is in OR-sibling class; and} \\
		R(a) &= \frac{{1-p_a}}{c_a} \textrm{ if action $a$ is in AND-sibling class.}
	\end{align}
\end{definition}

The sibling theorem states that actions in the optimal policy for an attack graph that belong to the same sibling class are always executed in decreasing order of their R-ratios.

\begin{theorem}[Sibling class theorem]
	Let $AG$ be an attack graph and let $\xi_{opt}$ be the optimal policy for the attack graph. Then for any actions $x,y$ in the same sibling class, such that $R(y)>R(x)$, $x$ is never performed before $y$ in $\xi_{opt}$.
\end{theorem}


The proofs for the AND and OR sibling classes are symmetric; hence, we focus only on the OR sibling class. The proof of this theorem in \cite{greiner2006finding} relies on a lemma about attack policies. If we denote a tree rooted in action $x$ with left subtree $\xi_+$ and right subtree $\xi_-$ by $(x;\xi_+;\xi_-)$ then the lemma can be rephrased in the following way:

\begin{lemma}\label{lem:switch}
	Let $x,y\in A$ be actions in the same OR-sibling class of an attack graph such that $R(y) > R(x)$. Let $\xi_{xy}$ be an optimal policy for the attack graph in the form $(x;\xi_+;(y;\xi_+;\xi_-))$, then $\xi_{yx}=(y;\xi_+;(x;\xi_+;\xi_-))$ is a valid policy with a higher expected reward than $\xi_{xy}$.
\end{lemma}


\begin{figure}[t]
	\includegraphics[width=\textwidth]{fig/siblingsProof}
	\caption{Illustration of the proof of the sibling theorem taken from \cite{greiner2006finding}.}\label{fig:siblings}
\end{figure}

The proof of the Sibling class theorem in \cite{greiner2006finding} works by induction on the number of attack actions in the optimal policy. It is demonstrated on Figure~\ref{fig:siblings} taken form the article. For contradiction, assume that there are actions $x$ and $y$ in the wrong order in the optimal attack policy. Without loss of generality, assume a case with an action $x$ in the root of the policy and its sibling $y$ at several places in the negative branch of $x$ (Figure~\ref{fig:siblings}(a)). The positive branch does not include $y$, because it is an OR-sibling of $x$, which means that the fact achieved by $y$ is already achieved by $x$, if it is successful. The proof shows that the expected utility of the policy is not decreased if it starts as the negative branch of $x$ and action $x$ is executed just before action $y$ would be executed in the original policy (Figure~\ref{fig:siblings}(b)). Then the proof uses the Lemma~\ref{lem:switch} to show that if each instance of the actions $x$ and $y$ are switched (Figure~\ref{fig:siblings}(c)), the utility of the policy is increased if the R-ratio of $y$ is higher than R-ration of $x$.

We argue that a very similar proof is applicable for the more general case of attack graphs defined above.
The differences between the model in ~\cite{greiner2006finding} and our model are:

\begin{description}
	\item[i)] The attack graph is in general a cyclic graph and not a tree.
	\item[ii)] The attack graph has actions with a probability of success and costs in the inner nodes, and not only in the leaves. This creates preconditions for the actions.
	\item[iii)] Different attacks give different rewards and it is not necessary to resolve the root node of the attack graph.
	\item[iv)] The attack terminates when the expected cost of continuing the attack is higher than the expected reward, not when the root of the AND-OR tree is resolved.
\end{description}

The proof is based on properties of the optimal policy with very little reference to the structure of the AND-OR graph. The only use of the AND-OR graph is to assure that after the transformations, the policy is still a legal policy. From the definition of the sibling classes, any action that belongs to a sibling class is a leaf of the attack graph. It does not have any preconditions and it can be executed at any place in the policy. The inner nodes of the attack graph are not moved in the tree and the transformations preserve any preconditions the following actions might have. This means that properties (i) and (ii) do not have any effect on validity of the proof.

The policy in \cite{greiner2006finding} does not have any rewards, only costs for the actions. The proof assumes that each subtree of the policy has an expected cost of execution, but it never requires the costs of the sub-trees to be positive. We can define the expected reward (or negative cost) of a leaf node $\boxT$ as the sum of the rewards acquired in the attack branch from the root to the leaf. Afterwards, we can treat this leaf as any other subtree in the proof. This resolves difference (iii).

The last difference between the models is caused by changing the point when the attack stops. The attack stops at node $\boxT$ if the expected value of the optimal sub-policy continuing from this node is negative.
This is a solely a property of the successors of a node in the policy tree.
The transformations used in the proof do not change which actions can be executed below a current leaf in the policy and they do not change what other facts can be activated below the current leaves.
Therefore, it cannot be optimal to prolong any branch after the transformations.
Furthermore, no attack would be terminated earlier due to the transformations. The attacker stops an attack if the expected reward of some subtree would become negative. The proof initially assumes an optimal strategy and shows that the expected utility of the strategy stays the same when $x$ is moved just before $y$. If the expected cost of playing the subtree rooted at the new position of $x$ were positive, stopping the attack at that place would increase the utility of the whole strategy, which contradicts the optimality of the original strategy. The same holds after switching the positions of $x$ and $y$. The original proof states that the utility is increased when the two are switched. If in addition, some of the subtrees would have negative expected reward after the switch. Removing the subtree would increase the expected utility even more, which still means that the original strategy was suboptimal.

Therefore, when we consider which action to perform at a decision point we can consider only those with the highest R-ratios in each sibling-class and those that have multiple parents or grand-parents. After some actions in the attack graph are executed the inner nodes in the attack graph can become leaf nodes and the sibling theorem is then applicable to those nodes as well. The use of the sibling theorem is demonstrated by the following example.

Assume we have the same problem as in Fig.~\ref{fig:decisionPoint} and we come to the same decision point as previously, but now we computed $R(\texttt{"Exploit Firewall"}) = \frac{0.27}{5.0} = 0.054$, $R(\texttt{"Send MW Email"}) = \frac{0.23}{2.0} = 0.115$ and $R(\texttt{"Create Dictionary"}) = \frac{1.0}{11.0} = 0.091$ and we know that actions \texttt{"Exploit Firewall"} and \texttt{"Send MW Email"} have the effect \texttt{"Net Access"} and therefore belong to the same OR-sibling class, while action \texttt{"Create Dictionary"} belongs to a separate sibling class. Now we explore only the actions that have maximal R-ratios in each sibling class:  \texttt{"Send MW Email"} and \texttt{"Create Dictionary"}.
The action \texttt{"Exploit Firewall"} surely will not be the first action in the optimal policy.
Fig.~\ref{fig:decisionSibling} shows in the branch and bound search tree the actions that belong to the same class (yellow), nodes that must be explored (white), and nodes that are pruned (grey).
\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\textwidth]{fig/decisionSibling.pdf}
	\caption{This figure presents nodes that are explored (white) and nodes that can be pruned (grey) in the MDP search if we know that actions \texttt{"Exploit Firewall"} and \texttt{"Send MW Email"} are in the same sibling class and action R-ratios.}
	\label{fig:decisionSibling}
	\vspace*{-0.4cm}
\end{figure}
\vspace*{-0.4cm}

However the Sibling-Class Theorem works to a limited level.
It does not examine the higher structures then are the leaves and their parents of the attack graph.
For example, it successfully detected that action 17 and 25 from Fig.~\ref{fig:stExample}, which are in the OR relationship, are in the same class and thus one of them can be pruned when attacker decides which to perform.
However notice, that actions 27 and 25 might be also in the same class and that the attacker could reason about them in some level, although they do not have same effect.
There may be even more general structures detectable in the attack graph which could help to prune the search tree.
These structures are obviously more difficult to detect than the current examinations of the leaf and leaf-parents relationships.
However, it may be worthy speed up of the optimal attack policy computation

\begin{figure}[ht!]
	\begin{center}
		\begin{tikzpicture}[<-, scale=0.7, every node/.style={transform shape}]
			\node [ffact] {$f_1$\\+100} [grow=up]
			child{ node [act] {$a_1$\\$(0.1,2)$}}
			child{ node [act] {$a_2$\\$(0.2,5)$}
				child{ node [ffact] {S\\+20}
					child{ node [act] {$a_3$\\$(0.5,8)$} }
				}
			};
		\end{tikzpicture}
	\end{center}
	\caption{Example of simple attack graph.}\label{fig:stExample}
\end{figure}



\paragraph{Branch and bound}

The third pruning technique we use is branch and bound.
We compute lower (LB) and upper (UB) bounds of the expected reward in each MDP state.
Consequently, we use them to prune the MDP subtrees if they are provably suboptimal.

{\bf Lower bounds} are computed from the values of the previously computed MDP subtrees and bound the future MDP subtrees.
E.g., in Fig.~\ref{fig:decisionSibling} the attacker decides wither start with action ``Send MW Email'' or ``Create Dictionary'' (the ``Exploit Firewall'' is pruned out at this decision point).
After exploring action ``Send MW Email'' and obtaining the attacker's expected utility for that action, we use this value as a lower bound for action ``Create Dictionary''.
The attacker will switch to begin with action ``Create Dictionary'' only if its expected utility is higher than beginning with action ``Send MW Email'', therefore, it is reasonable lower bound.

We compute the {\bf upper bound} as the expected reward of the optimal policy in an attack graph relaxed in two ways.
First, we ignore the action costs ($c_a = 0$). % and the probabilities of touching HPs (h = 0).
This only improves the expected utility of the attack and causes the optimal strategy to always use all available actions.
Therefore, we can compute the expected reward of the policy by computing the probability that each fact is achieved if the attacker attempts all actions.
In order to compute this probability efficiently, we run a depth first search in the attack graph from each fact which provides a reward, traversing edges only in the opposite direction.
Moreover, to avoid complications caused by cycles, we disregard edges leading to AND nodes from already traversed parts of the attack graph.
Removing edges to AND nodes can always only increase the probability of reaching the reward fact.
More details can be found in \cite{durkota2015ijcai}.


\paragraph{Action Merging}
As mentioned above, computing an optimal attack policy is an NP-hard problem due to the number of actions that attacker can perform.
If it was be possible to reduce the number of actions in the attack graph, computation complexity would tremendously decrease.
Thus, we initially focused the research on the possibility of reducing the number of actions by replacing a certain substructures of the attack graph by one action which would represent the whole substructure.
E.g., the chain structure seemed to be a good candidate, by which we mean a list of actions $C = \langle a_1,\dots,a_N\rangle$, where effect of previous action is an preconditions of the following action, formally: $\eff(a_i) = \pre(a_{i+1})$.
The whole chain $C$ could be replaced by one meta-action $a'$, with following rules:
\begin{itemize}
	\item preconditions of $a'$ are equal to the preconditions of the first action in the chain, thus $\pre(a') = \pre(a_1)$
	\item effects of the $a'$ are equal to the effects of the last action in the chain, thus $\eff(a') = \eff(a_N)$
	\item cost $c'$ of the meta action $a'$ is the expected cost of the chain structure, thus $c' = c_1 + p_1c_2 + p_1p_2c_3 + \dots + p_1\dots p_{N-1}c_N$ (or $c' = \sum_{i=1}^{N}(c_i)$)
	\item probability $p'$ of the meta action $a'$ is a joint probability of the chain structure, thus $p' = p_1\dots p_N$.
\end{itemize}
This action reduction idea is based on the fact, if attacker's optimal policy is to perform the first action in the chain structure, and the action is performed successfully, it is optimal to perform the next action in the chain.

Idea behind is that if attacker's optimal policy is to perform a leaf-action in the chain structure, then if action is successful, it is optimal for the attacker to continue in the chain.
It is a reasonable assumption, that if the first action in the chain-structured branch is the cheapest for the attacker to perform, then after successfully performing this action, that branch cannot become more expensive, only cheaper, while other actions stay untouched, and thus equally expensive, as before performing the first action in the chain-structure.
Unfortunately, the previous assumption does not hold.
In Fig.~\ref{fig:merge} we present the counterexample.
By our previous assumption, we could merge the actions $a_2$ with $a_8$ and create a meta-action $a'$ with cost $c' = 8.2 + 0.5 512 = 264.2$ and probability $p' = 0.25$.
Unfortunately, in the optimal policy the action $a_2$ is not followed by action $a_8$ in neither case. 
In the optimal attack policy attacker performs action $a_8$ just to be informed if in the future he will be able to perform the action a2 (if a8 is successful) or not (if a8 is unsuccessful).
And this knowledge will change his strategy in the right subgraph of the attack policy (either to start with performing the action $a_{11}$ or $a_{12}$.


\begin{figure}[t]
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
		\begin{tikzpicture}[<-, scale=0.7, every node/.style={transform shape}]
			\tikzstyle{level 1}=[sibling distance=4.6cm] 
			\tikzstyle{level 2}=[sibling distance=4cm] 
			\tikzstyle{level 3}=[sibling distance=3cm] 
			\node [ffact] {$f_1$\\+1000} [grow=up]
			child{ node [act] {$a_2$\\$(0.1, 2, \{\})$} 
				child {
					node[ffact] {$f_4$}
					child{ node [act] {$a_8$\\$(0.5, 8.2, \{\})$} 
					}
				}
			}
			child{ node [act] {$a_3$\\$(1, 0, \{\})$}
				child{ node [ffact] {$f_6$}
					child{ node [act] {$a_{11}$\\$(0.5, 64.5)$} }
				}
				child{ node [ffact] {$f_7$}
					child{ node [act] {$a_{12}$\\$(0.5, 8.3, \{\})$} }
					child{ node [act] {$a_{13}$\\$(0.5, 512.4, \{\})$} }
				}
			};
		\end{tikzpicture}
		\label{fig:mergeCE}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=\textwidth]{dot/mergeOptStg}
                \caption{}
		\label{fig:mergeAP}
        \end{subfigure}%
	\caption{(a) Counterexample for merge concept from; (b) An optimal attack policy for (a)}\label{fig:merge}
\end{figure}

However, this approach could be used to compute an approximately optimal solution or as a lower or upper bound of the optimal attack policy.  For example, by inserting the meta-action where it's cost is the cost of the first action in the chain, while probability stays alike, the optimal attack policy of the this new attack graph will be a lower bound of the original attack graph.
Similarly, by setting the meta-actions' cost as a sum of all the actions in the chain, and having the probability set to the probability of the first action in the chain will produce an optimal attack graph which is an upper bound of the original attack graph.  

\subsubsection{Network Hardening}
In the literature the attack graphs are usually used to analyze the most vulnerable parts of the network or finding the cheapest subset of vulnerabilities in the network, so that when they are patched, the goal is not reachable for the attacker anymore.
However, having known the optimal attack policy that the optimal attacker would follow, we can produce much cheaper subset of vulnerabilities.
For the attacker not to attack the network it is sufficient to configure the network (patch the vulnerabilities) in such a way, that it is not worth for him attacking.
Specifically, when the expected reward is lower than the expected cost of the optimal attack policy.
This analysis would surely give cheaper solution then the one that blocks the attacker completely.
