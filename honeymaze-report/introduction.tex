%!TEX root = main.tex

\section{Introduction}

In this report we summarizes the technical achievements to date for the project ``Optimizing Heterogeneous Intrusion Detection System Against Rational Adversary'', which has been conducted as a joint research effort between the Czech Technical University (CTU) and the University of Texas at El Paso (UTEP) with support from the Office of Naval Research Global.
The project has been successful in developing a collaborative research effort between the two universities, with researchers working closely throughout the project and exchanging visits between the two institutions. 
In addition, we have made significant scientific advances related to the technical goals of the project, many of which have already led to publications, with other publications in progress. 
We begin by describing the background and motivation of the project, and then provide an overview of the major contributions of the research effort. 
We discuss the research contributions in greater detail in the subsequent sections of the report and in the publication resulting from the project attached at the end of this document.

\subsection{Background and Motivation}

Networked computer systems now play a critical role in daily life, commercial interests, and military operations.
Protecting these systems against sophisticated intruders including both criminal organizations and state actors is one of the most important challenges for national security in today’s highly interconnected world.
Even though there is an increasing volume of research in computer network security, the main focus of the research is on specific technical methods and identification of vulnerabilities, and the suggested approaches often lack rigorous foundations and a detailed modeling of the adversarial interaction between defenders and attackers.
This makes it difficult to compare the effectiveness of different security measures and to select the most suitable combination of security measures to protect a computer system or network.
The lack of the rigorous foundations is mostly caused by the complexity of the domain and its openness -- in real-world conditions, there are endless options available to the attackers that can never fully fit into one formal model.
In addition, computational scalability of solution methods is a significant problem for the existing formal models, which often cannot scale to analyzing realistic networks.
In spite of these findings, it is still important to create formal models of specific situations in the network security and seek for theoretically sound solutions with well-defined performance metrics. 

Our work seeks to develop formal models for network security decisions and resource allocation problems that explicitly capture the adversarial interactions inherent in the domain by applying game-theoretic methods.
In addition, we aim to address computational limitations in the analysis by developing new solution methods that are much more scalable than existing approaches so that formal modeling can be applied to more realistic cases.
The benefits of developing a valid formal approach stems from having a possibility of sound experimental evaluation of the proposed model, identifying the gap between the theoretical model and the real world and thus identifying the phenomena not covered by the theory and further extending theoretical models.
Following this methodology we can increase the portion of the covered problems in network security with well understood effectiveness, guarantees and trade-offs between the quality and resource limitations.

Formal models of network security phenomena can be built in various frameworks, such as information theory, statistical machine learning or decision theory.
However, the recent attacks, such as described in \cite{alperovitch2011revealed} and frequently called Advanced Persistent Threats \cite{cisco11}, show that the attackers have the skills and resources to avoid security measures using custom written malware, exploiting the information about work structure, procedures and customs of the users within the target organization.
The attackers are increasingly aware of used defensive techniques and avoid the detection by very targeted and well-informed operation inside the compromised network.
They are actively reasoning about the actions of the network administrators who are in turn reasoning about the actions of the attackers.
Game theory is a useful framework designed to model situations with multiple self-interested parties interact in a shared environment.
We adopt this framework in our proposal and focus on game theoretic models of network intrusion detection and honeypots used as a security strategy.
We aim to provide performance guarantees within the models and identify and evaluate the attacker’s courses of action not covered by the models and the effect of partially satisfied assumptions and uncertainty in the model.

One of the key components of our modeling approach for network intrusion detection is to use attack graphs to represent possible attacker plans.
Attack graphs are a rich representation that captures the most important actions that can be executed by an attacker to compromise a system or network, and the dependencies between these actions.
They are commonly used for penetration testing and representing attack plans, and they can be automatically generated for a specific network based on scanning results and extensive databases of known software vulnerability.
Furthermore, the attack graphs can be easily extended by potential unknown vulnerabilities similar to the known once.
However, attack graphs are not commonly used within a game-theoretic framework that also considers defender actions to secure the network, which we consider in our work. 

Based on the attack graphs, we can analyze what attack actions are necessary for a successful attack and what are the chances that they will be detected by existing intrusion detection systems, such as the statistical network traffic analysis tool CAMNEP (developed under W911NF-12-1-0028), or specialized detector tuned for specific attack actions classes.
In our previous project, we have identified honeypots as a particularly useful tool for detecting certain classes of attacks.
We designed a game theoretic model for determining the configuration of honeypots that maximize the chance that a sophisticated attacker will use them in an attack.
In this project, we consider honeypots as detection methods and countermeasures against attacks in the context of richer attack graph representations for attacker strategies.
We specialize the use of honeypots so that only the attack actions that are difficult to detect using other intrusion detection methods are the primary purpose of the honeypots. There attack actions are mainly exploits of specific vulnerabilities in software and services running on the network. Single request, statistically very similar to any other request, is sufficient to compromise security of a node, which is virtually impossible to detect using statistical IDS. Therefore, an overall system consisting of a combination of different forms of intrusion detection techniques is more effective and efficient.
We also focus on approximate solution methods for deciding on the strategies for using honeypots and other intrusion detection methods that will allow solving more complex problems arising from adding the attacker's uncertainty about the attacked network.

%At the end of the project, we aim to unify individual detection methods in one framework that will, based on expected performance of the methods, decide what combination of the detection methods will be used to optimize the performance of the whole system.

We worked on this project collaboratively with University of Texas El Paso.
The main collaborator from UTEP is Christopher Kiekintveld, one of the key researchers in the area of resource allocation security games, which quickly lead to many real world applications.
The aim of this project was also to extend the collaboration to the experts from the Information Security Division of Govermental CERT Georgia and from NATO Cooperative Cyber Defense Centre of Excellence Estonia.

\subsection{Contributions}

Here we briefly highlight the major research tasks of this project and the contributions that have been achieved.
These contributions are described in detail throughout the rest of the report and in the supported papers included in Appendix~\ref{app:papers}.
Work on the project was initially organized around six research tasks listed below, though based on the initial results additional work was conducted in some areas while other received reduced focus. 

\begin{description}
\item[RT1:] Basic attack graph generation
\item[RT2:] Generation of attack graphs
\item[RT3:] Scalable (approximate) methods for analyzing attack graphs
\item[RT4:] Intrusion detection game
\item[RT5:] Proof of concept simulations
\item[RT6:] Real world network experiments
\end{description}

\subsubsection{Basic attack graph generation}\label{sec:rt1}

Attack graphs aim to capture all known sequences of actions that can lead to compromising the network.
They may contain additional information, such as the cost of the individual actions and the probability that the action will be successfully executed.
Using attack graphs, we can model the attacker's behavior, identify an optimal strategy of the attacker and optimize network configuration against a rational attacker.
There are multiple commercial as well as open source tools for generating attack graphs.
They are summarized in \cite{yi2013}.
In order to enable other parts of this project, we have created a simple pipeline of open source tools for generated attack graphs for real world as well as synthetic networks.
These attack graphs are sufficient for proof-of-concept prototypes and include all important structures possible in attack graphs.
However, wider coverage of different kinds of attacks would be achievable with more sophisticated attack graphs generation rules.

There is an interactive and a fully automated option for creating attack graphs. In the interactive option, 
we use the freeware diagram editor yED to represent the network topology together with the machines' different local, remote, and client vulnerabilities.
The output of the editor can be processed by other components and turned into the input file for the open-source attack graph generator MulVAL from Kansas State University.
This tool allows generating the attack graph in both graphical format and simple CVS format which can be easily further processed automatically.
An action in the attack graph corresponds mainly to using individual vulnerability to gain access to a new host or escalate privileges.
In order to do the attack-graph based optimization, we need to assign a cost and probability of success to each action.
MulVAL is also able to use the National Vulnerability Database to provide rough estimates of the probabilities of success of individual attack actions.

For larger networks and actual deployments in practice, a fully automated way for creating attack graphs is preferable. They can be created automatically based on the data obtained using network security scanner, such as Nessus or OpenVAS. These tules can scan the network for available hosts, find the open ports, download firewall rules tables from routers, and even connect to all hosts using provided credentials and download lists of installed software packages and their specific versions. Based on this lists, these programs allow searching vulnerability databases, such as National Vulnerability Database, for specific security issues present on the specific hosts. Exploiting these vulnerabilities than becomes actions in the attack graph and MulVAL can use this information to create attack graphs representing their meaningful sequences.

%Besides the input to our game theoretic models, the produced attack graphs can be used for evaluating the learning approaches.
%Random or heuristic attacker��s models can generate attacks and we can measure how quickly the system has adapted to detect these attacks.



\subsubsection{Generation of attack graphs}

In this research target, we evaluated the strengths and the limitations of the existing attack graph generation tools and possibly improve these tools with additional rule sets representing new attack types.
This research target was conditioned by having a more applied partner in the project.
At the beginning of the project, it took more time than we expected to identify suitable partners.
The partners suggested in the proposal were interested in the project, but did not have available human resources to participate.
In the first year of the project, we started collaboration with Giorgi Gurgenidze, a network security expert form the Republic of Georgia.
His analysis of one typical attack scenario is presented in Section~\ref{sec:georgi}. 
This section discusses individual actions in an attack plan, how difficult it is to detect them using honeypots of alternative intrusion detection system.
Further collaboration with Giorgi Gurgenidze became impaired due to his relocation and other responsibilities.
Acquiring few more scenarios like this, could evaluate how well they are covered by the automatically generated attack graphs and possibly suggest their improvements. Despite of repeated promises to provide more similar analysis, we were not able to get any additional report to this day and therefore, we have discontinued this collaboration.

The most important insufficiency we have identified in the existing attack graph generation tools is a simplistic computation of the expected probability of success of individual attacks actions and absence of automated tools for estimating costs of performing individual attack actions by the attacker. In order to solve this problem, we have focussed on possible sources of these data and machine learning methods to generalize the existing data to create estimates for all possible attack actions.
Learning was based on information vulnerability information from National Vulnerability Database, CVSS (Common Vulnerability Scoring System) metric vector describing its characteristics, and other sources information available online.
We used CVSS vector along with other features (i.e., vulnerability age, whether exists a downloadable exploit for it, etc.) and based on the bounty rewards we estimate the attacker's cost for exploiting the vulnerability.
 This way, we have created a system that provides estimates of these costs for our game theoretic models. The details of the analysis and the results are provided in Section~\ref{sec:labut}.

\subsubsection{Scalable (approximate) methods for analyzing attack graphs}
In this part we developed an algorithm that analyses the attack graph and based on its structure, actions' costs and actions' probabilities, it generates an optimal attack policy in sense that it has the highest expected reward for the attacker. 
We have developed a basic method for this problem in the previous project (N62909-12-1-7019).
In the current project, we have leveraged the previous AI research in solving probabilistic AND-OR graphs and MDPs to improve scalability of computing the optimal attack based on the attack graphs.
We relaxed several restrictions of the previous work and allowed the attacker to act in a more realistic manner, e.g., the attacker may have more than one goal in the attack graph, may want to cease the attack as soon as it is not worth for him to attack (final expected reward is not worth the costs of actions), and may have uncertainty about the honeypot allocations in the attacked network.
Even though the problem is NP-hard, as attacker has to analyze up to the $N!$ many action sequences, where $N$ is the number of actions in the attack graph, now we are able to optimally solve attack graphs with 50 different attack actions, which is approximately twice as many as with our previous method.
Furthermore, we designed simple fast heuristics that allow quickly computing bounds on the expected reward of the optimal attack.
We further use these techniques in our game theoretic models.
We have published the initial work on finding optimal attack policy in \cite{durkota2014stairs} and~\cite{durkota2015ijcai} together with extended version with attacker uncertainty in~\cite{durkota2015gamesec}.
%We use this algorithm to solve a proposed game-theoretic models in next point. %to find the optimal honeypot allocation in a perfect information game, published in~\cite{durkota2015ijcai}.
%Additionally, we extended the game introducing the attacker's uncertainty about the attacked network and proposed several approximation algorithms to solve it, published in~\cite{durkota2015gamesec}.
All of these papers are attached in Appendix~\ref{app:papers}. 
Brief introduction to the results in the papers along with additional results not included in the papers is presented in this Section~\ref{sec:planning}.

\subsubsection{Intrusion detection game}
In the main line of work, we have developed methods for optimizing objectively measurable network security properties against a rational adversary.
We have developed three main game models with different requirements on the amount and precision of knowledge required to execute the prescribed strategies.
If we have a good model of the attacker's rationality, a reliable estimate of the attacker's options in the network, attacker's action costs, and rewards and penalties for a successful and unsuccessful attacks, we can use the first two models build on standard game theoretic approach.

They are based on automatically generated attack graphs.
We formulate the generic problem of network configuration selection as a Stackelberg game between the attacker and the defender (i. e., the network administrator).
The actions of the network administrator are different options for network configuration, such as the set of honeypots that the administrator adds to the network.
The task of the attacker is then to choose the optimal attack in the network based on the configuration selected by the defender.
We model this task of the attacker as the problem of finding an optimal attack policy for the attacker based on an attack graph.
The game-theoretic model than allows selecting the optimal configuration for the defender, maximizing the trade-off between the costs for the selected honeypots and expected loss for having the network attacked.
A key factor in the computational complexity of this approach is computing the optimal attack policy of the attacker, for which we use the algorithms developed in RT3.
The main difference between the game models is the amount of information assumed to be available to the attacker, when computing the optimal attack strategy.
The first model is more pessimistic and assumes that the attacker knows exactly the original state of the network and the security measures deployed by the defender.
This model is analyzed in details in \cite{durkota2015ijcai} and Section~\ref{sec:gamePI}.
The second model weakens the assumed amount of information available to the attacker.
It assumes that the attacker has beliefs about the set of networks the defender could possibly be protecting in his company and likelihood of deploying each network.
The attacker can observe the current state of the network, but does not know neither the original state of the network before introducing honeypots nor the exact actions performed by the defender.
This model is a direct extension of the abstract honeypot selection game we introduced in \cite{pibil2012gamesec}.
Solving this game exactly has very low scalability, therefore, we developed several approximation algorithms which uses state-of-the-art approaches and present their detailed analysis and comparison in published paper~\cite{durkota2015gamesec}.
The high level description of the model and the algorithm is presented in Section~\ref{sec:gameII}.
Both of the previous game models find (approximately) optimal randomized honeypot allocation for the network administrator, but does not learn or adopt it according to the attacker's behaviours.

Our third model combines the game-theoretic approach with online learning model. If the attackers do not behave optimally, or if the estimates of action success probabilities or costs used for computing the game theoretic strategies are imprecise, the strategy computed by game theory may be suboptimal. Furthermore, the strategies of the attackers can unexpectedly change during deployment of the system, for example due to availability of new  attack tools or zero day exploits. Therefore, we have also investigated the possibility of combining the pre-computed game theoretic strategy with online learning, which could adapt the deployed strategy based on observed attacks. This approach is promissing mainly in domains with frequent interaction with attackers, which is certainly true in network security. We explain this model in detail in Section~\ref{sec:onlineGame} and we have published its analysis in a simplified domain in \cite{klima2014gamesec} and 
\cite{klima2015gamesec}.


%The third game model is on the other extreme and tries to use as little background knowledge as possible.
%The attacker's does not necessarily have to be rational, we do not need to know their intentions and not even the attack actions that are available to them.
%In this model, the defender's strategy for selecting intrusion detection system configuration is quickly learned from experience.
%The configuration that tend to discover more attacks are used with higher probability, but alternative configurations are also used sufficiently often, in order to prevent the attacker for exploiting the learning process.
%Later in this project, we intend to investigate the effectiveness of the pre-computed and learned strategies dependent on how exact are the data assumed about the adversary.
%We have presented the motivation and initial experiments with learning in adversarial environment in \cite{klima2014gamesec}.
%Further explanation on how these results can be used in network intrusion detection is presented in Section~\ref{sec:learning}.


\subsubsection{Proof of concept simulations}

All the algorithms for computing the optimal attack policies, the bounds on their quality and the game theoretic solutions are implemented in a unified software framework.
It is integrated with the tools for generating attack graphs introduced in Section~\ref{sec:rt1}.
This allows us to evaluate the expected effect of introducing different security measures in various computer networks to the behavior of rational attacker's and to the expected costs of the defender for the undetected attacks as well as deployment of the security measures.
A proof-of-concept case study based on this simulation framework is presented for example in \cite{durkota2015ijcai,durkota2015gamesec}.
It presents the optimal configurations of honeypots for a sample network as well as the expected improvement of the attack detection probability caused by introducing the honeypots.

The experiments with adversarial learning have been performed in a separate MATLAB framework that facilitates simpler experiments. 
%However, we intend to integrate the developed methods to a unified framework and use the learning methods in a simulation environment, where specific honeypot configurations will be added to a network and attack will be performed based on the attack graphs generated for the new network.
Using different models of selecting the actions from the attack graph, we can evaluate the expected robustness of the learning process to different exploitation strategies form the attacker, as well as its performance against various oblivious attackers.
We have also created a tool to visualize the attack graphs, attack policies along with additional information related to these structures in order to analyze them with the naked eye.

\subsubsection{Real world network experiments}

The execution of this tasks was conditioned by developing the collaboration with a more applied partner on the project. Without them, we have focussed more on developing game theoretic models and solution algorithms. In order to emulate the actual deployment as closely as possible, we have acquired real world experimental data from Swedish Defence Research Agency (FOI).
FOI conducts cyber network exercise, where a set of hosts in the network contain targeted keys that the hired hackers attack for, while network administrators defend it.
We obtained the network traffic data, network flow data, attack paths and attack graphs of these exercises.
We have experimentally evaluated our game-theoretic approaches on attack graphs based on this data in \cite{durkota2016ieee} and we plan to use these data for further evaluation of the proposed methods in our future work.


