\subsection{Attack Graph}

\begin{figure*}[t!]
		\begin{subfigure}[b]{0.54\textwidth}
			%\includegraphics[width=\textwidth]{fig/attackGraphWithHP}
			\includegraphics[width=\textwidth]{fig/ag-orig-pretty-highly-motivated-reversed-crop.pdf}
			\caption{Attack graph}
			\label{fig:attackGraph}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.45\textwidth}
			\includegraphics[width=\textwidth]{fig/optStrategyMod2-crop}
			\caption{Attack policy}
			\label{fig:attackPolicy}
		\end{subfigure}
	\caption{(a) Attack graph representing the possible ways of gaining an access to the database. (b) Optimal attack policy for the attack graph in (a).}\label{fig:fig1}
\end{figure*}

Attack Graph (AG) compactly represents all possible sequences of actions that lead to compromising specific network.
AGs are usually represented as AND-OR graphs, where each node is either AND-node or OR-node.
AND node is achieved only if all of his successors were successful; OR-node is achieved if at least one of the successors is successful.
AND-OR graphs has been extensively studied in AI literature, but this research was mainly ignored by attack graph analysis research.
In Fig~\ref{fig:attackGraph} is an example of the attack graph for the network in Fig~\ref{fig:localNet}.
Formally, attack graph is a directed graph consisting of two types of nodes:(i)\emph{fact nodes} that can be initially either true (rectangles in Fig.~\ref{fig:attackGraph}) or false (diamonds in Fig.~\ref{fig:attackGraph}), and (ii) \emph{action nodes} (rounded rectangles in Fig.~\ref{fig:attackGraph}) that the attacker can perform.
Each fact $f$ has associated reward $r_f$ that the attacker obtains when the fact becomes true (in Fig.~\ref{fig:attackGraph} only non-zero rewards are denoted).
Each action $a$ has \emph{preconditions} --- a set of facts that must be true before the action is performed and \emph{effects} --- a set of fact that becomes true if action is successfully performed.
These relations are represented by edges between facts and action nodes in the attack graph.
Moreover, every action $a$ has an associated probability success rate $p_a$; cost $c_a$ that the attacker pays to perform the action, regardless of whether $a$ succeeds or fails; and set of host types that the actions interacts with $\tau_a$ (in Fig~\ref{fig:attackGraph} actions are labeled with action name and $\{h_a,p_a,c_a,\tau_a\}$.
Every fact have an associated rewards that attacker receives when the fact is activated (in the diagram we show only non-zero rewards under the fact name).

\begin{definition}
 An Attack Graph is defined by $AG = \langle T, F, A, p, c, \tau \rangle$, where:
 \begin{description}
	 \item[$T$] is the set host types in the network,
 \item[$F$] is a finite set of facts, that can be either \texttt{true} or \texttt{false},
 \item[$A$] is a finite set of actions; each action $a\in A$ has \emph{preconditions} $\pre(a) \subseteq \mathrm{F}$ and \emph{effects} $\eff(a) \subseteq \mathrm{F}$; additionally, there is a special action \boxT ($\pre(\boxT)=\eff(\boxT)=\emptyset$),
 %\item[$r$]$:F \rightarrow \mathbb{R^+}$ is a reward function (we use notation $r_f$ for reward of the fact f),
 \item[$p$]$:A \rightarrow (0,1]$ is the probability that an action succeeds (we use the notation $p_a$ for probability of action $a$ to succeed, with $\bar{p_a} = 1 - p_a$ the probability of failing),
 \item[$c$]$:A \rightarrow \mathbb{R^{+}}$ is cost of the action (we use notation $c_a$ for cost of action $a$) and $c_{0}=0$,
 \item[$\tau$]$:A \rightarrow \tau$, where $\tau\subseteq T$ is a function denoting the set of host types that the action interacts with.
 \end{description}
\end{definition}

For example in Fig~\ref{fig:attackGraph} we assume that the attacker have two goals: (i) to have a access to the net (reward of 100) and access to the database (reward of 1000).

There are several previous works that suggest using attack graphs for improving network security.
They often identify the minimal set of vulnerabilities that has to be removed to perfectly protect the network\cite{wang2006minimum, noel2003efficient} or allow ranking the importance of vulnerabilities based on some notion of importance\cite{mehta2006ranking}, e.g., by probability that a random attack will use a specific vulnerability.
More sophisticated models of how the attacker chooses the actions form the attack graphs are much less common in literature.
This may cause the suggested security measures to be too conservative and directed against attack plans, which would not be considered by any attacker.
For example, the attack can be too time-consuming or too costly.
We suggest modelling the attacker as a rational agent, which tries to achieve his goals while minimizing the expected cost of his actions.
If the attacker's attempt to perform an action that fails, he cannot repeat this action again.
We disallow the action repetition for two reasons: (i) if actions have static dependencies (e.g., installed software version, open port, etc.), additional attempts to perform the same action would have identical results, and (ii) if we allow infinitely many repetitions of any action, this attack graph can be equivalently transformed into a new attack graph, with those actions having the probability of success 1 and accordingly set the costs\cite{buldas2012upper}.

AGs can be automatically generated using various tools.
We use the MulVAL~\cite{ou2005mulval} to construct dependency AGs from information automatically collected using network scanning tools, such as Nessus\footnote{http://www.nessus.org~~~ $^2$http://www.openvas.org} or OpenVAS$^2$.
These AGs consist of an attacker's atomic actions, e.g., exploit actions for each vulnerability of each host, pivoting "hop" actions between the hosts that are reachable from each other, etc.
Previous works (e.g., \cite{sawilla2008identifying}) show that probabilistic metrics can be extracted from the Common Vulnerability Scoring System~\cite{mell2006common}, National Vulnerability Database~\cite{bacic2006mulval}, historical data, red team exercises, or be directly specified by the network administrator.

\subsubsection{Correlated Actions}
The actions in the attack graph are currently assumed to be uncorrelated, which means that performing one action does not change the cost or the probability of any other action.
This assumption tremendously simplifies the optimal strategy planning.
This is similar assumption to the naivety assumption in the naive Bayes in the classification problems.
However, in reality the actions may be correlated: when the attacker performs e.g. scanning of the network it could change his estimation about the cost or probability of a certain action that he knew he could have performed earlier anyway, but with different cost or probability.
Nevertheless, obtaining the actions' relationships is a difficult task, as there are $N(N-1)$ direct relationship couples, where $N$ is a total  number of actions. 
Given that there are couple of hundred actions, this task becomes infeasible by to manage by hand.
However, there might be some relationship between the actions based on their preconditions and effects, from which the actions' correlation could be computed.

\subsubsection{Action Probabilities}
The basis for the optimal policy planning is the attack graph structure, action costs and probabilities.
The MulVAL tool mainly focuses on creating good attack graph structure.
It also provides an option to calculate the action probabilities, however, in very simplistic way.
In particular, MulVAL offers two options: (i) ``with metric artifact'', which statically assigned to every action rule one of the following probabilities: 1.0 (certain), 0.8 (likely), 0.5 (possible) or 0.2 (unlikely), or (ii) ``with metric'', which sets probability based on a possible values of Access Vector (high, medium or low) from Common Vulnerability Scoring System\footnote{www.first.org/cvss} as $\{\textrm{high} \rightarrow 0.9, \textrm{medium} \rightarrow 0.6, \textrm{low} \rightarrow 0.2\}$.

\subsubsection{Action Costs}
Action costs should represent the attacker's effort for attempting the exploit. 
There are few reasonable representations for the costs e.g., duration of the exploit (this metric is useful for penetration testers who maximize the number of successful attacks during the limited time of period), communication complexity between the attacker and networks (the lower communication complexity, the lower the probability of being detected with e.g., network anomaly detectors) or monetary (amount of money that the attacker pays for obtaining the exploit code).
In our research we focus on the monetary representation of the costs, mainly due to publicly available bounty programs (discussed below).

As to our knowledge, there is no research conducted in estimating the attacker's cost for attempting to exploit the vulnerabilities.
In collaboration with a student, we attempted to develop a machine learning approach for automatic estimation of the costs.
The final diploma thesis ``Estimating the Attacker's Cost for Exploiting Computer Network Vulnerabilities'' can be found in the Appendix~\ref{app:papers}.  
Generally, the attacker's cost for exploiting a vulnerability consists of two components: (i) cost for obtaining an exploit code and (ii) cost for executing the exploit itself.
We investigated each cost separately.

{\bf Obtaining Exploit Cost}
We assume that if exploit for the required vulnerability is available on the Internet (e.g., on Metasploit, Exploit-DB, etc.), the attacker will download at no cost.
However, if the exploit is not available, the attacker is required to develop or purchase the exploit.
Estimating the development or purchase cost is a challenging task.
However, many software manufacturers run the \emph{bounty programs}, where they issue the rewards for reporting exploits for their software.
We based our research on bounty programs as a source of the obtaining the exploit costs for learning.
In a scenario, where the attacker would develop an exploit, we postulate that the bounty reward can be used as a lower bound on the development cost of the exploit.
Assuming that the attacker will either report the exploit or use it to attack (many bounty programs require to verify the identity), then by choosing to attack he is giving up the possible bounty, which is equivalent to paying the reward cost.
The specific bounty rewards are usually publicly available on large companies websites.
However, smaller companies do not offer bounty program, therefore, we used a machine learning technique to estimate the bounty program rewards.


{\bf Exploit Execution Cost}
In this part, we focused on learning the attacker's cost for executing the exploit, \emph{given} that he possesses the exploit and vulnerability is present in the computer network.
We estimate the execution cost on (i) Common Vulnerability Scoring System (CVSS) vector metric, formally describing its characteristics, assigned to every vulnerability, etc.
The labels we obtained from a penetrating testing expert on several representative vulnerabilities.
%From all 14 values, only three are relevant for the cost estimation. 
%\emph{Access Complexity}, which denotes the difficulty of exploiting given vulnerability.
%It impacts the cost by reflecting the difficulty of performing given attack.
%\emph{Exploitability} represents the availability of exploit code.
%If exploitability is low, attacker will have to write his own attack code, which will definitely increase its cost.
%Finally, \emph{Remediation level} reflects availability of patches or fixes of the vulnerability.
%It can indirectly reflect the cost of vulnerability, as exploit for vulnerability that is likely to be patched has low reusability in future, implying that its high cost.
%Access complexity, exploitability and remediation level can be obtained from the NVD and X-Force Exchange databases. 

We used genetic programming algorithm to learn both estimators, the obtaining exploit cost and exploit execution cost.
It uses evolutionary algorithm to find best mathematical formula that represents the data.
The advantage of this approach is the transparency of the output, after all it is a mathematical formula, which can be well analyzed by eye.
For learning we used CVSS features (discussed above), vulnerability age, Google searches, vulnerability update periods, etc.

Experiments for choosing the correct genetic programming parameters and detailed results of learning progress are presented in the Diploma Thesis in the Appendix~\ref{app:papers}.
In Table~\ref{tab:GPerrors} we present the errors of the found formulas for both estimators.
The second column is the standard error of the estimators, third column is the mean in the labeled data (on which the learning was based upon), and fourth column is the standard deviation in the labeled data.

\begin{table}
	\caption{Error of the learning algorithms.}
	\label{tab:GPerrors}
\begin{tabular}{|c|c|c|c|}
	\hline
	Estimator & Standard error & Label mean & Label standard deviation \\ \hline
	Obtaining Exploit Cost & 327 & 945 & 638 \\ \hline
	Exploit Execution Cost & 157 & 255 & 223 \\ \hline
\end{tabular}
\end{table}

Although the resulting formulas some errors, this is the first algorithm for estimating the attacker's costs.
With this we can reason much more precisely the attacker's preferred paths during an attack and apply appropriate countermeasures.

\paragraph{Pseudorules}
The MulVAL tool contains also actions, that do not represent any action to perform in reality, but merely the attackers knowledge.
Example of such action is the action \textit{canAccesHost} or \textit{canAccesFile}.
These actions have the success probability set to 1.0 (certain) by MulVAL.
However, this knowledge costs nothing the attacker and has no probability of interacting with honeypots thus we have set both, their costs and the probability of interacting with honeypot to 0.

